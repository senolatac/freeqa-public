freeQA
====

Hybrid Question Answering (freeQA) -- is going to drive forth the QALD vision of hybrid question answering using Linked Data and full-text search. Performance benchmarks will be done on the QALD-4 task 3 hybrid QA.
