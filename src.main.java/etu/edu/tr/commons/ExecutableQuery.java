package etu.edu.tr.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import etu.edu.tr.freeqa.nlp.DependencyTreeNode;

public class ExecutableQuery implements Cloneable, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3978632198419274158L;
	private List<String> varList = new ArrayList<String>();
	private String query = "";
	private int depth =0;
	private List<DependencyTreeNode> nodeList = new ArrayList<DependencyTreeNode>();
	
	public ExecutableQuery(){
		
	}
	
	public ExecutableQuery(List<DependencyTreeNode> nodeList){
		this.nodeList = nodeList;
	}
	
	public List<String> getVarList() {
		return varList;
	}
	public void setVarList(List<String> varList) {
		this.varList = varList;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	
	public List<DependencyTreeNode> getNodeList() {
		return nodeList;
	}
	public void setNodeList(List<DependencyTreeNode> nodeList) {
		this.nodeList = nodeList;
	}
	public void addVar(String var){
		if(this.varList==null){
			this.varList = new ArrayList<String>();
		}
		if(!varList.contains(var)){
			varList.add(var);
		}
	}
	
	public void addVarList(List<String> varList){
		for(String s:varList){
			this.addVar(s);
		}
		
	}
	public void addNode(DependencyTreeNode node){
		if(!nodeList.contains(node)){
			nodeList.add(node);
		}
		
	}
	public void addNodeList(List<DependencyTreeNode> nodeList){
		for(DependencyTreeNode n:nodeList){
			this.addNode(n);
		}
		
	}
	
	public String toString(){
		return query;
	}
	
	

}
