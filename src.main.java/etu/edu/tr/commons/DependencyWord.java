package etu.edu.tr.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class DependencyWord implements Serializable{
	
	private static final long serialVersionUID = 1821126918791136162L;

	private Integer index;
	private String source;
	private List<Lemma> lemmaList;
	private List<String> tagList;
	private String clearText;
	private List<String> clearTagList;
	private List<String> nerList;
	private boolean active = false;
	
	
	public DependencyWord() {
		this.clearText="";
		this.source="";
		this.clearTagList=new ArrayList<String>();
		this.active = false;
	}
	
	public DependencyWord(Integer index, String source, List<Lemma> lemmaList,
			List<String> tagList, String clearText, List<String> nerList, List<String> cTagList) {
		super();
		this.index = index;
		this.source = source;
		this.lemmaList = lemmaList;
		this.tagList = tagList;
		this.clearText = clearText;
		this.nerList = nerList;
		this.clearTagList=cTagList;
	}
	
	

	public Integer getIndex() {
		return index;
	}


	public void setIndex(Integer index) {
		this.index = index;
	}


	public String getSource() {
		return this.source;
	}


	public void setSource(String source) {
		this.source = source;
	}
	
	
	public List<Lemma> getLemmaList() {
		return lemmaList;
	}

	public void setLemmaList(List<Lemma> lemmaList) {
		this.lemmaList = lemmaList;
	}

	public List<String> getTagList() {
		return tagList;
	}

	public void setTagList(List<String> tagList) {
		this.tagList = tagList;
	}
	
	public String getClearText() {
		return clearText;
	}

	public void setClearText(String clearText) {
		this.clearText = clearText;
	}
	
	
	public List<String> getNerList() {
		return nerList;
	}

	public void setNerList(List<String> nerList) {
		this.nerList = nerList;
	}
	

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public List<String> getClearTagList() {
		return clearTagList;
	}

	public void setClearTagList(List<String> clearTagList) {
		this.clearTagList = clearTagList;
	}

	public String toString(){
		return this.index + " : " + this.source  + " : " + this.clearTagList.toString() + " : " + this.clearText + " : " +this.nerList.toString() ;
	}

}

