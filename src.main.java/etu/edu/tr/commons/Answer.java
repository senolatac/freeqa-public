package etu.edu.tr.commons;

import java.util.Set;

import com.hp.hpl.jena.rdf.model.RDFNode;

import etu.edu.tr.freeqa.query.SPARQLQuery;


public class Answer {

	public Set<RDFNode> answerSet;
	public SPARQLQuery query;
}