package etu.edu.tr.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import etu.edu.tr.freeqa.nlp.DependencyTree;
import etu.edu.tr.freeqa.nlp.DependencyTreeNode;
import etu.edu.tr.freeqa.nlp.SentenceAnalyze.ComparaterOfQuestion;
import etu.edu.tr.freeqa.nlp.SentenceAnalyze.TypeOfQuestion;
import etu.edu.tr.freeqa.query.SPARQLQuery;

/** citation by HAWK*/ 
public class Question implements Serializable,Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9130793012431486456L;

	public Integer id;
	public Boolean onlydbo;
	public Boolean outOfScope;
	public Boolean aggregation;
	public Boolean hybrid;
	public Boolean startsWithDate;
	public Boolean isOutputPredictable =false;
	public TypeOfQuestion typeOfQuestion;
	public ComparaterOfQuestion comporatorOfQuestion;
	public String answerType;
	public String sparqlQuery;
	public String pseudoSparqlQuery;
	public DependencyTree tree;
	public DependencyTree clearTree;
	public List<AnnotationEntity> relatedResources;
	public List<DependencyWord> dependencyList;
	public List<Dependency> dependencies;
	public List<DependencyTreeNode> orderedTree;
	public int maxDepth =0;
	public List<SPARQLQuery> queryList = new ArrayList<SPARQLQuery>();
	public Map<String, String> languageToQuestion = new LinkedHashMap<String, String>();
	public Map<String, List<String>> languageToKeywords = new LinkedHashMap<String, List<String>>();
	public Map<String, List<Entity>> languageToNamedEntites = new LinkedHashMap<String, List<Entity>>();
	public Map<String, List<Entity>> languageToNounPhrases = new LinkedHashMap<String, List<Entity>>();
	public Map<String, List<GoldEntity>> goldEntites = new HashMap<String, List<GoldEntity>>();
	public Map<String, Set<String>> goldenAnswers = new HashMap<String, Set<String>>();

	public int cardinality;


	public Question() {

		goldEntites.put("en", new ArrayList<GoldEntity>());
	}
	
	public void addRelatedResource(DependencyTreeNode node,String resource){
		if(relatedResources==null){
			relatedResources = new ArrayList<AnnotationEntity>();
		}
		relatedResources.add(new AnnotationEntity(resource, false,node));
	}
	
	public void addQuery(SPARQLQuery eq){
		SPARQLQuery e = this.contains(eq);
		if(e!=null){
			if(eq.uriVar==null || eq.uriVar.equals("")){
				eq.uriVar = e.uriVar;
			}
			this.queryList.set(this.queryList.indexOf(e), eq);
		}else{
			this.queryList.add(eq);
		}
	}
	
	private SPARQLQuery contains(SPARQLQuery eq){
		for(SPARQLQuery e : this.queryList){
			if(eq.executedQuery.contains(e.executedQuery) && eq.doubleTriples.size()>=e.doubleTriples.size() && eq.constraintTriples.size()>=e.constraintTriples.size()){
				return e;
			}
		}
		return null;
	}
	
	public void removeQuery(ExecutableQuery eq){
		if(this.queryList.contains(eq)){
			this.queryList.remove(this.queryList.indexOf(eq));
		}
		
	}
	
	@Override
	public Object clone(){
		Question q = new Question();
		try {
			q = (Question) super.clone();
			q.tree = (DependencyTree) this.tree.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return q;
	}

}
