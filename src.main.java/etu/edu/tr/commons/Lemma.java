package etu.edu.tr.commons;

import java.io.Serializable;
import java.util.List;

public class Lemma implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1833810659337488865L;
	private String word;
	private String tag;
	private List<String> snonymList;
	
	
	public Lemma(String word,String tag, List<String> snonymList) {
		super();
		this.tag = tag;
		this.word = word;
		this.snonymList = snonymList;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public List<String> getSnonymList() {
		return snonymList;
	}
	public void setSnonymList(List<String> snonymList) {
		this.snonymList = snonymList;
	}
	
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String toString(){
		return this.word + " : " + this.snonymList.toString();
	}
	

}

