package etu.edu.tr.commons;

import etu.edu.tr.freeqa.nlp.DependencyTreeNode;

public class AnnotationEntity {
	
	public String uri;
	public boolean isRelated;
	public DependencyTreeNode node;
	
	public AnnotationEntity(String uri,boolean isRelated,DependencyTreeNode node){
		this.uri = uri;
		this.isRelated = isRelated;
		this.node = node;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnnotationEntity other = (AnnotationEntity) obj;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return uri;
	}


}
