package etu.edu.tr.commons;

import java.io.Serializable;

public class Dependency  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8460699350499348115L;
	private String source;
	private String relation;
	private String target;
	
	public Dependency(String source, String relation, String target) {
		super();
		this.source = source;
		this.relation = relation;
		this.target = target;
	}

	public Dependency(){
		
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}
	
	
}
