package etu.edu.tr.freeqa.nlp;

/** citation by HAWK*/ 
public class TreeTraversal {

	public static String inorderTraversal(DependencyTreeNode depNode, int i, StringBuilder sb) {
		if (sb == null) {
			sb = new StringBuilder();
			sb.append("\n");
		}
		int size = depNode.getAnnotations().size();
		sb.append(printTabs(i) + depNode.label + " : " + depNode.lemma + " (" + depNode.nodeNumber + "|" + depNode.posTag + "|" + size + "("+depNode.getAnnotations().toString()+" : " + depNode.getClassAnnotations().toString() +")" + " : " + depNode.isLastChild + ")\n");
		++i;
		for (DependencyTreeNode node : depNode.getChildren()) {
			inorderTraversal(node, i, sb);
		}
		return sb.toString();
	}

	private static String printTabs(int i) {
		String tabs = "";
		if (i > 0) {
			tabs = "|";
		}
		for (int j = 0; j < i; ++j) {
			tabs += "=";
		}
		tabs += ">";
		return tabs;
	}
}