package etu.edu.tr.freeqa.nlp.stanford;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;










import java.util.Queue;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.BasicDependenciesAnnotation;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.util.CoreMap;
import etu.edu.tr.commons.Dependency;
import etu.edu.tr.commons.DependencyWord;
import etu.edu.tr.commons.Entity;
import etu.edu.tr.commons.Lemma;
import etu.edu.tr.commons.Question;
import etu.edu.tr.freeqa.nlp.DependencyTree;
import etu.edu.tr.freeqa.nlp.DependencyTreeNode;
import etu.edu.tr.freeqa.nlp.disambiguate.NER;
import etu.edu.tr.freeqa.nlp.disambiguate.ParserHelper;
import etu.edu.tr.freeqa.nlp.disambiguate.SynonymHelper;

public class StanfordNLP {
	
	static Logger log = LoggerFactory.getLogger(StanfordNLP.class);
	private StanfordCoreNLP pipeline;
	private SynonymHelper synonymHelper;
	private ParserHelper parserHelper;
	private static int maxLevel = 0;
	
	protected synchronized void initialize() {
    	try{
            Properties props = new Properties();
            props.put("annotators", "tokenize, ssplit, pos, lemma,ner, parse");
            pipeline = new StanfordCoreNLP(props);
            synonymHelper = new SynonymHelper();
            parserHelper = new ParserHelper();
        	}catch(Exception e){
        		e.printStackTrace();
        	}
	}
	
	public StanfordNLP(){
		initialize();
	}
	
	public SemanticGraph process(Question q){
		String sentence = q.languageToQuestion.get("en");
		if (!q.languageToNamedEntites.isEmpty()) {
			sentence = replaceLabelsByIdentifiedURIs(sentence, q.languageToNamedEntites.get("en"));
			log.debug(sentence);
		}
		  sentence = replaceLabelsByQuestionTypes(sentence);
	      Annotation document = new Annotation(sentence);
	      this.pipeline.annotate(document);
	      List<CoreMap> sentences = document.get(SentencesAnnotation.class);
	      SemanticGraph dependencies = sentences.get(0).get(BasicDependenciesAnnotation.class);
		return dependencies;
	}
	
	public SemanticGraph processClear(Question q){
		String sentence = q.languageToQuestion.get("en");
	      Annotation document = new Annotation(sentence);
	      this.pipeline.annotate(document);
	      List<CoreMap> sentences = document.get(SentencesAnnotation.class);
	      SemanticGraph dependencies = sentences.get(0).get(BasicDependenciesAnnotation.class);
		return dependencies;
	}
	
	private String replaceLabelsByIdentifiedURIs(String sentence, List<Entity> list) {
		for (Entity entity : list) {
			if (!entity.label.equals("")) {
				// " " inserted so punctuation gets seperated correctly from URIs
				sentence = sentence.replace(entity.label, entity.uris.get(0).getURI() + " ").trim();
			} else {
				log.error("Entity has no label in sentence: " + sentence);
			}
		}
		return sentence;
	}
	
	private String replaceLabelsByQuestionTypes(String sentence){
		for(QuestionType type : QuestionType.values()){
			sentence = sentence.replaceAll("(?i)"+type.toString(), type.name());
		}
		return sentence;
	}
	
    public List<String> findSentences(String documentText)
    {
    	documentText = documentText.replace(".", ". ").replace(".  ", ". ");
        List<String> sentenceList = new ArrayList<String>();
        Annotation document = new Annotation(documentText);
        pipeline.annotate(document);
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        for(CoreMap sentence: sentences) {
            sentenceList.add(sentence.toString());
        }
        return sentenceList;
    }
    
    public List<String> tagger(String documentText){
        List<String> tags = new LinkedList<String>();
        for(TaggedWord t: parserHelper.findTagList(documentText)) {
            tags.add(t.tag());
        }
        return tags;
    }
    
    public String lemmatizeSentence(String documentText)
    {
        String lemmaSentence = "";
        Annotation document = new Annotation(documentText);
        pipeline.annotate(document);
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        for(CoreMap sentence: sentences) {
            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
            	if(!token.get(PartOfSpeechAnnotation.class).toString().equals("IN")){
            		lemmaSentence = lemmaSentence + " " + token.get(LemmaAnnotation.class);
            	}
            }
        }
        return lemmaSentence.trim();
    }
    
    public List<Lemma> lemmatize(String documentText)
    {
        List<Lemma> lemmas = new ArrayList<Lemma>();
        Annotation document = new Annotation(documentText);
        pipeline.annotate(document);
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        for(CoreMap sentence: sentences) {
            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
                lemmas.add(new Lemma(token.get(LemmaAnnotation.class).toString(),token.get(PartOfSpeechAnnotation.class), Arrays.asList(synonymHelper.run(token.get(LemmaAnnotation.class)))));
            }
        }
        return lemmas;
    }
    
    public List<String> lemmatizeAsString(String documentText)
    {
        List<String> lemmas = new ArrayList<String>();
        Annotation document = new Annotation(documentText);
        pipeline.annotate(document);
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        for(CoreMap sentence: sentences) {
            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
                lemmas.add(token.get(LemmaAnnotation.class));
            }
        }
        return lemmas;
    }
    
    public List<NER> detectNersOnSentence(String documentText){
        List<NER> lemmas = new ArrayList<NER>();
        Annotation document = new Annotation(documentText);
        pipeline.annotate(document);
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        for(CoreMap sentence: sentences) {
            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
                lemmas.add(new NER(token.word(), token.get(NamedEntityTagAnnotation.class)));
            }
        }
        return lemmas;
    }
    
    public List<String> ner(List<NER> nerList,String documentText)
    {
    	String[] splits = documentText.split(" ");
        List<String> lemmas = new LinkedList<String>();
        for(String s:splits){
        	for(NER n:nerList){
            	if(n.getWord().equals(s)){
            		lemmas.add(n.getNer());
            		break;
            	}
            }
        }
        return lemmas;
    }
    
    public String getClearText(List<String> lemmas,List<String> tags){
    	String retStr = "";
    	for(int i=0; i<lemmas.size(); i++){
    		if(!lemmas.get(i).equals("be") && !lemmas.get(i).equals("do") && !tags.get(i).startsWith("W") && !tags.get(i).equals("DT") && !tags.get(i).equals("RB") && !tags.get(i).startsWith("PRP")){
    			retStr = retStr + " " + lemmas.get(i);
    		}
    	}
    	return retStr.trim();
    }
    
    public List<String> getClearTextList(List<String> lemmas,List<String> tags){
    	List<String> returnTags = new ArrayList<String>();
    	for(int i=0; i<tags.size(); i++){
    		if(!lemmas.get(i).equals("be")  && !lemmas.get(i).equals("do") && !tags.get(i).startsWith("W") && !tags.get(i).equals("DT") && !tags.get(i).equals("RB") && !tags.get(i).startsWith("PRP")){
    			returnTags.add(tags.get(i));
    		}
    	}
    	return returnTags;
    }
    
    public List<Dependency> getDependencies(Question q){
		List<Dependency> dList = new ArrayList<Dependency>();
		Annotation document = new Annotation(q.languageToQuestion.get("en"));
		pipeline.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		Tree tree = sentences.get(0).get(TreeAnnotation.class);
		// Get dependency tree
		TreebankLanguagePack tlp = new PennTreebankLanguagePack();
		GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
		GrammaticalStructure gs = gsf.newGrammaticalStructure(tree);
		Collection<TypedDependency> td = gs.typedDependencies();
		Object[] list = td.toArray();
		TypedDependency typedDependency;
		for (Object object : list) {
			typedDependency = (TypedDependency) object;
//			System.out.println(typedDependency.dep().word() + " : " +typedDependency.reln().toString() +" : " + typedDependency.gov().word());
			dList.add(new Dependency(typedDependency.dep().word(),typedDependency.reln().toString(), typedDependency.gov().word()));
		}
		return dList;
    }
    
    public List<DependencyWord> getDependencyList(Question q){
    	String sentence = q.languageToQuestion.get("en");
      return toCompactString(sentence);
  }
    
    public List<DependencyWord> toCompactString(String documentText) {
    	List<DependencyWord> wordList = new ArrayList<DependencyWord>();
    	List<NER> nerList = detectNersOnSentence(documentText);
        String str=parserHelper.parseSentence(documentText);
        maxLevel = parserHelper.maxLevel;
        String source = null;
        String strSource = null;
        boolean firstTime = true;
        int sayi=0;
        int sayi2=0;
        while(maxLevel>=0 && str.trim().length()>3){
        	if(!str.contains("["+maxLevel+"$")){
        		--maxLevel;
        	}
    	if(firstTime){
    		 source = str.substring(str.indexOf("["+maxLevel+"$")+2+(maxLevel+"").length(), str.indexOf("$"+maxLevel+"]"));
    		 firstTime=false;
    	}else{
    		while(!source.contains("_a_")){
    			source = str.substring(str.indexOf("["+maxLevel+"$",sayi)+2+(maxLevel+"").length(), str.indexOf("$"+maxLevel+"]",sayi2));
    			sayi = str.indexOf("["+maxLevel+"$",sayi+1)+1;
    			sayi2 = str.indexOf("$"+maxLevel+"]",sayi2+1)+1;
    		}
    	}
       
        if(source.contains("[")&&source.contains("]")){
        	++maxLevel;
        	firstTime=true;
        }else{
        strSource = source;
        source = source.replaceAll("_a_","").trim();
    	if(!documentText.contains(source)){
    		source = orderSource(documentText, source);
    	}
    	if(!source.trim().equals("")&&!source.trim().equals("?")&&!source.trim().equals("."))
    	wordList.add(new DependencyWord(maxLevel, source.trim(),lemmatize(source.trim()),tagger(source.trim()),getClearText(lemmatizeAsString(source.trim()),tagger(source.trim())),ner(nerList,source.trim()),getClearTextList(lemmatizeAsString(source.trim()),tagger(source.trim()))));
    	str = str.replace("["+maxLevel+"$"+strSource+"$"+maxLevel+"]", "_a_");
    	--maxLevel;
        }
        source = "";
        sayi=0;
        sayi2=0;
        }       
        return wordList;
      } 
    
    private String orderSource(String sentence,String source){
    	String result = source;
    	String[] sourceSplits = source.split(" ");
    	List<String> sList = separeteSentence(sentence, sourceSplits.length);
    	for(String s:sList){
    		int i=0;
    		for(String s1:sourceSplits){
    			if(s.contains(s1)){
    				++i;
    			}
    		}
    		if(i==sourceSplits.length){
    			result = s;
    		}
    	}
    	return result;
    }
    
    private List<String> separeteSentence(String sentence , int parse){
    	sentence = sentence.replace("?", "").replace(".", "");
    	String[] splits = sentence.split(" ");
    	List<String> orderList = new ArrayList<String>();
    	for(int i=0; i<(splits.length-parse); i++){
    		String s = "";
    		for(int j=i; j<(i+parse); j++){
    			s = s + splits[j] + " ";
    		}
    		orderList.add(s.trim());
    	}
    	return orderList;
    }
    
	public static enum QuestionType{
		HOW_MANY("how many",""),
		HOW_MUCH("how much",""),
		HOW_OLD("how\\s+old","http://dbpedia.org/ontology/birthYear"),
		HOW_LONG("how long","");
		
		private final String value;
		private final String replaceValue;

	    private QuestionType(String value,String replaceValue) {
	        this.value = value;
	        this.replaceValue=replaceValue;
	    }

	    public String getValue() {
	        return value;
	    }
	    
	    public String getReplaceValue() {
			return replaceValue;
		}

		public String toString(){
			return value;
	    	
	    }
	}
	
	public static void main(String[] args) {
		String s = "Which actress starring in the TV series Friends owns the production company Coquette Productions?";
		System.out.println(s.replaceAll("(?i)how\\s+old", "HOW_OLD"));
	}

}
