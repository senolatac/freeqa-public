package etu.edu.tr.freeqa.nlp.stanford;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import edu.stanford.nlp.semgraph.SemanticGraph;
import etu.edu.tr.commons.DependencyWord;
import etu.edu.tr.commons.Question;
import etu.edu.tr.freeqa.nlp.DependencyTree;
import etu.edu.tr.freeqa.nlp.TreeTransformer;

/** citation by HAWK*/ 
public class StanfordParser {
	private Logger log = LoggerFactory.getLogger(StanfordParser.class);
	private TreeTransformer treeTransform;

	public StanfordParser() {
		treeTransform = new TreeTransformer();
	}

	public DependencyTree process(Question q,StanfordNLP stanfordNLP) {
		if(stanfordNLP==null){
			stanfordNLP = new StanfordNLP();
		}
		SemanticGraph sg = stanfordNLP.process(q);
			DependencyTree tree = treeTransform.semanticGraphToDependencyTree(sg,q);
			return tree;
	}
	
	public DependencyTree processClear(Question q,StanfordNLP stanfordNLP) {
		if(stanfordNLP==null){
			stanfordNLP = new StanfordNLP();
		}
		SemanticGraph sg = stanfordNLP.processClear(q);
		DependencyTree tree = treeTransform.semanticGraphToDependencyTree(sg,q);
		return tree;
	}
	
	public List<DependencyWord> parse(Question q,StanfordNLP stanfordNLP){
		if(stanfordNLP==null){
			stanfordNLP = new StanfordNLP();
		}
		return stanfordNLP.getDependencyList(q);
	}

}
