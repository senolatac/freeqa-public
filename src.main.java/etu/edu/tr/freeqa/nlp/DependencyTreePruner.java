package etu.edu.tr.freeqa.nlp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;

import etu.edu.tr.commons.Question;

/** citation by HAWK*/ 
public class DependencyTreePruner {
	Logger log = LoggerFactory.getLogger(DependencyTreePruner.class);

	public DependencyTree prune(Question q) {
		log.debug(q.tree.toString());
		removalRules(q);
		removalBasedOnDependencyLabels(q);
		/*
		 * interrogative rules last else each interrogative word has at least
		 * two children, which can't be handled yet by the removal
		 */
		applyInterrogativeRules(q);
		sortTree(q.tree);
		List<DependencyTreeNode> nodes = null;
		List<DependencyTreeNode> returnNodes = inorderTraversal(nodes, q.tree.head);
		if(returnNodes.size()>1){
			DependencyTreeNode node = returnNodes.get(0);
			node.isRoot =true;
			q.tree.set(node);
			node = returnNodes.get(returnNodes.size()-1);
			node.isLastChild = true;
			q.tree.set(node);
		}
		log.debug(q.tree.toString());
		return q.tree;
	}
	
	public static List<DependencyTreeNode> inorderTraversal(List<DependencyTreeNode> nodes,DependencyTreeNode depNode) {
		if (nodes == null) {
			nodes = new ArrayList<DependencyTreeNode>();
		}
		nodes.add(depNode);
		for (DependencyTreeNode node : depNode.getChildren()) {
			inorderTraversal(nodes, node);
		}
		return nodes;
	}

	private void sortTree(DependencyTree tree) {
		Queue<DependencyTreeNode> queue = new LinkedList<DependencyTreeNode>();
		queue.add(tree.getRoot());
		while (!queue.isEmpty()) {
			DependencyTreeNode tmp = queue.poll();
			Collections.sort(tmp.getChildren());
			queue.addAll(tmp.getChildren());
		}

	}

	private void removalBasedOnDependencyLabels(Question q) {
		for (String depLabel : Lists.newArrayList("auxpass", "aux")) {
			inorderRemovalBasedOnDependencyLabels(q.tree.getRoot(), q.tree, depLabel);
		}
	}

	private boolean inorderRemovalBasedOnDependencyLabels(DependencyTreeNode node, DependencyTree tree, String depLabel) {
		if (node.depLabel.matches(depLabel)) {
			tree.remove(node);
			return true;
		} else {
			for (Iterator<DependencyTreeNode> it = node.getChildren().iterator(); it.hasNext();) {
				DependencyTreeNode child = it.next();
				if (inorderRemovalBasedOnDependencyLabels(child, tree, depLabel)) {
					it = node.getChildren().iterator();
				}
			}
			return false;
		}
	}

	private void applyInterrogativeRules(Question q) {
		DependencyTreeNode root = q.tree.getRoot();
		// GIVE ME will be deleted
		if (root.label.equals("Give")) {
			for (Iterator<DependencyTreeNode> it = root.getChildren().iterator(); it.hasNext();) {
				DependencyTreeNode next = it.next();
				if (next.label.equals("me")) {
					it.remove();
					q.tree.remove(root);
				}
			}
		}
		// LIST will be deleted
		if (root.label.equals("List")) {
			q.tree.remove(root);
		}
		// GIVE will be deleted
		if (root.label.equals("Give")) {
			q.tree.remove(root);
		}

	}

	/**
	 * removes: * punctuations (.) * wh- words(WDT|WP$) * PRP($) * DT *
	 * BY and IN (possessive) pronouns * PDT predeterminer all both
	 * 
	 * Who,Where WP|WRB stays in
	 * @param q
	 */
	private void removalRules(Question q) {
		DependencyTreeNode root = q.tree.getRoot();
		for (String posTag : Lists.newArrayList(".", "WDT", "POS",  "WP\\$", "PRP\\$", "RB","PRP", "DT", "IN", "PDT")) {
			Queue<DependencyTreeNode> queue = Queues.newLinkedBlockingQueue();
			queue.add(root);
			while (!queue.isEmpty()) {
				DependencyTreeNode tmp = queue.poll();
				if (tmp.posTag.matches(posTag)) {
					q.tree.remove(tmp);
				}
				for (DependencyTreeNode n : tmp.getChildren()) {
					queue.add(n);
				}
			}
		}

	}
	
	public Question removeEntity(Question q,String label){
		DependencyTreeNode root = q.tree.getRoot();
			Queue<DependencyTreeNode> queue = Queues.newLinkedBlockingQueue();
			queue.add(root);
			while (!queue.isEmpty()) {
				DependencyTreeNode tmp = queue.poll();
				if (tmp.label.matches(label)) {
					q.tree.remove(tmp);
				}
				for (DependencyTreeNode n : tmp.getChildren()) {
					queue.add(n);
				}
			}
			log.debug(q.tree.toString());
			return q;
	}

}
