package etu.edu.tr.freeqa.nlp;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.clearnlp.tokenization.EnglishTokenizer;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.google.common.collect.Sets;

import etu.edu.tr.commons.DependencyWord;
import etu.edu.tr.commons.Entity;
import etu.edu.tr.commons.Question;

/** citation by HAWK*/ 
public class SubSequence {
	Logger log = LoggerFactory.getLogger(SubSequence.class);
	Set<String> blackList = Sets.newHashSet("only","some","same");


	// combine noun phrases
	// TODO improve noun phrases e.g. combine following nulls, i.e., URLs to get
	// early life of Jane Austin instead of early life
	public void combineSequences(Question q) {
		this.addRelatedResources(q);
		String question = q.languageToQuestion.get("en");
		EnglishTokenizer tok = new EnglishTokenizer();
		List<String> list = tok.getTokens(question);
		List<String> subsequence = Lists.newArrayList();
		List<String> lemmaSubsequence = Lists.newArrayList();
		for (int tcounter = 0; tcounter < list.size(); tcounter++) {
			String token = list.get(tcounter);
			String pos = pos(token, q);
			String lemma = lemma(token, q);
			// look for start "RB|JJ|NN(.)*"
			if (subsequence.isEmpty() && null != pos && pos.matches("CD|JJ|NN(.)*|RB(.)*")) {
				subsequence.add(token);
				if(!blackList.contains(token))
				lemmaSubsequence.add(lemma);
			}
			// split "of the" or "of all" via pos_i=IN and pos_i+1=DT
			else if (!subsequence.isEmpty() && null != pos && tcounter + 1 < list.size() && null != pos(list.get(tcounter + 1), q) && pos.matches("IN") && pos(list.get(tcounter + 1), q).matches("(W)?DT")) {
				if (subsequence.size() >= 2) {
					transformTree(subsequence,lemmaSubsequence, q);
				}
				subsequence = Lists.newArrayList();
				lemmaSubsequence = Lists.newArrayList();
			}
			else if (!subsequence.isEmpty() && null != pos && null != pos(list.get(tcounter - 1), q) && !pos(list.get(tcounter - 1), q).matches("NNP") && pos.matches("NNP")) {
				if (subsequence.size() >= 2) {
					transformTree(subsequence,lemmaSubsequence, q);
				}
				subsequence = Lists.newArrayList();
				lemmaSubsequence = Lists.newArrayList();
			}
			// do not combine NNS and NNPS but combine "stage name",
			// "British Prime minister"
			else if (!subsequence.isEmpty() && null != pos && null != pos(list.get(tcounter - 1), q) && pos(list.get(tcounter - 1), q).matches("NNS") && pos.matches("NNP(S)?")) {
				if (subsequence.size() > 2) {
					transformTree(subsequence,lemmaSubsequence, q);
				}
				subsequence = Lists.newArrayList();
				lemmaSubsequence = Lists.newArrayList();
			}
			// finish via VB* or IN -> null or IN -> DT or WDT (now a that or which follows)
			else if (!subsequence.isEmpty() && !pos(list.get(tcounter - 1), q).matches("JJ|HYPH")
					&& (null == pos || pos.matches("VB(.)*|\\.|WDT") || (pos.matches("IN") && pos(list.get(tcounter + 1), q) == null) || (pos.matches("IN") && pos(list.get(tcounter + 1), q).matches("DT")))) {
				// more than one token, so summarizing makes sense
				if (subsequence.size() > 1) {
					transformTree(subsequence,lemmaSubsequence, q);
				}
				subsequence = Lists.newArrayList();
				lemmaSubsequence = Lists.newArrayList();
			}
			// continue via "NN(.)*|RB|CD|CC|JJ|DT|IN|PRP|HYPH"
			else if (!subsequence.isEmpty() && null != pos && (pos.matches("NN(.)*|RB|CD|CC|JJ|DT|PRP|HYPH|VBN") || token.equals("of"))) {
				subsequence.add(token);
				if(!blackList.contains(token))
				lemmaSubsequence.add(lemma);
			} else {
				subsequence = Lists.newArrayList();
				lemmaSubsequence = Lists.newArrayList();
			}
		}
	}

	private void transformTree(List<String> subsequence,List<String> lemmaSubsequence, Question q) {
		subsequence = clearSequence(subsequence, q);
		if(subsequence.isEmpty() || subsequence.size()<2){
			return;
		}
		lemmaSubsequence = clearLemmaSequence(subsequence, lemmaSubsequence, q);
		String newLabel = Joiner.on("_").join(subsequence);
		String newLemma = Joiner.on(" ").join(lemmaSubsequence);
		DependencyTreeNode top = findTopMostNode(q.tree.getRoot(), subsequence);
		// if top node equals null the target subsequence is not in one
		// dependence subtree and thus will not be joined together
		if (top == null) {
			System.err.println("Burada : " + newLabel);
			return;
		}
		for (String sub : subsequence) {
			Queue<DependencyTreeNode> queue = Queues.newLinkedBlockingQueue();
			queue.add(q.tree.getRoot());
			// delete unnecessary sub nodes
			while (!queue.isEmpty()) {
				DependencyTreeNode tmp = queue.poll();
				if (tmp.label.equals(sub) && !tmp.equals(top)) {
					q.tree.remove(tmp);
					break;
				} else {
					for (DependencyTreeNode n : tmp.getChildren()) {
						queue.add(n);
					}
				}
			}
		}
		top.label = newLabel;
		top.lemma = newLemma;
		top.posTag = "CombinedNN";
	}
	
	private List<String> clearSequence(List<String> subsequence,Question q){
		String newLabel = Joiner.on(" ").join(subsequence);
		for(DependencyWord dw : q.dependencyList){
			if(dw.getSource().replace("\\s+", " ").contains(newLabel.trim())){
				return subsequence;
			}
		}
		List<String> rList = new ArrayList<String>();
		for(int i=0; i<subsequence.size(); i++){
			List<String> tList = target(subsequence.get(i), q);
			String t = isContainElement(subsequence, tList);
			if(t!=null){
				if(!rList.contains(subsequence.get(i))){
					rList.add(subsequence.get(i));
				}
				if(!rList.contains(t)){
					rList.add(t);
				}else{
					rList.remove(rList.indexOf(t));
					rList.add(t);
				}
			}
		}
		if(rList.size()==subsequence.size()){
			return subsequence;
		}
		return rList;
	}
	
	private List<String> clearLemmaSequence(List<String> subsequence,List<String> lemmas,Question q){
		List<String> rList = new ArrayList<String>();
		for(int i=0; i<subsequence.size(); i++){
			String l = lemma(subsequence.get(i), q);
			if(lemmas.contains(l)){
				rList.add(l);
			}
		}
		return rList;
	}

	// use breadth first search to find the top most node
	private DependencyTreeNode findTopMostNode(DependencyTreeNode root, List<String> tokenSet) {
		Queue<DependencyTreeNode> queue = Queues.newLinkedBlockingQueue();
		queue.add(root);
		while (!queue.isEmpty()) {
			DependencyTreeNode tmp = queue.poll();
			for (String token : tokenSet) {
				if (token.equals(tmp.label)) {
					if (subTreeContainsOtherToken(tmp, tokenSet)) {
						return tmp;
					}
				}
			}
			for (DependencyTreeNode n : tmp.getChildren()) {
				queue.add(n);
			}
		}
		return null;
	}

	/**
	 * to match correct subtree it is important to find the top most node in the
	 * subtree that contains every token of the combined noun
	 * 
	 * @param root
	 * @param tokenSet
	 * @return
	 */
	private boolean subTreeContainsOtherToken(DependencyTreeNode root, List<String> tokenSet) {
		boolean[] tokenCheck = new boolean[tokenSet.size()];
		for (int i = 0; i < tokenSet.size(); i++) {
			Queue<DependencyTreeNode> queue = Queues.newLinkedBlockingQueue();
			if(root.getChildren().isEmpty()){
				root = root.parent;
			}
			queue.add(root);
			while (!queue.isEmpty()) {
				DependencyTreeNode tmp = queue.poll();
				if (tokenSet.get(i).equals(tmp.label)) {
					tokenCheck[i] = true;
				}
				for (DependencyTreeNode n : tmp.getChildren()) {
					queue.add(n);
				}

			}
		}
		for (boolean item : tokenCheck) {
			if (!item) {
				return false;
			}
		}
		return true;
	}

	private String pos(String token, Question q) {
		Stack<DependencyTreeNode> stack = new Stack<DependencyTreeNode>();
		stack.push(q.tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			if (tmp.label.equals(token)) {
				return tmp.posTag;
			} else {
				for (DependencyTreeNode child : tmp.getChildren()) {
					stack.push(child);
				}
			}

		}
		return null;
	}
	
	private String lemma(String token, Question q) {
		Stack<DependencyTreeNode> stack = new Stack<DependencyTreeNode>();
		stack.push(q.tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			if (tmp.label.equals(token)) {
				return tmp.lemma;
			} else {
				for (DependencyTreeNode child : tmp.getChildren()) {
					stack.push(child);
				}
			}

		}
		return null;
	}
	
	private List<String> target(String token, Question q) {
		Stack<DependencyTreeNode> stack = new Stack<DependencyTreeNode>();
		stack.push(q.tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			if (tmp.label.equals(token)) {
				return tmp.targetList;
			} else {
				for (DependencyTreeNode child : tmp.getChildren()) {
					stack.push(child);
				}
			}

		}
		return null;
	}
	
	private void addRelatedResources(Question question){
		for(Entity e : question.languageToNamedEntites.get("en")){
			if(e.uris!=null && !e.uris.isEmpty()){
				question.addRelatedResource(node(e.uris.get(0).getURI(),question),e.uris.get(0).getURI());
			}
			
		}
		
	}
	
	private DependencyTreeNode node(String token, Question q){
		Stack<DependencyTreeNode> stack = new Stack<DependencyTreeNode>();
		stack.push(q.tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			if (tmp.label.equals(token)) {
				return tmp;
			} else {
				for (DependencyTreeNode child : tmp.getChildren()) {
					stack.push(child);
				}
			}

		}
		return null;
	}
	
	private String getStringOfList(List<String> subsequence){
		String r = "";
		for(String s :subsequence){
			r = r + s + " ";
		}
		return r.trim();
	}
	
	private String isContainElement(List<String> subsequence ,List<String> targetList){
		for(String s : targetList){
			if(subsequence.contains(s)){
				return s;
			}
		}
		return null;
	}

}
