package etu.edu.tr.freeqa.nlp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

import etu.edu.tr.commons.DependencyWord;

/** citation by HAWK*/ 
public class DependencyTreeNode implements Comparable<DependencyTreeNode>, Serializable,Cloneable {
	private static final long serialVersionUID = 3684161169564127853L;
	public List<DependencyTreeNode> children = new ArrayList<>();
	private boolean used = false;
	public boolean isLastChild = false;
	public boolean isRoot = false;
	public boolean isOutput = false;
	public DependencyTreeNode parent;
	public int nodeNumber;
	public String depLabel;
	public String label;
	public String posTag;
	public String lemma;
	public List<String> targetList = new ArrayList<String>();
	public DependencyWord word;
	public String type;
	private List<String> annotations = Lists.newArrayList();
	private List<String> classAnnotations = Lists.newArrayList();

	public DependencyTreeNode() {
	}

	public DependencyTreeNode(String label, String posTag, String depLabel, DependencyTreeNode parent, int i, String lemma) {
		this.label = label;
		this.posTag = posTag;
		this.parent = parent;
		this.depLabel = depLabel;
		this.nodeNumber = i;
		this.lemma = lemma;
	}

	public void addChild(DependencyTreeNode newNode) {
		children.add(newNode);
	}

	public List<DependencyTreeNode> getChildren() {
		return children;
	}
	
	@Override
	public Object clone(){
		DependencyTreeNode cloned = new DependencyTreeNode();
		try {
			cloned = (DependencyTreeNode) super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cloned;
	}

	@Override
	public String toString() {
		return label + ":" +"\t posTag: " +posTag +"\t lemma: " +lemma;
	}

	@Override
	public int compareTo(DependencyTreeNode o) {
		if (this.nodeNumber > o.nodeNumber) {
			return 1;
		} else {
			return -1;
		}
	}

	public void isUsed() {
		used = true;
	}

	public boolean used() {
		return used;
	}


	public void addAnnotation(String resourceImpl) {
		if (annotations == null) {
			annotations = new ArrayList<>();
		}
		annotations.add(resourceImpl);
	}

	public List<String> getAnnotations() {
		if (annotations == null) {
			return new ArrayList<>();
		} else {
			return annotations;
		}
	}
	
	public void setAnnotations(List<String> annotations) {
		this.annotations = annotations;
	}

	public void addClassAnnotation(String resourceImpl) {
		if (classAnnotations == null) {
			classAnnotations = new ArrayList<>();
		}
		classAnnotations.add(resourceImpl);
	}

	public List<String> getClassAnnotations() {
		if (classAnnotations == null) {
			return new ArrayList<>();
		} else {
			return classAnnotations;
		}
	}

	public void setClassAnnotations(List<String> classAnnotations) {
		this.classAnnotations = classAnnotations;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DependencyTreeNode other = (DependencyTreeNode) obj;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		return true;
	}
	
	
}
