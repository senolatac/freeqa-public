package etu.edu.tr.freeqa.nlp.similarity;

import simplenlg.features.Feature;
import simplenlg.features.Form;
import simplenlg.features.InterrogativeType;
import simplenlg.features.Tense;
import simplenlg.framework.InflectedWordElement;
import simplenlg.framework.LexicalCategory;
import simplenlg.framework.NLGFactory;
import simplenlg.framework.WordElement;
import simplenlg.lexicon.XMLLexicon;
import simplenlg.phrasespec.NPPhraseSpec;
import simplenlg.phrasespec.SPhraseSpec;
import simplenlg.realiser.english.Realiser;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.data.PointerType;
import net.didion.jwnl.data.relationship.Relationship;

public class WordNetService {
	
	public static void main(String[] args) {
		XMLLexicon lexicon = new XMLLexicon("resources/default-lexicon.xml");
		NLGFactory nlgFactory = new NLGFactory(lexicon);
//		WordElement word = lexicon.getWord("sign", LexicalCategory.VERB);
//		InflectedWordElement infl = new InflectedWordElement(word);
//		infl.setFeature(Feature.FORM, Form.GERUND);
//		Realiser realiser = new Realiser(lexicon);
//		String past = realiser.realise(infl).getRealisation();
//		System.out.println(past);
		
		SPhraseSpec p = nlgFactory.createClause("I","sign");
		p.setFeature(Feature.FORM, Form.GERUND);
		Realiser realiser = new Realiser(lexicon);
		String output2 = realiser.realiseSentence(p); // Realiser created earlier.
        System.out.println(output2);
	}
	
    public static boolean isRelatedWords(String word1, String word2,String tag1) {
      try{
        IndexWord start = WordNetHelper.getWord(tag1.startsWith("V")?POS.VERB:POS.NOUN, word1);
        IndexWord end = WordNetHelper.getWord(POS.NOUN, word2);
        PointerType type = tag1.startsWith("V")?PointerType.NOMINALIZATION:PointerType.HYPERNYM;
        
        // Ask for a Relationship object
        Relationship rel = WordNetHelper.getRelationship(start, end, type);
        if(rel==null && tag1.startsWith("V")){
            start = WordNetHelper.getWord(POS.NOUN, word1);
            end = WordNetHelper.getWord(POS.NOUN, word2);
            type = PointerType.HYPERNYM;
            rel = WordNetHelper.getRelationship(start, end, type);
        }
        // If it's not null we found the relationship
        if (rel != null && rel.getDepth()<4) {
            return true;
        }
        else {
            return false;
        }
	}catch(Exception e){
		e.printStackTrace();
	}
      return false;

    }

}
