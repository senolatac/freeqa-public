package etu.edu.tr.freeqa.nlp.similarity;

import org.apache.lucene.search.spell.LevensteinDistance;

import net.ricecode.similarity.JaroWinklerStrategy;
import net.ricecode.similarity.SimilarityStrategy;
import net.ricecode.similarity.StringSimilarityService;
import net.ricecode.similarity.StringSimilarityServiceImpl;

public class StringSimilarityHelper {
	
	public static void main(String[] args) {
		SimilarityStrategy strategy = new JaroWinklerStrategy();
		String target = "Mars_Ill";
		String source = "Mona Lisa";
		StringSimilarityService service = new StringSimilarityServiceImpl(strategy);
		double score = service.score(source, target); // Score is 0.90
		System.out.println(score);
	}
	
	public boolean isTextSimilar(String target,String source){
		SimilarityStrategy strategy = new JaroWinklerStrategy();
		StringSimilarityService service = new StringSimilarityServiceImpl(strategy);
		double score = service.score(source, target); // Score is 0.90
		if(score>0.70){
			System.err.println(score + "iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
			return true;
		}
		return false;
		
	}

}
