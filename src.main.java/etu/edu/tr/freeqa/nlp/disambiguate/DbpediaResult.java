package etu.edu.tr.freeqa.nlp.disambiguate;

import java.util.List;


public class DbpediaResult {
	
	private String resource;
	private String label;
	private String abstractValue;
	private List<String> nameList;
	private List<String> typeList;
	private List<String> disambiguateList;
	private List<String> wikiList;
	
	public DbpediaResult() {
	}
	
	public DbpediaResult(String resource) {
		this.resource = resource;
	}
	
	public DbpediaResult(String resource,String abstractValue) {
		this.resource=resource;
		this.abstractValue = abstractValue;
	}
	
	public DbpediaResult(String resource, String label, String abstractValue,List<String> name,List<String> typeList,List<String> disambiguateList,List<String> wikiList) {
		super();
		this.resource = resource;
		this.label=label;
		this.abstractValue = abstractValue;
		this.nameList = name;
		this.typeList = typeList;
		this.disambiguateList = disambiguateList;
		this.wikiList=wikiList;
	}
	public String getResource() {
		return resource;
	}
	public void setResource(String resource) {
		this.resource = resource;
	}
	public String getAbstractValue() {
		return abstractValue;
	}
	public void setAbstractValue(String abstractValue) {
		this.abstractValue = abstractValue;
	}

	public List<String> getNameList() {
		return nameList;
	}

	public void setNameList(List<String> nameList) {
		this.nameList = nameList;
	}
	
	public List<String> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<String> typeList) {
		this.typeList = typeList;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public List<String> getDisambiguateList() {
		return disambiguateList;
	}

	public void setDisambiguateList(List<String> disambiguateList) {
		this.disambiguateList = disambiguateList;
	}
	
	

	public List<String> getWikiList() {
		return wikiList;
	}

	public void setWikiList(List<String> wikiList) {
		this.wikiList = wikiList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + this.resource.hashCode();
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final DbpediaResult other = (DbpediaResult) obj;
		if (!this.resource.equals(other.resource)) {
			return false;
		}
		return true;
	}
	

}

