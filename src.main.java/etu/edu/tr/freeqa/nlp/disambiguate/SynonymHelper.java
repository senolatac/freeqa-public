package etu.edu.tr.freeqa.nlp.disambiguate;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.data.Synset;
import net.didion.jwnl.data.Word;
import net.didion.jwnl.dictionary.Dictionary;

import org.apache.lucene.wordnet.SynonymMap;

import edu.smu.tspell.wordnet.WordNetDatabase;

public class SynonymHelper {
	
	private SynonymMap map;
	static WordNetDatabase database = WordNetDatabase.getFileInstance();

	public SynonymHelper() {
		try {
			map = new SynonymMap(new FileInputStream("resources/wn_s.pl"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String[] run(String word){
		return map.getSynonyms(word);
	}
	
	public static List<String> getAllSynonyms(String tag,String word)
	{
		List<String> resultList = new ArrayList<String>();
		System.setProperty("wordnet.database.dir", "dict/");
		try {
			JWNL.initialize(new FileInputStream("dict/file_properties.xml"));
			Dictionary wordnet = Dictionary.getInstance();
			if(tag.startsWith("V") || tag.startsWith("N")){
			IndexWord token = wordnet.lookupIndexWord(tag.startsWith("V")?POS.VERB:POS.NOUN, word);
			if(token!=null){
			Synset[] senses = token.getSenses();
			for (int i = 0; i < senses.length; i++) {
				Word[] w = senses[i].getWords();
				for(Word a : w){
					resultList.add(a.getLemma());
				}
		    }
			}
			}
			} catch (JWNLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return resultList;
	}

}

