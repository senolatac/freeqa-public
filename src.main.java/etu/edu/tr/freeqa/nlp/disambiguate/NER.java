package etu.edu.tr.freeqa.nlp.disambiguate;

public class NER {
	private String word;
	private String ner;
	
	
	
	public NER(String word, String ner) {
		super();
		this.word = word;
		this.ner = ner;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public String getNer() {
		return ner;
	}
	public void setNer(String ner) {
		this.ner = ner;
	}
	
	

}

