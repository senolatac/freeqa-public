package etu.edu.tr.freeqa.nlp.disambiguate;

import java.util.ArrayList;
import java.util.List;

import org.jvnet.inflector.Noun;

import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.trees.Tree;

public class ParserHelper {
	private static LexicalizedParser lp;
	public static int maxLevel = 0;
	
	public ParserHelper(){
		String grammar = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";
	    String[] options = { "-maxLength", "80", "-retainTmpSubcategories" };
	    lp = LexicalizedParser.loadModel(grammar, options);
	}
	
	  public static String rep(String sentence,String str){
		  if(sentence.indexOf(str)==-1){
			  return sentence;
		  }
			String sstr = sentence.substring(sentence.indexOf(str));
			int say = 0;
			while(sstr.contains("(") || sstr.contains(")")){
				if(sstr.indexOf("(")<sstr.indexOf(")") && sstr.indexOf("(")!=-1){
					sstr = sstr.substring(0,sstr.indexOf("("))+"X"+sstr.substring(sstr.indexOf("(")+1);
		    		++say;
		    	}else{
		    		--say;
		    		if(say==0){
		    			sstr = sstr.substring(0,sstr.indexOf(")"))+sstr.substring(sstr.indexOf(")")+1);
		    			break;
		    		}
		    		sstr = sstr.substring(0,sstr.indexOf(")"))+"Y"+sstr.substring(sstr.indexOf(")")+1);
		    		
		    	}
			}
			return sentence.substring(0,sentence.indexOf(str))+sstr.replace("X", "(").replace("Y", ")").replace(str, "");
		  }
	
	  public String parseSentence(String sentence){
		    String pString = lp.parse(sentence).pennString();
//		    pString = rep(pString, "(SQ");
		    int say=0;
		    while(pString.contains("(") || pString.contains(")")){
		    	if(pString.indexOf("(")<pString.indexOf(")") && pString.indexOf("(")!=-1){
		    		pString = pString.substring(0,pString.indexOf("("))+"["+say+"$"+pString.substring(pString.indexOf("(")+1);
		    		maxLevel = getMaxNum(maxLevel, say);
		    		++say;
		    	}else{
		    		--say;
		    		pString = pString.substring(0,pString.indexOf(")"))+"$"+say+"]"+pString.substring(pString.indexOf(")")+1);
		    	}
		    }
		    String[] pSplits = pString.split("\n");
		    String r = "";
		    	for(String s:pSplits){
		    		s =s.trim();
		    		int max= maxLevel;
		    		while(max>0){
			    	if(s.trim().contains("["+max+"$")&&s.trim().contains("$"+max+"]")&&s.trim().indexOf("["+max+"$")>1){
			    		s= s.replace("["+max+"$", "").replace("$"+max+"]", "");
			    	}else{
			    		--max;
			    	}
		    		}
		    		s = s+" ";
		    		s = s.substring(0, s.indexOf("$")+1) + s.substring(s.indexOf(" "));
		        	r = r+s;
		    		
		    	}
		    	while(!r.contains("["+maxLevel+"$")){
		    		--maxLevel;
		    	}
		    	return clearSentence(r,sentence);
	  }
	  
	  private String clearSentence(String sentence,String clearSentence){
	    	int max = maxLevel;
	    	while(max>0){
	    		int ind = 0;
	    		String kismi = sentence;
		    	while(kismi.contains("$"+max+"] ["+max+"$")){
		    			ind = sentence.indexOf("$"+max+"] ["+max+"$",ind)+(max+"").length()+2;
		    			String k = sentence.substring(0, ind);
		    			String l = sentence.substring(ind);
		    			if(!k.substring(k.lastIndexOf("["+max+"$")).contains("VB") && !k.substring(k.lastIndexOf("["+max+"$")).contains("["+(max+1)+"$") && !l.substring(0,l.indexOf("$"+max+"]")).contains("VB") && !l.substring(0,l.indexOf("$"+max+"]")).contains("["+(max+1)+"$"))
		    				sentence = k.substring(0, ind-((max+"").length()+2)) + l.substring(((max+"").length()+3));
		    			kismi = sentence.substring(ind);
		    	}
	    	--max;
	    	}
	    	for(TaggedWord t:findTagList(clearSentence)){
	    		sentence = sentence.replace(" "+t.tag()+" ", " ");
    	}
	    	return sentence;
	  }
	  
	  public List<TaggedWord> findTagList(String sentence){
	      Tree parse = lp.parse(sentence);
	      ArrayList<TaggedWord> tagList = parse.taggedYield();
	      return tagList;
	  }
	  
	    private int getMaxNum(int first,int second){
	    	if(first>second){
	    		return first;
	    	}else{
	    		return second;
	    	}
	    }
	    
	    public static void main(String[] args) {
	    	System.out.println(Noun.pluralOf("the first known photographer of snowflake"));
			ParserHelper h = new ParserHelper();
			System.out.println(h.parseSentence("Which actress starring in the TV series Friends owns the production company Coquette Productions?"));
//		h.maxLevel = 5;
//			System.out.println(h.clearSentence("[0$ [1$ [2$ [3$ Which buildings$3] [3$ in [4$ art deco style$4]$3]$2] [2$ did [3$ [4$ Shreve$4] [4$ ,$4] [4$ Lamb [5$ and Harmon$5]$4]$3] [3$ design$3]$2] [2$ ?$2]$1]$0]"));
	    }

}

