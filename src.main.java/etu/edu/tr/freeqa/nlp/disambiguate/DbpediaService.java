package etu.edu.tr.freeqa.nlp.disambiguate;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.*;
import org.jvnet.inflector.Noun;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

import etu.edu.tr.commons.Triple;
import etu.edu.tr.freeqa.query.SPARQL;


public class DbpediaService {
	
	SPARQL sparql;
	
	public DbpediaService(){
		this.sparql = new SPARQL();
	}
	
// ?person ?type ?label ?abstract ?disambiguate ?wiki 
	public List<DbpediaResult> get(String entityName){
		List<DbpediaResult> resultList = new ArrayList<DbpediaResult>();
		int offsetCount = 10;
		for(int j=0; j<offsetCount; j++){
		QueryExecution qe = sparql.qef.createQueryExecution(getEntityString(entityName, j*2000));
		if(qe == null){
			break;
		}
		if (qe != null) {
			ResultSet results = qe.execSelect();
			while (results.hasNext()) {
				QuerySolution qs = results.next();
				String resource = qs.get("?person").asResource().getURI();
	            if(resultList.contains(new DbpediaResult(resource.trim()))){
	            	  DbpediaResult dp = resultList.get(resultList.indexOf(new DbpediaResult(resource.trim())));
	            	if(qs.get("?type")!=null && qs.get("?type").asResource().getURI()!=null){
	            	  if(!dp.getTypeList().contains(qs.get("?type").asResource().getURI())){
	            		  dp.getTypeList().add(qs.get("?type").asResource().getURI()); 
	            	  }
	            	} 
	            	
	            	if(qs.get("?disambiguate")!=null && qs.get("?disambiguate").asResource().getURI()!=null){
	              	  if(!dp.getDisambiguateList().contains(qs.get("?disambiguate").asResource().getURI())){
	              		  dp.getDisambiguateList().add(qs.get("?disambiguate").asResource().getURI()); 
	              	  }
	              	} 
	            	
	            	if(qs.get("?wiki")!=null && qs.get("?wiki").asResource().getURI()!=null){
	            	  if(!dp.getWikiList().contains(qs.get("?wiki").asResource().getURI())){
	            		  dp.getWikiList().add(qs.get("?wiki").asResource().getURI()); 
	            	  }
	            	}           	
	            	
	            	  resultList.set(resultList.indexOf(dp), dp);
	              }else{
	            	  List<String> typeList = new ArrayList<String>();
	            	  List<String> disList = new ArrayList<String>();
	            	  List<String> wikiList = new ArrayList<String>();
	            	  if(qs.get("?type")!=null && qs.get("?type").asResource().getURI()!=null){
	            		typeList.add(qs.get("?type").asResource().getURI());
	            	  }
	            	  if(qs.get("?disambiguate")!=null && qs.get("?disambiguate").asResource().getURI()!=null){
	            		disList.add(qs.get("?disambiguate").asResource().getURI());
	            	  }
	            	  if(qs.get("?wiki")!=null && qs.get("?wiki").asResource().getURI()!=null){
	            		wikiList.add(qs.get("?wiki").asResource().getURI());
	            	  }
	            	  String label = qs.get("?label").asLiteral().getString();
	            	  String abst = "";
	            	  if(qs.get("?abstract")!=null){
	            		  abst = qs.get("?abstract").asLiteral().getString(); 
	            	  }
	            	  
	                resultList.add(new DbpediaResult(resource,label,abst,null,typeList,disList,wikiList));
	            
	              }
			}
		}
		}
		return resultList;
	}
	
	public boolean labelIsExist(String label){
		try{
			QueryExecution qe = sparql.qef.createQueryExecution("ASK WHERE {?uri rdfs:label ?label. ?label bif:contains '\"" +label+"\"'@en}");
			if(qe!=null){
				boolean result = qe.execAsk();
				return result;
			}
		}catch(Exception e){
			return false;
		}
		return false;
	}
	
//	public static List<DbpediaResult> get(String entityName){
//		List<DbpediaResult> resultList = new ArrayList<DbpediaResult>();
//		try{
//		int offsetCount = 10;
//		for(int j=0; j<offsetCount; j++){
//		String sparqlQuery = constructSparqlQuery(entityName,j*2000);
//		String TEST_XML_STRING = httpGet(sparqlQuery);
//		if(TEST_XML_STRING==null){
//			break;
//		}
//		JSONObject xmlJSONObj = XML.toJSONObject(TEST_XML_STRING);
//		JSONArray arr1 =  xmlJSONObj.getJSONObject("table").optJSONArray("tr");
//		if(arr1==null){
//			break;
//		}
//		for(int i = 1; i < arr1.length() ; i++){
//			JSONArray arr2 = arr1.getJSONObject(i).getJSONArray("td");
//			JSONObject o2 = arr2.getJSONObject(0).getJSONObject("a");
//			String resource = o2.getString("href");
//            if(resultList.contains(new DbpediaResult(resource.trim()))){
//          	  DbpediaResult dp = resultList.get(resultList.indexOf(new DbpediaResult(resource.trim())));
//          	if(!arr2.isNull(1)&&!arr2.get(1).equals("")){
//          	  if(!dp.getTypeList().contains(arr2.getJSONObject(1).getJSONObject("a").getString("href"))){
//          		  dp.getTypeList().add(arr2.getJSONObject(1).getJSONObject("a").getString("href")); 
//          	  }
//          	} 
//          	
//          	if(!arr2.isNull(4)&&!arr2.get(4).equals("")){
//            	  if(!dp.getDisambiguateList().contains(arr2.getJSONObject(4).getJSONObject("a").getString("href"))){
//            		  dp.getDisambiguateList().add(arr2.getJSONObject(4).getJSONObject("a").getString("href")); 
//            	  }
//            	} 
//          	
//          	if(!arr2.isNull(5)&&!arr2.get(5).equals("")){
//          	  if(!dp.getWikiList().contains(arr2.getJSONObject(5).getJSONObject("a").getString("href"))){
//          		  dp.getWikiList().add(arr2.getJSONObject(5).getJSONObject("a").getString("href")); 
//          	  }
//          	}           	
//          	
//          	  resultList.set(resultList.indexOf(dp), dp);
//            }else{
//          	  List<String> typeList = new ArrayList<String>();
//          	  List<String> disList = new ArrayList<String>();
//          	  List<String> wikiList = new ArrayList<String>();
//          	  if(!arr2.isNull(1)&&!arr2.get(1).equals("")){
//          		typeList.add(arr2.getJSONObject(1).getJSONObject("a").getString("href"));
//          	  }
//          	  if(!arr2.isNull(4)&&!arr2.get(4).equals("")){
//          		disList.add(arr2.getJSONObject(4).getJSONObject("a").getString("href"));
//          	  }
//          	  if(!arr2.isNull(5)&&!arr2.get(5).equals("")){
//          		wikiList.add(arr2.getJSONObject(5).getJSONObject("a").getString("href"));
//          	  }
//          	  
//              resultList.add(new DbpediaResult(resource,arr2.getString(2),arr2.getString(3),null,typeList,disList,wikiList));
//          
//            }
//
//		}
//		}
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//		return resultList;
//
//	}
	
	public static Long getDisambiguateCount(String entityName){
	    try{
			String sparqlQuery = countDisambiguateQuery(entityName);
			String TEST_XML_STRING = httpGet(sparqlQuery);
			if(TEST_XML_STRING==null){
				return 0L;
			}
			JSONObject xmlJSONObj = XML.toJSONObject(TEST_XML_STRING);
			JSONArray arr1 =  xmlJSONObj.getJSONObject("table").optJSONArray("tr");
			if(arr1==null){
				return 0L;
			}
			return Long.parseLong(arr1.getJSONObject(1).get("td").toString());
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	    return 0L;
	}
	
	public static String getPartial(String entityName){
		try{
			String r = retrievePartial(entityName.replaceAll("\\s+", "+"));
			String T_X_S = httpGet(r);
			if(T_X_S==null){
				return null;
			}
			JSONObject xmlJSON = XML.toJSONObject(T_X_S);
			JSONArray tr = xmlJSON.getJSONObject("table").optJSONArray("tr");
			if(tr==null){
				return null;
			}
			String resource = entityName.trim().replaceAll("\\s+", "_");
			for(int i = 1; i < tr.length() ; i++){
				JSONObject arr2 = tr.getJSONObject(i).getJSONObject("td");
				JSONObject o1 = arr2.getJSONObject("a");
				String r1 = o1.getString("href");
				if(r1.replace("http://dbpedia.org/resource/", "").equalsIgnoreCase(resource)){
					return r1;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getDescriptionPartial(String entityName,String fullName){
		try{
			String r = retrieveDescriptionPartial(entityName, fullName);
			String T_X_S = httpGet(r);
			if(T_X_S==null){
				return null;
			}
			JSONObject xmlJSON = XML.toJSONObject(T_X_S);
			JSONArray tr = xmlJSON.getJSONObject("table").optJSONArray("tr");
			if(tr==null){
				return null;
			}
			if(tr.length()>2){
				return null;
			}
			for(int i = 1; i < tr.length() ; i++){
				JSONObject arr2 = tr.getJSONObject(i).getJSONObject("td");
				JSONObject o1 = arr2.getJSONObject("a");
				String r1 = o1.getString("href");
				return r1;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getKnownAs(String fullName){
		try{
			String r = retrieveKnownAs(fullName);
			String T_X_S = httpGet(r);
			if(T_X_S==null){
				return null;
			}
			JSONObject xmlJSON = XML.toJSONObject(T_X_S);
			JSONArray tr = xmlJSON.getJSONObject("table").optJSONArray("tr");
			if(tr==null){
				return null;
			}
			if(tr.length()>2){
				return null;
			}
			for(int i = 1; i < tr.length() ; i++){
				JSONObject arr2 = tr.getJSONObject(i).getJSONObject("td");
				JSONObject o1 = arr2.getJSONObject("a");
				String r1 = o1.getString("href");
				return r1;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static List<Triple> getResourceFrom(String resource){
		List<Triple> resultList = new ArrayList<Triple>();
		try{
			String r = retrieveResourceFrom(resource);
			String T_X_S = httpGet(r);
			if(T_X_S==null){
				return null;
			}
			JSONObject xmlJSON = XML.toJSONObject(T_X_S);
			JSONArray tr = xmlJSON.getJSONObject("table").optJSONArray("tr");
			if(tr==null){
				return null;
			}
//			System.out.println(tr.toString());
			for(int i = 1; i < tr.length() ; i++){
				JSONArray arr2 = tr.getJSONObject(i).getJSONArray("td");
				JSONObject o1 = arr2.getJSONObject(0).getJSONObject("a");
				String r1 = o1.getString("href");
				JSONObject o2 = arr2.optJSONObject(1);
				String r2 = arr2.optString(1).replace("\"", "").replace("@en", "");
				if(o2!=null){
					r2 = o2.getJSONObject("a").getString("href");
				}
				
//				System.out.println(r1 + " : " + r2);
				resultList.add(new Triple(resource, r1, r2));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return resultList;
	}

	
	private static String constructSparqlQuery(String entityName,int offset){
        StringBuilder sb = new StringBuilder();
        sb.append("http://dbpedia.org/sparql?default-graph-uri=http://dbpedia.org&query=");
        sb.append("select+distinct+?person+?type+?label+?abstract+?disambiguate+?wiki+where+{+");
        sb.append("?person+rdfs:label+?label.+");
        sb.append("OPTIONAL+{+?person+<http://dbpedia.org/ontology/abstract>+?abstract.+FILTER+langMatches(+lang(?abstract),+\"EN\"+)+}+");
        sb.append("OPTIONAL+{+?person+rdf:type+?type.+}+");
        sb.append("OPTIONAL+{+?disambiguate+<http://dbpedia.org/ontology/wikiPageDisambiguates>+?person.+}+");
        sb.append("OPTIONAL+{+?wiki+<http://dbpedia.org/ontology/wikiPageRedirects>+?person.+}+");
        sb.append("?label+bif:contains+'\""+entityName+"\"'@en.+");
        sb.append("FILTER+(!regex(?person,\"http://dbpedia.org/resource/Category:\",\"i\")+%26%26+regex(?person,\"http://dbpedia.org/resource/\",\"i\")+%26%26+langMatches(+lang(?label),+\"EN\"+))+}");	   
        sb.append("+LIMIT+2000+OFFSET+"+offset);
        sb.append("&format=text/html&timeout=30000");
        System.out.println(sb.toString());
		return sb.toString();
	}
	
	private String getEntityString(String entityName,int offset){
        StringBuilder sb = new StringBuilder();
        sb.append("select distinct ?person ?type ?label ?abstract ?disambiguate ?wiki where { ");
        sb.append("?person rdfs:label ?label.");
        sb.append("OPTIONAL{?person <http://dbpedia.org/ontology/abstract> ?abstract. FILTER langMatches( lang(?abstract), \"EN\" ) } ");
        sb.append("OPTIONAL { ?person rdf:type ?type. } ");
        sb.append("OPTIONAL { ?disambiguate <http://dbpedia.org/ontology/wikiPageDisambiguates> ?person. } ");
        sb.append("OPTIONAL { ?wiki <http://dbpedia.org/ontology/wikiPageRedirects> ?person. } ");
        sb.append("?label bif:contains '\""+entityName+"\"'@en. ");
        sb.append("FILTER (!regex(?person,\"http://dbpedia.org/resource/Category:\",\"i\") && regex(?person,\"http://dbpedia.org/resource/\",\"i\") && langMatches(lang(?label),\"EN\"))}");	   
        sb.append("LIMIT 2000 OFFSET "+offset);
		return sb.toString();
	}
	
	private static String countDisambiguateQuery(String entityName){
        StringBuilder sb = new StringBuilder();
        sb.append("http://dbpedia.org/sparql?default-graph-uri=http://dbpedia.org&query=");
        sb.append("select+count(?x)+where+{+");
        sb.append("<http://dbpedia.org/resource/"+entityName+">+dbpedia-owl:wikiPageDisambiguates+?x.+");
        sb.append("}&format=text/html&timeout=30000");
        return sb.toString();
	}
	
	private static String retrievePartial(String entityName){
        StringBuilder sb = new StringBuilder();
        sb.append("http://dbpedia.org/sparql?default-graph-uri=http://dbpedia.org&query=");
        sb.append("select+distinct+?x+where+{+");
        sb.append("?x+rdfs:label+?label.+");
        sb.append("?label+bif:contains+'\""+entityName+"\"'@en.+");
        sb.append("}");   
        sb.append("&format=text/html&timeout=30000");
		return sb.toString();
	}
	
	private static String retrieveDescriptionPartial(String entityName,String fullName){
		String pluralName = Noun.pluralOf(entityName)==null?entityName:Noun.pluralOf(entityName);
		String pluralFullName = Noun.pluralOf(fullName)==null?fullName:Noun.pluralOf(fullName);
        StringBuilder sb = new StringBuilder();
        System.out.println(entityName + " : " + pluralFullName);
        sb.append("http://dbpedia.org/sparql?default-graph-uri=http://dbpedia.org&query=");
        sb.append("select+distinct+?x+where+{+");
        sb.append("?x+dbpedia-owl:abstract+?abstract.");
        sb.append("?x+dbpprop:shortDescription+?sd.+");
        sb.append("?sd+bif:contains+'\""+entityName.replaceAll("\\s+", "+")+"\"+OR+\""+pluralName.replaceAll("\\s+", "+")+"\"'@en.+");
        sb.append("?abstract+bif:contains+'\""+fullName.replaceAll("\\s+", "+")+"\"+OR+\""+pluralFullName.replaceAll("\\s+", "+")+"*\"'@en.+");
        sb.append("}");   
        sb.append("&format=text/html&timeout=30000");
		return sb.toString();
	}
	
	private static String retrieveKnownAs(String fullName){
		String knownAs = "known as";
        StringBuilder sb = new StringBuilder();
        sb.append("http://dbpedia.org/sparql?default-graph-uri=http://dbpedia.org&query=");
        sb.append("select+distinct+?x+where+{+");
        sb.append("?x+dbpedia-owl:abstract+?abstract.");
        sb.append("?abstract+bif:contains+'\""+knownAs.replaceAll("\\s+", "+")+"\"+AND+\""+fullName.replaceAll("\\s+", "+")+"\"'@en.+");
        sb.append("}");   
        sb.append("&format=text/html&timeout=30000");
		return sb.toString();
	}
	
	private static String retrieveResourceFrom(String resource){
        StringBuilder sb = new StringBuilder();
        sb.append("http://dbpedia.org/sparql?default-graph-uri=http://dbpedia.org&query=");
        sb.append("select+distinct+?p+?o+where+{+");
        sb.append("<"+resource+">+?p+?o+.+");
        sb.append("filter(lang(?o)='en'+%7C%7C+lang(?o)=''+%7C%7C+regex(?o,\"http://dbpedia.org/resource/\"))+");
        sb.append("}");   
//        sb.append("+LIMIT+10000+OFFSET+"+offset);
        sb.append("&format=text/html&timeout=30000");
//        System.out.println(sb.toString());
		return sb.toString();
	}


	
	public static String httpGet(String urlStr) {
		try{
			URL url = new URL(urlStr);
			HttpURLConnection conn =
					(HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Accept-Charset", "UTF-8");
	
			BufferedReader rd = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line + '\n');
			}
			rd.close();
	
			conn.disconnect();
			return sb.toString();
		}catch(Exception e){
		return null;	
		}
	}
	
	public static void main(String[] args) {
		DbpediaService ds = new DbpediaService();
		System.out.println(Noun.pluralOf("sister"));
		List<DbpediaResult> e =ds.get("Mona");
			System.out.println(e.size());
		
//		ds.getDeneme("British Museum");
	}

}
