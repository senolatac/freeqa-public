package etu.edu.tr.freeqa.nlp.disambiguate;

import java.util.List;

import etu.edu.tr.commons.DependencyWord;



public class SearchText {
	private String text;
	private String clearFullText;
	private String fullText;
	private List<String> fullIndexTagList;
	private List<String> fullFrontTagList;
	private DependencyWord word;
	private DependencyWord indexText;
	private String type;
	private int dwIndex;
	
	public SearchText(){
		
	}
	
	public SearchText(String text,String fullText,String clearFullText,List<String> fullIndexTagList,List<String> fullFrontTagList, DependencyWord word, DependencyWord indexText,String type,int dwIndex) {
		super();
		this.text = text;
		this.fullText = fullText;
		this.clearFullText=clearFullText;
		this.word = word;
		this.indexText = indexText;
		this.type = type;
		this.dwIndex=dwIndex;
		this.fullIndexTagList=fullIndexTagList;
		this.fullFrontTagList=fullFrontTagList;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public DependencyWord getWord() {
		return word;
	}
	public void setWord(DependencyWord word) {
		this.word = word;
	}

	public DependencyWord getIndexText() {
		return indexText;
	}

	public void setIndexText(DependencyWord indexText) {
		this.indexText = indexText;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getDwIndex() {
		return dwIndex;
	}

	public void setDwIndex(int dwIndex) {
		this.dwIndex = dwIndex;
	}

	public String getClearFullText() {
		return clearFullText;
	}

	public void setClearFullText(String fullText) {
		this.clearFullText = fullText;
	}

	public List<String> getFullIndexTagList() {
		return fullIndexTagList;
	}

	public void setFullIndexTagList(List<String> fullIndexTagList) {
		this.fullIndexTagList = fullIndexTagList;
	}

	public List<String> getFullFrontTagList() {
		return fullFrontTagList;
	}

	public void setFullFrontTagList(List<String> fullFrontTagList) {
		this.fullFrontTagList = fullFrontTagList;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	
	

}

