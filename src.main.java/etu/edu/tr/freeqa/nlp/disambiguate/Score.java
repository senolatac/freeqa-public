package etu.edu.tr.freeqa.nlp.disambiguate;


public class Score {
	private double resembleRatio;
	private boolean isFrontInclude;
	private boolean isBackInclude;
	private boolean isTypeInclude;
	private boolean isAbstractInclude;
	private boolean isMaxResemble;
	private boolean isSynonymInclude;
	private boolean isLabelInclude;
	private double disambiguateRatio;
	private double wikiScore;
	private String resource;
	private String label;
	private DbpediaResult dbPediaResult;
	private SearchText searchText;
	private double score;
	
	public Score(SearchText searchText,String resource,String label,DbpediaResult dbPediaResult,double resembleRatio, boolean isFrontInclude,boolean isTypeInclude,boolean isAbstractInclude,boolean isBackInclude,double disambiguateRatio,double wikiRatio,boolean isSynonymInclude,boolean isLabelInclude) {
		this.searchText=searchText;
		this.resembleRatio = resembleRatio;
		this.isFrontInclude = isFrontInclude;
		this.isTypeInclude = isTypeInclude;
		this.resource = resource;
		this.label=label;
		this.dbPediaResult=dbPediaResult;
		this.isAbstractInclude=isAbstractInclude;
		this.isBackInclude = isBackInclude;
		this.disambiguateRatio=disambiguateRatio;
		this.wikiScore=wikiRatio;
		this.isSynonymInclude=isSynonymInclude;
		this.isLabelInclude = isLabelInclude;
	}
	public double getResembleRatio() {
		return resembleRatio;
	}
	public void setResembleRatio(double resembleRatio) {
		this.resembleRatio = resembleRatio;
	}
	public boolean isFrontInclude() {
		return isFrontInclude;
	}
	public void setFrontInclude(boolean isFrontInclude) {
		this.isFrontInclude = isFrontInclude;
	}
	public boolean isTypeInclude() {
		return isTypeInclude;
	}
	public void setTypeInclude(boolean isTypeInclude) {
		this.isTypeInclude = isTypeInclude;
	}
	public boolean isMaxResemble() {
		return isMaxResemble;
	}
	public void setMaxResemble(boolean isMaxResemble) {
		this.isMaxResemble = isMaxResemble;
	}
	public String getResource() {
		return resource;
	}
	public void setResource(String resource) {
		this.resource = resource;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public boolean isAbstractInclude() {
		return isAbstractInclude;
	}
	public void setAbstractInclude(boolean isAbstractInclude) {
		this.isAbstractInclude = isAbstractInclude;
	}
	public boolean isBackInclude() {
		return isBackInclude;
	}
	public void setBackInclude(boolean isBackInclude) {
		this.isBackInclude = isBackInclude;
	}
	public double getWikiScore() {
		return wikiScore;
	}
	public void setWikiScore(double wikiScore) {
		this.wikiScore = wikiScore;
	}
	public double getDisambiguateRatio() {
		return disambiguateRatio;
	}
	public void setDisambiguateRatio(double disambiguateRatio) {
		this.disambiguateRatio = disambiguateRatio;
	}
	public boolean isSynonymInclude() {
		return isSynonymInclude;
	}
	public void setSynonymInclude(boolean isSynonymInclude) {
		this.isSynonymInclude = isSynonymInclude;
	}
	public SearchText getSearchText() {
		return searchText;
	}
	public void setSearchText(SearchText searchText) {
		this.searchText = searchText;
	}
	public DbpediaResult getDbPediaResult() {
		return dbPediaResult;
	}
	public void setDbPediaResult(DbpediaResult dbPediaResult) {
		this.dbPediaResult = dbPediaResult;
	}
	public boolean isLabelInclude() {
		return isLabelInclude;
	}
	public void setLabelInclude(boolean isLabelInclude) {
		this.isLabelInclude = isLabelInclude;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	
	

}
