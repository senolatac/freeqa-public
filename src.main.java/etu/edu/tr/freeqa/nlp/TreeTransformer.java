package etu.edu.tr.freeqa.nlp;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.clearnlp.dependency.DEPNode;
import com.clearnlp.dependency.DEPTree;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import etu.edu.tr.commons.Dependency;
import etu.edu.tr.commons.DependencyWord;
import etu.edu.tr.commons.Question;

/** citation by HAWK*/ 
public class TreeTransformer {
	Logger log = LoggerFactory.getLogger(TreeTransformer.class);

	static int i = 0;

	
	public DependencyTree semanticGraphToDependencyTree(SemanticGraph smg,Question q) {
		DependencyTree tree = new DependencyTree();
		 i = 0;
		addNodeRecursivly(tree, tree.head,smg, smg.getFirstRoot(),q);
		return tree;
	}
	
	
	private DependencyTreeNode addNodeRecursivly(DependencyTree tree, DependencyTreeNode parent,SemanticGraph smg, IndexedWord word,Question q) {

		DependencyTreeNode newParent = null;
		if (parent == null) {
			newParent = new DependencyTreeNode(word.value(), word.tag(), word.originalText(), null, i, word.lemma());
            newParent.word = findWordInDependincies(word.value(), q);
            newParent.targetList = findDependency(word.value(), q);
			//			System.out.println(depNode.docID() + " : " + depNode.toPrimes() + " : " + depNode.labelFactory().toString() + " : " +depNode.backingLabel().after());
			tree.head = newParent;
		} else {
			newParent = new DependencyTreeNode(word.value(), word.tag(), word.originalText(), parent, i,word.lemma());
			newParent.word = findWordInDependincies(word.value(), q);
			newParent.targetList = findDependency(word.value(), q);
			//			System.out.println(newParent.toString());
			parent.addChild(newParent);
		}
		for (IndexedWord tmpChilds : smg.getChildren(word)) {
			i++;
			addNodeRecursivly(tree, newParent,smg, tmpChilds,q);
		}
		return newParent;
	}
	
	private DependencyWord findWordInDependincies(String label,Question q){
		for(DependencyWord dw : q.dependencyList){
			if(dw.getSource().trim().toLowerCase(Locale.ENGLISH).contains(label.trim().toLowerCase(Locale.ENGLISH))){
				return dw;
			}
		}
		return null;
	}
	
	private List<String> findDependency(String label,Question q){
		List<String> rLst = new ArrayList<String>();
		for(Dependency d : q.dependencies){
			if(d.getSource().equals(label)){
				rLst.add(d.getTarget());
			}
		}
		return rLst;
	}
}
