package etu.edu.tr.freeqa.nlp;

import java.util.Locale;

import etu.edu.tr.commons.Question;
import etu.edu.tr.freeqa.nlp.stanford.StanfordNLP;

public class SentenceAnalyze {
	
	public static void main(String[] args) {
		String sentence = "When find ir";
		if(sentence.toLowerCase(Locale.ENGLISH).startsWith("when")){
			System.out.println("OK.");
		}
	}
	
	private StanfordNLP stanfordNLP;
	
	public SentenceAnalyze(StanfordNLP stanfordNLP){
		if(stanfordNLP==null){
			stanfordNLP = new StanfordNLP();
		}
		this.stanfordNLP = stanfordNLP;
	}
	
	public Question analyze(Question question){
		question.startsWithDate = startsWithDate(question);
		findTypeOfQuestion(question);
		findComparatorOfQuestion(question);
		return question;
	}
	
	public boolean startsWithDate(Question question){
		for(DateType d : DateType.values()){
			if(question.languageToQuestion.get("en").trim().toLowerCase().replace("\\s+", " ").startsWith(d.value)){
				return true;
			}
		}
		return false;
	}
	
	public void findTypeOfQuestion(Question question){
		String sentence = stanfordNLP.lemmatizeSentence(question.languageToQuestion.get("en").trim());
		for(TypeOfQuestion t : TypeOfQuestion.values()){
			if(sentence.toLowerCase(Locale.ENGLISH).startsWith(t.getValue())){
				question.typeOfQuestion = t;
			}
		}
	}
	
	public void findComparatorOfQuestion(Question question){
		String sentence = stanfordNLP.lemmatizeSentence(question.languageToQuestion.get("en").trim());
		for(ComparaterOfQuestion t : ComparaterOfQuestion.values()){
			if(sentence.toLowerCase(Locale.ENGLISH).contains(t.getValue())){
				question.comporatorOfQuestion = t;
			}
		}
	}
	
	public static enum DateType{
		HOW_OLD("how old","http://dbpedia.org/ontology/birthYear"),
		HOW_LONG("how long",""),
		WHEN("when","");
		
		private final String value;
		private final String replaceValue;

	    private DateType(String value,String replaceValue) {
	        this.value = value;
	        this.replaceValue=replaceValue;
	    }

	    public String getValue() {
	        return value;
	    }
	    
	    public String getReplaceValue() {
			return replaceValue;
		}

		public String toString(){
			return value;
	    	
	    }
	}
	
	public static enum TypeOfQuestion{
		YES_NO("be"),
		DURATION("how long"),
		TIME("when"),
		LOCATION("where"),
		PERSON("who");
		
		private final String value;

	    private TypeOfQuestion(String value) {
	        this.value = value;
	    }

	    public String getValue() {
	        return value;
	    }
	    
		public String toString(){
			return value;
	    	
	    }
	}
	
	public static enum ComparaterOfQuestion{
		SAME("same"),
		GREAT("great"),
		MORE("more"),
		SMALL("small");
		
		private final String value;

	    private ComparaterOfQuestion(String value) {
	        this.value = value;
	    }

	    public String getValue() {
	        return value;
	    }
	    
		public String toString(){
			return value;
	    	
	    }
	}

}
