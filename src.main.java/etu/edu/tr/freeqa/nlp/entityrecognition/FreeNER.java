package etu.edu.tr.freeqa.nlp.entityrecognition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Stack;

import com.hp.hpl.jena.rdf.model.impl.ResourceImpl;

import etu.edu.tr.commons.DependencyWord;
import etu.edu.tr.commons.Entity;
import etu.edu.tr.commons.Lemma;
import etu.edu.tr.commons.Question;
import etu.edu.tr.commons.Triple;
import etu.edu.tr.freeqa.nlp.disambiguate.DbpediaResult;
import etu.edu.tr.freeqa.nlp.disambiguate.DbpediaService;
import etu.edu.tr.freeqa.nlp.disambiguate.Score;
import etu.edu.tr.freeqa.nlp.disambiguate.SearchText;


public class FreeNER{
	
	DbpediaService dbPediaService;
	
	public FreeNER() {
		this.dbPediaService = new DbpediaService();
	}

	public Map<String, List<Entity>> getEntities(String question) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Entity resourceDisambiguate(Question q ,String searchText){
		SearchText st = findSearchText(q, searchText);
		return disambiguateFromSearchText(st,searchText);
	}
	
	public boolean entityIsRight(Question q ,String searchText){
		DependencyWord dw = findTextInWordList(q.dependencyList, searchText);
		if(dw==null){
			return true;
		}
		String src = dw.getSource().substring(dw.getSource().indexOf(searchText));
		SearchText st = findSearchText(q, searchText);
		Entity tmp = fullDescriptionPartialTextAsEntity(st);
		if(tmp!=null){
			return false;
		}
		
		if(src.equals(searchText)){
			return true;
		}
		return !dbPediaService.labelIsExist(src.trim().replaceAll("\\s+", " "));
	}
	
	public Map<String, List<Entity>> disambiguate(List<DependencyWord> wordList){
		HashMap<String, List<Entity>> tmp = new HashMap<String, List<Entity>>();
		Entity e = this.disambiguateAsEntity(wordList);
	  	ArrayList<Entity> tmpList = new ArrayList<>();
		tmpList.add(e);
	  	tmp.put("en", tmpList);
	  	return tmp;
	  	
	}
	
	public Entity disambiguateAsEntity(List<DependencyWord> wordList){
		SearchText srcText = findSearchText(wordList);
		return disambiguateFromSearchText(srcText,srcText.getWord().getSource());
	  	
	}
	
	public Entity disambiguateFromSearchText(SearchText srcText,String label){
		List<Score> scoreList = new ArrayList<Score>();
		Entity tmp = fullDescriptionPartialTextAsEntity(srcText);
		if(tmp!=null){
			return tmp;
		}
		
		tmp = fullPartialTextAsEntity(srcText);
		if(tmp!=null){
			return tmp;
		}
		
		if(!srcText.getWord().getNerList().contains("PERSON")){
		tmp = knownAsTextAsEntity(srcText);
		if(tmp!=null){
			return tmp;
		}
		}
		
		double maxScore = 0;
		double maxFinalScore = 0;
		System.out.println(srcText.getText().trim().replaceAll("\\s+", " "));
		List<DbpediaResult> resultList =  dbPediaService.get(srcText.getText().trim().replaceAll("\\s+", " "));
		for(DbpediaResult db : resultList){
	  		Score score = new Score(srcText,db.getResource(),db.getLabel(),db, howResemble(srcText.getText().trim(), db.getResource()), isIncludeFrontText(srcText, db.getLabel(),db.getResource()), isIncludeType(srcText, db),isAbstractContain(srcText, db),isIncludeBackText(srcText, db.getLabel()),getDisambiguatationRatio(srcText, db),db.getWikiList().size()*0.001,isIncludeSynonymText(srcText, db.getLabel()),isLabelContains(srcText.getWord(), db.getLabel()));
	  		maxScore = findMaxScore(maxScore, score.getResembleRatio());
	  		scoreList.add(score);
	  	}
	  	List<Score> finalList = new ArrayList<Score>();
	  	for(int i=0; i<scoreList.size(); i++){
	  		if(scoreList.get(i).getResembleRatio()==maxScore){
	  			scoreList.get(i).setMaxResemble(true);
	  		}else{
	  			scoreList.get(i).setMaxResemble(false);
	  		}
	  		double score = 0;
	  		if(scoreList.get(i).isFrontInclude()){
	  			++score;
	  		}
	  		if(scoreList.get(i).isBackInclude()){
	  			scoreList.get(i).setResembleRatio(1);
	  			++score;
	  		}
	  		if(scoreList.get(i).isTypeInclude()){
	  			++score;
	  		}
	  		if(scoreList.get(i).isAbstractInclude()){
	  			++score;
	  		}
	  		if(scoreList.get(i).isSynonymInclude()){
	  			++score;
	  		}
	  		if(scoreList.get(i).isLabelInclude()){
	  			scoreList.get(i).setResembleRatio(1);
	  			++score;
	  		}
	  		if(scoreList.get(i).getResource().contains("(") && scoreList.get(i).getResource().contains(")")){
	  			--score;
	  		}
	  		scoreList.get(i).setScore(score+scoreList.get(i).getResembleRatio()+scoreList.get(i).getWikiScore()+scoreList.get(i).getDisambiguateRatio());
	  		maxFinalScore = findMaxScore(maxFinalScore, scoreList.get(i).getScore());
	  		scoreList.set(i, scoreList.get(i));
	  	}
	  	for(Score score:scoreList){
	  		if((score.getScore()==maxFinalScore) && (score.isFrontInclude() || score.isBackInclude() || score.isSynonymInclude() || score.getResembleRatio()>0.4)){
	  			finalList.add(score);
	  		}
	  	}
	  	if(finalList.isEmpty()){
		  	for(Score score:scoreList){
		  		if((score.getResembleRatio()==1)){
		  			finalList.add(score);
		  		}
		  	}
	  	}
	  	Entity ent = new Entity();
	  	if(!finalList.isEmpty()){
	  		String r = finalList.get(0).getResource();
	  		String rLabel = r.replace("http://dbpedia.org/resource/", "").replace("-", " ").replace("_", " ");
	  		if(srcText.getWord().getSource().contains(rLabel) ){
	  			label = rLabel;
	  		}else if(finalList.get(0).getLabel().equalsIgnoreCase(srcText.getWord().getClearText())){
	  			label = srcText.getWord().getSource();
	  		}
		  	ent.label = label;
			ResourceImpl e = new ResourceImpl(r.replaceAll(",", "%2C").replace(".", "%2E"));
		  	ent.uris.add(e);
	  	}
	  	return ent;
	  	
	}
	
	private String wikiPageRedirect(String uri){
			List<Triple> fromList =  DbpediaService.getResourceFrom(uri);
			for(Triple t:fromList){
				if(t.getPredicate().trim().equals("http://dbpedia.org/ontology/wikiPageRedirects")){
					return t.getObject();
				}
			}
			return uri;
	}
	
	private HashMap<String, List<Entity>> fullPartialText(SearchText srcText){
		HashMap<String, List<Entity>> tmp = new HashMap<String, List<Entity>>();
		List<Score> scoreList = new ArrayList<Score>();
		String partialResource =  DbpediaService.getPartial(srcText.getClearFullText().trim());
		if(partialResource!=null){
			List<Triple> fromList =  DbpediaService.getResourceFrom(partialResource);
			for(Triple t:fromList){
				if(t.getPredicate().trim().equals("http://dbpedia.org/ontology/wikiPageRedirects")){
					partialResource = t.getObject();
					break;
				}
			}
			fromList.clear();
			scoreList.add(new Score(srcText, partialResource,partialResource,new DbpediaResult(), 1, true, true, true, true, 1, 1, true,true));
		}
		if(!scoreList.isEmpty()){
		  	ArrayList<Entity> tmpList = new ArrayList<>();
		  	Entity ent = new Entity();
		  	ent.label = srcText.getClearFullText().trim();
			ResourceImpl e = new ResourceImpl(scoreList.get(0).getResource().replaceAll(",", "%2C").replace(".", "%2E"));
		  	ent.uris.add(e);
		  	tmpList.add(ent);
		  	tmp.put("en", tmpList);
		}
	  	return tmp;
	}
	
	private Entity fullPartialTextAsEntity(SearchText srcText){
		List<Score> scoreList = new ArrayList<Score>();
		String partialResource =  DbpediaService.getPartial(srcText.getClearFullText().trim());
		if(partialResource!=null){
			List<Triple> fromList =  DbpediaService.getResourceFrom(partialResource);
			for(Triple t:fromList){
				if(t.getPredicate().trim().equals("http://dbpedia.org/ontology/wikiPageRedirects")){
					partialResource = t.getObject();
					break;
				}
			}
			fromList.clear();
			scoreList.add(new Score(srcText, partialResource,partialResource,new DbpediaResult(), 1, true, true, true, true, 1, 1, true,true));
		}
		Entity ent = new Entity();
		if(!scoreList.isEmpty()){
		  	ent.label = srcText.getClearFullText().trim();
			ResourceImpl e = new ResourceImpl(scoreList.get(0).getResource().replaceAll(",", "%2C").replace(".", "%2E"));
		  	ent.uris.add(e);
		  	return ent;
		}
	  	return null;
	}
	
	private HashMap<String, List<Entity>> fullDecriptionPartialText(SearchText srcText){
		HashMap<String, List<Entity>> tmp = new HashMap<String, List<Entity>>();
		List<Score> scoreList = new ArrayList<Score>();
		String partialResource =  DbpediaService.getDescriptionPartial(srcText.getText().trim(),srcText.getFullText().trim());
		if(partialResource!=null){
			List<Triple> fromList =  DbpediaService.getResourceFrom(partialResource);
			for(Triple t:fromList){
				if(t.getPredicate().trim().equals("http://dbpedia.org/ontology/wikiPageRedirects")){
					partialResource = t.getObject();
					break;
				}
			}
			fromList.clear();
			scoreList.add(new Score(srcText, partialResource,partialResource,new DbpediaResult(), 1, true, true, true, true, 1, 1, true,true));
		}
		if(!scoreList.isEmpty()){
		  	ArrayList<Entity> tmpList = new ArrayList<>();
		  	Entity ent = new Entity();
		  	ent.label = srcText.getFullText().trim();
			ResourceImpl e = new ResourceImpl(scoreList.get(0).getResource().replaceAll(",", "%2C").replace(".", "%2E"));
		  	ent.uris.add(e);
		  	tmpList.add(ent);
		  	tmp.put("en", tmpList);
		}
	  	return tmp;
	}
	
	private Entity fullDescriptionPartialTextAsEntity(SearchText srcText){
		List<Score> scoreList = new ArrayList<Score>();
		System.err.println(srcText.getText().trim() +" : " +srcText.getClearFullText().trim());
		String partialResource =  DbpediaService.getDescriptionPartial(srcText.getText().trim(),srcText.getClearFullText().trim());
		if(partialResource!=null){
			List<Triple> fromList =  DbpediaService.getResourceFrom(partialResource);
			for(Triple t:fromList){
				if(t.getPredicate().trim().equals("http://dbpedia.org/ontology/wikiPageRedirects")){
					partialResource = t.getObject();
					break;
				}
			}
			fromList.clear();
			scoreList.add(new Score(srcText, partialResource,partialResource,new DbpediaResult(), 1, true, true, true, true, 1, 1, true,true));
		}
		Entity ent = new Entity();
		if(!scoreList.isEmpty()){
		  	ent.label = srcText.getFullText().trim();
			ResourceImpl e = new ResourceImpl(scoreList.get(0).getResource().replaceAll(",", "%2C").replace(".", "%2E"));
		  	ent.uris.add(e);
		  	return ent;
		}
	  	return null;
	}
	
	private HashMap<String, List<Entity>> knownAsText(SearchText srcText){
		HashMap<String, List<Entity>> tmp = new HashMap<String, List<Entity>>();
		List<Score> scoreList = new ArrayList<Score>();
		String partialResource =  DbpediaService.getKnownAs(srcText.getWord().getSource().trim());
		if(partialResource!=null){
			List<Triple> fromList =  DbpediaService.getResourceFrom(partialResource);
			for(Triple t:fromList){
				if(t.getPredicate().trim().equals("http://dbpedia.org/ontology/wikiPageRedirects")){
					partialResource = t.getObject();
					break;
				}
			}
			fromList.clear();
			scoreList.add(new Score(srcText, partialResource,partialResource,new DbpediaResult(), 1, true, true, true, true, 1, 1, true,true));
		}
		if(!scoreList.isEmpty()){
		  	ArrayList<Entity> tmpList = new ArrayList<>();
		  	Entity ent = new Entity();
		  	ent.label = srcText.getWord().getSource().trim();
			ResourceImpl e = new ResourceImpl(scoreList.get(0).getResource().replaceAll(",", "%2C").replace(".", "%2E"));
		  	ent.uris.add(e);
		  	tmpList.add(ent);
		  	tmp.put("en", tmpList);
		}
	  	return tmp;
	}
	
	private Entity knownAsTextAsEntity(SearchText srcText){
		List<Score> scoreList = new ArrayList<Score>();
		String partialResource =  DbpediaService.getKnownAs(srcText.getWord().getSource().trim());
		if(partialResource!=null){
			List<Triple> fromList =  DbpediaService.getResourceFrom(partialResource);
			for(Triple t:fromList){
				if(t.getPredicate().trim().equals("http://dbpedia.org/ontology/wikiPageRedirects")){
					partialResource = t.getObject();
					break;
				}
			}
			fromList.clear();
			scoreList.add(new Score(srcText, partialResource,partialResource,new DbpediaResult(), 1, true, true, true, true, 1, 1, true,true));
		}
		Entity ent = new Entity();
		if(!scoreList.isEmpty()){
		  	ent.label = srcText.getWord().getSource().trim();
			ResourceImpl e = new ResourceImpl(scoreList.get(0).getResource().replaceAll(",", "%2C").replace(".", "%2E"));
		  	ent.uris.add(e);
		  	return ent;
		}
	  	return null;
	}
	

	
	private static SearchText findSearchText(List<DependencyWord> wordList){
		List<SearchText> searchList = new ArrayList<SearchText>();
		for(DependencyWord dw : wordList){
		String searchText = dw.getClearText();
		List<String> nerList = dw.getNerList();
		if(!isAllSameType(nerList)){
			searchText = findNerWord(dw);
		}
		if(searchText!=null){
			DependencyWord d = getClearTextFromList(wordList, dw);
			SearchText st= new SearchText(searchText,getFullTextFromList(wordList, dw, d),getClearFullTextFromList(wordList, dw, d),getFullIndexTagList(dw, d),getFullFrontTagList(dw, d), dw, d,findNerType(dw),wordList.indexOf(dw));
			if(st.getType()!=null){
				return st;
			}else if(Character.isUpperCase(st.getText().trim().charAt(0))){
				return st;
			}
			searchList.add(st);
		}
		}
		return searchList.isEmpty()?null:searchList.get(0);
	}
	
	private static boolean isAllSameType(List<String> nerList){
		if(nerList==null || nerList.isEmpty()){
			return false;
		}else if(nerList.contains("O") || nerList.contains("DATE") || nerList.contains("ORDINAL")){
			return false;
		}else{
			for(String s:nerList){
				if(!s.equals(nerList.get(0))){
					return false;
				}
			}
			return true;
		}
	}
	
	private static String findNerWord(DependencyWord word){
		String type = null;
		String clearWord = "";
		for(int i=0; i<word.getNerList().size(); i++){
			if(!word.getNerList().get(i).equals("O") && !word.getNerList().get(i).equals("ORDINAL") && !word.getNerList().get(i).equals("DATE") && type==null){
				type=word.getNerList().get(i);
			}
			
			if(type!=null && word.getNerList().get(i).equals(type)){
				clearWord = clearWord + " " + word.getLemmaList().get(i).getWord();
			}else if(type!=null && !word.getNerList().get(i).equals(type)){
				break;
			}
		}
		if(!clearWord.equals("")){
			return clearWord;
		}
		type = null;
		boolean nnpContains = false;
		if(!word.getTagList().isEmpty() && word.getTagList().contains("NNP")){
			nnpContains = true;
		}
		for(int i=0; i<word.getTagList().size(); i++){
			if(nnpContains && word.getTagList().get(i).equals("NNP") && type==null){
				type=word.getTagList().get(i);
			}else if(!nnpContains && (word.getTagList().get(i).equals("NN") || word.getTagList().get(i).equals("NNS") || word.getTagList().get(i).equals("NNP")) && type==null){
				type=word.getTagList().get(i);
			}
			
			if(type!=null && word.getTagList().get(i).equals(type)){
				clearWord = clearWord + " " + word.getLemmaList().get(i).getWord();
			}else if(type!=null && !word.getTagList().get(i).equals(type)){
				break;
			}
		}
		if(!clearWord.equals("")){
			return clearWord;
		}
		return null;
		
	}
	
	private static double howResemble(String searchText,String resource){
		resource = resource.replace("http://dbpedia.org/resource/", "");
		return (double)searchText.trim().length()/resource.trim().length();
	}
	
	private static boolean isTypeResemble(String searchText,String type){
		type = type.substring(type.lastIndexOf("/")+1).replace("/", "");
		if(type.toLowerCase().equals(searchText.trim().toLowerCase())||type.toLowerCase().equals(searchText.trim().toLowerCase()+"s")){
			return true;
		}else{
			return false;
		}
	}
	
	public static DependencyWord getClearTextFromList(List<DependencyWord> wordList,DependencyWord dw){
		int index = wordList.indexOf(dw);
		if(index>=(wordList.size()-1)){
			return null;
		}else if((wordList.get(index+1).getTagList().size()==1 && wordList.get(index+1).getTagList().get(0).equals("IN")) || wordList.get(index+1).getClearText().equals("")){
			return getClearTextFromList(wordList, wordList.get(index+1));
		}else{
			return wordList.get(index+1);
		}
		
	}
	
	private static boolean isIncludeFrontText(SearchText searchText,String label,String resource){
		if(searchText.getIndexText()==null || searchText.getIndexText().getClearText()==null){
			return false;
		}
		label = label.replace("\"", "").replace("@en", "").replace(searchText.getText(), "");
		if(isIncludeFrontResource(searchText, resource)){
			return true;
		}else if(label.contains(searchText.getIndexText().getClearText())){
			label = label.replace(searchText.getIndexText().getClearText(), "").replace(" of ", "").trim();
			if(label.equals("")){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	private static boolean isIncludeFrontResource(SearchText searchText,String resource){
		if(searchText.getIndexText()==null || searchText.getIndexText().getClearText()==null){
			return false;
		}
		resource = resource.replace("http://dbpedia.org/resource/", "").toLowerCase(new Locale("en")).trim();
		resource = resource.replaceAll("_of_", "").replaceAll("_in_", "");
		String frontText = searchText.getIndexText().getClearText().trim().replaceAll("\\s+", "_");
		if(resource.contains(frontText)){
			return true;
		}else{
			return false;
		}
	}
	
	private static boolean isIncludeBackText(SearchText searchText,String label){
		label = label.toLowerCase(Locale.ENGLISH).replace("\"", "").replace("@en", "").replace(searchText.getText().trim().toLowerCase(Locale.ENGLISH), "");
		String st = searchText.getWord().getSource().toLowerCase(Locale.ENGLISH).replace(searchText.getText().trim().toLowerCase(Locale.ENGLISH), "").trim();
		if(!st.equals("")&&label.contains(st)){
			label = label.replace(st, "").replace(" of ", "").trim();
			if(label.equals("")){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	private static boolean isIncludeSynonymText(SearchText searchText,String label){
		label = label.replace("\"", "").replace("@en", "").replace(searchText.getText(), "");
		for(Lemma l:searchText.getWord().getLemmaList()){
			for(String s:l.getSnonymList()){
				if(label.contains(s)){
					return true;
				}
			}
		}
		return false;
	}
	
	private static String findNerType(DependencyWord dw){
		for(String ner:dw.getNerList()){
			if(!ner.equals("O") && !ner.equals("ORDINAL") && !ner.equals("DATE")){
				return ner;
			}
		}
		return null;
	}
	
	private static boolean isIncludeType(SearchText searchText,DbpediaResult result){
		if(searchText.getType()==null){
			return false;
		}
		for(String type:result.getTypeList()){
			if(type.toLowerCase(new Locale("en")).contains(searchText.getType().toLowerCase(new Locale("en")))){
				return true;
			}
		}
		return false;
	}
	
	private static double findMaxScore(double maxScore,double score){
		if(maxScore<score){
			return score;
		}else{
			return maxScore;
		}
	}
	
	private static boolean isAbstractContain(SearchText searchText,DbpediaResult result ){
		if(searchText.getIndexText()==null || searchText.getIndexText().getClearText()==null){
			return false;
		}
		return result.getAbstractValue().toLowerCase(new Locale("en")).contains(searchText.getIndexText().getClearText().toLowerCase(new Locale("en")));
	}
	
	private static double getDisambiguatationRatio(SearchText searchText,DbpediaResult result ){
		if(searchText.getText()==null){
			return 0;
		}
		String text = searchText.getText().replace(" ", "_");
		double r =0;
		for(String dis:result.getDisambiguateList()){
			dis = dis.replace("http://dbpedia.org/resource/", "");
			if(dis.toLowerCase(new Locale("en")).contains(text.toLowerCase(new Locale("en")))){
				if(((double)text.trim().length()/dis.trim().length())>r){
					r=(double)text.trim().length()/dis.trim().length();
				}
			}
		}
		return r;
	}
	
	public static String getClearFullTextFromList(List<DependencyWord> wordList,DependencyWord indexDw,DependencyWord frontDw){
		String result = "";
		for(int i=wordList.indexOf(frontDw); i>=wordList.indexOf(indexDw); i--){
			result = result + wordList.get(i).getClearText() + " ";
		}
		return result.trim();
		
	}
	
	public static String getFullTextFromList(List<DependencyWord> wordList,DependencyWord indexDw,DependencyWord frontDw){
		String result = "";
		if(frontDw==null || frontDw.getSource()==null || frontDw.getSource().equals("")){
			return indexDw.getSource();
		}
		for(int i=wordList.indexOf(frontDw); i>=wordList.indexOf(indexDw); i--){
			result = result + wordList.get(i).getSource() + " ";
		}
		return result.trim();
		
	}
	
	public static List<String> getFullIndexTagList(DependencyWord indexDw,DependencyWord frontDw){
		List<String> tagList = new ArrayList<String>();
		tagList.addAll(indexDw.getClearTagList());
        if(frontDw!=null &&frontDw.getClearTagList()!=null &&!frontDw.getClearTagList().isEmpty()){
        	tagList.addAll(frontDw.getClearTagList());
        }
		return tagList;
		
	}
	
	public static List<String> getFullFrontTagList(DependencyWord indexDw,DependencyWord frontDw){
		List<String> tagList = new ArrayList<String>();
		tagList.addAll(indexDw.getClearTagList());
        if(frontDw!=null &&frontDw.getClearTagList()!=null &&!frontDw.getClearTagList().isEmpty()){
        	tagList.clear();
        	tagList.addAll(frontDw.getClearTagList());
        	tagList.addAll(indexDw.getClearTagList());
        }
		return tagList;
		
	}
	
	private SearchText findSearchText(Question q, String searchText){
		DependencyWord dw = findTextInWordList(q.dependencyList, searchText);
		DependencyWord d = getClearTextFromList(q.dependencyList, dw);
		if(dw==null){
			return null;
		}
		SearchText st= new SearchText(searchText,getFullTextFromList(q.dependencyList, dw, d),getClearFullTextFromList(q.dependencyList, dw, d),getFullIndexTagList(dw, d),getFullFrontTagList(dw, d), dw, d,findNerType(dw),q.dependencyList.indexOf(dw));
	    return st;
	}
	
	private DependencyWord findTextInWordList(List<DependencyWord> wordList,String text){
		for(DependencyWord dw : wordList){
			if(dw.getSource().contains(text)){
				return dw;
			}
		}
		return null;
	}
	
	private boolean isLabelContains(DependencyWord dw,String label){
		String dwLabel = dw.getSource().trim().replaceAll("\\s+", " ");
		String dwLemma = dw.getClearText().trim().replaceAll("\\s+", " ");
		label = label.replace(",", "").replace("&", "and").replaceAll("\\s+", " ");
		if(label.equalsIgnoreCase(dwLabel)){
			return true;
		}else if(label.equalsIgnoreCase(dwLemma)){
			return true;
		}
		return false;
	}

}
