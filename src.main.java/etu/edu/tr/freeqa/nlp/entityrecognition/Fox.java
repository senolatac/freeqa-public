package etu.edu.tr.freeqa.nlp.entityrecognition;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFReader;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.rdf.model.impl.ResourceImpl;

import etu.edu.tr.commons.Entity;
import etu.edu.tr.commons.Question;
import etu.edu.tr.freeqa.query.SPARQL;

/** citation by HAWK*/ 
public class Fox {
	static Logger log = LoggerFactory.getLogger(Fox.class);

	private String requestURL = "http://139.18.2.164:4444/api";
	private String outputFormat = "N-Triples";
	private String taskType = "NER";
	private String inputType = "text";
	private SPARQL sparql;
	private Question question ; 
	private FreeNER freeNER;
	
	public Fox(){
		this.sparql = new SPARQL();
		this.freeNER = new FreeNER();
	}
	
	public void setQuestion(Question question) {
		this.question = question;
	}
	

	protected String requestPOST(String input, String requestURL) {
		try {

			String output = POST(input, requestURL);

			return output;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String POST(String urlParameters, String requestURL) throws MalformedURLException, IOException, ProtocolException {
		URL url = new URL(requestURL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setUseCaches(false);
		connection.setRequestProperty("Accept", "application/json");
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
		connection.setRequestProperty("Content-Length", String.valueOf(urlParameters.length()));

		DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();

		InputStream inputStream = connection.getInputStream();
		InputStreamReader in = new InputStreamReader(inputStream);
		BufferedReader reader = new BufferedReader(in);

		StringBuilder sb = new StringBuilder();
		while (reader.ready()) {
			sb.append(reader.readLine());
		}

		wr.close();
		reader.close();
		connection.disconnect();

		return sb.toString();
	}

	@Override
	public String toString() {
		String[] name = getClass().getName().split("\\.");
		return name[name.length - 1].substring(0, 3);
	}

	private String doTASK(String inputText) throws MalformedURLException, IOException, ProtocolException {

		String urlParameters = "type=" + inputType;
		urlParameters += "&task=" + taskType;
		urlParameters += "&output=" + outputFormat;
		urlParameters += "&input=" + URLEncoder.encode(inputText, "UTF-8");

		return requestPOST(urlParameters, requestURL);
	}


	public Map<String, List<Entity>> getEntities(String question) {
		HashMap<String, List<Entity>> tmp = new HashMap<String, List<Entity>>();
		try {
			String foxJSONOutput = doTASK(question);

			JSONParser parser = new JSONParser();
            JSONObject jsonArray = (JSONObject) parser.parse(foxJSONOutput);
            String output = URLDecoder.decode((String) jsonArray.get("output"), "UTF-8");

			String baseURI = "http://dbpedia.org";
			Model model = ModelFactory.createDefaultModel();
			RDFReader r = model.getReader("N3");
			r.read(model, new StringReader(output), baseURI);

			ResIterator iter = model.listSubjects();
			ArrayList<Entity> tmpList = new ArrayList<>();
			while (iter.hasNext()) {
				Resource next = iter.next();
				StmtIterator statementIter = next.listProperties();
				Entity ent = new Entity();
				while (statementIter.hasNext()) {
					Statement statement = statementIter.next();
					String predicateURI = statement.getPredicate().getURI();
					if (predicateURI.equals("http://www.w3.org/2000/10/annotation-ns#body")) {
						ent.label = statement.getObject().asLiteral().getString();
					} else if (predicateURI.equals("http://ns.aksw.org/scms/means")) {
						String uri = statement.getObject().asResource().getURI();
						if(!uri.contains(baseURI)){
							uri = baseURI + "/resource/" + uri.substring(uri.lastIndexOf("/")+1);
							QueryExecution qe = sparql.qef.createQueryExecution("SELECT ?x WHERE { <"+uri+"> dbpedia-owl:wikiPageRedirects ?x . }");
								if (qe != null) {
									ResultSet results = qe.execSelect();
									while (results.hasNext()) {
										uri = results.next().get("?x").asResource().getURI();
									}
								}
						}
						String encode = uri.replaceAll(",", "%2C").replace(".", "%2E");
						ResourceImpl e = new ResourceImpl(encode);
						ent.uris.add(e);
					} else if (predicateURI.equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")) {
						ent.posTypesAndCategories.add(statement.getObject().asResource());
					}
				}
				if(!freeNER.entityIsRight(this.question, ent.label)){
					System.out.println("Right de�il...");
					Entity en = freeNER.resourceDisambiguate(this.question, ent.label);
					if(en!=null && !en.uris.isEmpty()){
						ent = en;
					}
				}
				tmpList.add(ent);
			}
			
			tmp.put("en", tmpList);

		} catch (IOException | ParseException e) {
			log.error("Could not call FOX for NER/NED", e);
		}
		if (!tmp.isEmpty()) {
			log.debug("\t" + Joiner.on("\n").join(tmp.get("en")));
		}
		return tmp;
	}

}
