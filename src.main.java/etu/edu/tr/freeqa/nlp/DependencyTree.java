package etu.edu.tr.freeqa.nlp;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** citation by HAWK*/ 
public class DependencyTree implements Serializable,Cloneable {
	private static final long serialVersionUID = 1286195006804443794L;
	static Logger log = LoggerFactory.getLogger(DependencyTree.class);
	DependencyTreeNode head = null;

	public DependencyTreeNode getRoot() {
		return head;
	}

	public boolean remove(DependencyTreeNode target) {

		if (target.equals(head)) {
			if (head.children.size() == 1) {
				head = head.children.get(0);
				return true;
			} else {
				// more than one child on to be removed root
				log.error("More than one child on to be removed root. Need to rebalance tree or something.");
				return false;
			}
		} else {
			List<DependencyTreeNode> children = target.children;
			DependencyTreeNode parent = target.parent;
			List<DependencyTreeNode> parentsChildren = parent.children;
			parentsChildren.addAll(children);
			for(DependencyTreeNode grandchild: children){
				grandchild.parent=parent;
			}
			parentsChildren.remove(target);
			return true;
		}
	}
	
	public void set(DependencyTreeNode target) {

		if (target.equals(head)) {
			head = target;
		} else {
			List<DependencyTreeNode> children = target.children;
			DependencyTreeNode parent = target.parent;
			List<DependencyTreeNode> parentsChildren = parent.children;
			parentsChildren.addAll(children);
			for(DependencyTreeNode grandchild: children){
				grandchild.parent=parent;
			}
			parentsChildren.set(parentsChildren.indexOf(target), target);
		}
	}
	
	@Override
	public Object clone(){
		DependencyTree t =new DependencyTree();
		t.head = (DependencyTreeNode) this.head.clone();
		return t;
	}

	@Override
	public String toString() {
		return TreeTraversal.inorderTraversal(head, 0, null);
	}

}
