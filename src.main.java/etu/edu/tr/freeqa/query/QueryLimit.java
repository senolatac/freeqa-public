package etu.edu.tr.freeqa.query;

import java.util.Set;
import java.util.Stack;

import com.google.common.collect.Sets;

import etu.edu.tr.commons.Question;
import etu.edu.tr.freeqa.nlp.DependencyTreeNode;

/** citation by HAWK*/ 
public class QueryLimit {
	
	Set<String> blackList = Sets.newHashSet("person","people","be","did","do","when","and","while","have","take","HOW_MANY","has","tv series");
	Set<String> typeBlackList = Sets.newHashSet("who","where");
	
	public int cardinality(Question q) {
		// look at the first child of root and determine the quality based on
		// the POS Tag
		int cardinality = 15;
		DependencyTreeNode root = q.tree.getRoot();
		// IN because of "In which..."
		if (root.posTag.matches("VB(.)*")) {
			DependencyTreeNode firstChild = root.children.get(0);
			String posTag = firstChild.posTag;
			if (posTag.equals("NNS")) {
				cardinality = 15;
			} else if (posTag.matches("WP||WRB||ADD||NN||VBZ||IN")) {
				cardinality = 1;
			} else if (posTag.matches("IN")) {
				DependencyTreeNode secondChild = firstChild.getChildren().get(0);
				posTag = secondChild.posTag;
				if (posTag.equals("NN")) {
					cardinality = 1;
				} else {
					cardinality = 15;
				}
			} else {
				cardinality = 15;
			}

		} else {
			String posTag = root.posTag;
			if (posTag.matches("NNS||NNP(.)*")) {
				cardinality = 15;
			} else {
				cardinality = 1;
			}
		}
		return cardinality;
	}
	
	public int depthOfTree(Question q){
		int depth = 0;
		Stack<DependencyTreeNode> stack = new Stack<DependencyTreeNode>();
		stack.push(q.tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			if (!blackList.contains(tmp.lemma) && !(typeBlackList.contains(tmp.lemma)&&tmp.getClassAnnotations().isEmpty())) {
				++depth;
			} 
			
			for (DependencyTreeNode child : tmp.getChildren()) {
				stack.push(child);
			}

		}
		return depth;
	}

}
