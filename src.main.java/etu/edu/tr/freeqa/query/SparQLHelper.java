package etu.edu.tr.freeqa.query;

import java.net.SocketException;
import java.net.SocketTimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.QueryExecution;

import etu.edu.tr.commons.Question;
import etu.edu.tr.freeqa.nlp.DependencyTreeNode;

/** citation by HAWK*/ 
public class SparQLHelper {
	Logger log = LoggerFactory.getLogger(SparQLHelper.class);
	private SPARQL sparql;
	public boolean isTarget=true;
	public Question question;
	
	public SparQLHelper(SPARQL sparql,Question question){
		this.sparql = sparql;
		this.question = question;
	}
	
	public SPARQLQuery getResourcePropertyRelation(DependencyTreeNode n,SPARQLQuery executableQuery,String resource,String predicate){
		boolean result = false;
		SPARQLQuery eq=null;
		try{
			eq = ((SPARQLQuery) executableQuery.clone());
			String rtnStr = executableQuery.executedQuery;
			
			resource = resource.replace("%2C", ",").replace("%2E", ".");
			String relation = "?var";
			if(executableQuery.executedQuery.contains("?var")){
				relation = relation + System.currentTimeMillis();
			}
			if(rtnStr.contains(resource+" <"+predicate+"> ") || rtnStr.contains(" <"+predicate+"> "+resource)){
				eq.addPropertyConstraint(resource+" <"+predicate+"> "+relation+".");
				eq.addDoubleConstraint(resource+" <"+predicate+"> "+relation+".");
				return eq;
			}
			findUriVar(eq,n, relation,resource);
			
			rtnStr = executableQuery.executedQuery+" "+resource+" <"+predicate+"> "+relation+".";
			eq.executedQuery = rtnStr.trim()+"\n";
			QueryExecution qe = sparql.qef.createQueryExecution("ASK WHERE {"+rtnStr+"}");
			if (qe != null) {
				qe.setTimeout(10000);
				result = qe.execAsk();
				if(result){
					eq.addVar(resource);
					eq.addVar(relation);
					eq.addPropertyConstraint(resource+" <"+predicate+"> "+relation+".");
//					rtnStr = executableQuery.executedQuery+" "+resourceFrom+" <"+predicate+"> "+relation+".";
//					eq.executedQuery = rtnStr.trim()+"\n";
					return eq;
				}
			}
			rtnStr = executableQuery.executedQuery+" "+relation+" <"+predicate+"> "+resource+".";
			eq.executedQuery = rtnStr.trim()+"\n";
			findUriVar(eq,n, resource,relation);
			qe = sparql.qef.createQueryExecution("ASK WHERE {"+rtnStr+"}");
			if (qe != null) {
				qe.setTimeout(10000);
				result = qe.execAsk();
				if(result){
					eq.addVar(relation);
					eq.addVar(resource);
					eq.addPropertyConstraint(relation+" <"+predicate+"> "+resource+".");
//					rtnStr = executableQuery.executedQuery+" "+relation+" <"+predicate+"> "+resourceTo+".";
//					eq.executedQuery = rtnStr.trim()+"\n";
					return eq;
				}
			}
		}catch(Exception e){
           return eq;
		}
		return null;
	}
	
	public SPARQLQuery getResourceClassRelation(DependencyTreeNode n,SPARQLQuery executableQuery,String resource,String predicate){
		boolean result = false;
		try{
			SPARQLQuery eq = ((SPARQLQuery) executableQuery.clone());
			String resourceVar = "?trg";
			if(!resource.contains("http://dbpedia.org/resource/")){
				resourceVar = resource;
			}else if(executableQuery.executedQuery.contains("?trg")){
				resourceVar = resourceVar + System.currentTimeMillis();
			}
			String rtnStr = executableQuery.executedQuery;
			resource = resource.replace("%2C", ",").replace("%2E", ".");
//			eq.addVar(resource);
			findUriVar(eq,n, resource);
			String resourceTo = resource;
			String resourceFrom = resource;
			if(eq.uriVar!=null && eq.uriVar.contains("http://dbpedia.org/resource/") && n.isOutput && executableQuery.executedQuery.contains(resource)){
				String[] consts = executableQuery.executedQuery.split("\\.\n");
				for(String s :consts){
					if(s.trim().startsWith(eq.uriVar)){
						String[] rels = s.split(" ");
						String rln = "?r"+System.currentTimeMillis();
						resourceFrom = eq.uriVar+" "+rln+" "+rels[2]+".\n"+resourceVar + " " + rels[1] + " " + rels[2] + ".\n"; 
						executableQuery.executedQuery = executableQuery.executedQuery.replace(s, resourceFrom);
					}
					
					if(s.trim().endsWith(eq.uriVar)){
						String[] rels = s.split(" ");
						String rln = "?r"+System.currentTimeMillis();
						resourceTo = rels[0] + " " + rels[1] + " " + resourceVar + ".\n" + rels[0] + " " + rln + " " + eq.uriVar;
						executableQuery.executedQuery = executableQuery.executedQuery.replace(s, resourceTo);
					}
				}
					rtnStr = executableQuery.executedQuery;
					eq.uriVar = resourceVar;
			}else{
				resourceVar = resource;
			}
			eq.addVar(resourceVar);
			rtnStr = rtnStr+" "+resourceVar+" a "+predicate+".\n";
			eq.executedQuery = rtnStr.trim()+"\n";
			QueryExecution qe = sparql.qef.createQueryExecution("ASK WHERE {"+rtnStr+"}");
			if (qe != null) {
				qe.setTimeout(10000);
				result = qe.execAsk();
				if(result){
					eq.addClassConstraint(resource+" a "+predicate+".");
					return eq;
				}
			}
		}catch(Exception e){
//			log.error(e.getLocalizedMessage(), e);
		}
		return null;
	}
	
	public SPARQLQuery getResourceAbstractRelation(DependencyTreeNode n,SPARQLQuery executableQuery,String resource,String predicate,String lemma){

		boolean result = false;
		try{
			SPARQLQuery eq = ((SPARQLQuery) executableQuery.clone());
			String rtnStr = executableQuery.executedQuery;
			resource = resource.replace("%2C", ",").replace("%2E", ".");
			predicate = predicate.replace("%2C", ",").replace("%2E", ".").replace("http://dbpedia.org/resource/", "");
			lemma = lemma.replace("%2c", ",").replace("%2e", ".").replace("http://dbpedia.org/resource/", "");
//			if(n.posTag.matches("CombinedNN")){
//				lemma = lemma + "*";
//			}
			String target =resource;
			if(!resource.contains("?")){
				target = "?"+target;
			}
			findUriVar(eq,n, resource);
			String relation = target.replace("<http://dbpedia.org/resource/", "").replace(">", "").replace("_", "")+"Abc";
			if(executableQuery.executedQuery.contains(resource + " <http://dbpedia.org/ontology/abstract> " )){
				rtnStr = executableQuery.executedQuery.replace(relation + " bif:contains '", relation + " bif:contains '(" + "\"" + predicate + "\"" + " OR \"" + lemma + "\"" + ") AND ");
				eq.addAbstract(relation+" bif:contains '(" + "\"" + predicate + "\"" + " OR \"" + lemma + "\"" + ") AND ");
			}else{
				rtnStr = executableQuery.executedQuery+" "+resource+" <http://dbpedia.org/ontology/abstract> "+relation+".\n" + relation + " bif:contains '(" + "\"" + predicate + "\"" + " OR \"" + lemma + "\"" + ")'.";
			    eq.addAbstract(resource+" <http://dbpedia.org/ontology/abstract> "+relation+".");
			}
			eq.executedQuery = rtnStr.trim()+"\n";
			eq.addVar(resource);
			QueryExecution qe = sparql.qef.createQueryExecution("ASK WHERE {"+rtnStr+"}");
			if (qe != null) {
				qe.setTimeout(10000);
				result = qe.execAsk();
				if(result){
					return eq;
				}
			}
		}catch(Exception e){
//			log.error(e.getLocalizedMessage(), e);
		}
		return null;
	}
	
	public SPARQLQuery getResourceContainRelation(DependencyTreeNode n,SPARQLQuery executableQuery,String resource,String predicate,String lemma){

		boolean result = false;
		SPARQLQuery eq = null;
		try{
			eq = ((SPARQLQuery) executableQuery.clone());
			String rtnStr = executableQuery.executedQuery;
			predicate = predicate.replace("%2C", ",").replace("%2E", ".").replace("http://dbpedia.org/resource/", "");
			if(n.posTag.matches("NN(.)*|CombinedNN")){
				lemma = predicate.substring(0, 1).toUpperCase() + predicate.substring(1);
			}else{
				lemma = lemma.replace("%2c", ",").replace("%2e", ".").replace("http://dbpedia.org/resource/", "");
			}
			
			resource = resource.replace("%2C", ",").replace("%2E", ".");
			String target = "?cmd ";
			if(executableQuery.executedQuery.contains("?cmd")){
				target = "?cmd"+System.currentTimeMillis();
			}
			String relation = " ?r ";
			if(executableQuery.executedQuery.contains("?r")){
				relation = "?r"+System.currentTimeMillis();
			}
			if(executableQuery.executedQuery.contains("FILTER ( ")){
				rtnStr = executableQuery.executedQuery.replace("FILTER ( ", resource+" "+relation+" "+target+".\n"+"FILTER ( "+"(CONTAINS(str("+target.trim()+") , \""+predicate + "\" ) OR CONTAINS(str("+target.trim()+") , \""+lemma + "\" )) AND ");
			}else{
				rtnStr = executableQuery.executedQuery+resource+" "+relation+" "+target+".\n"+"FILTER ( "+"(CONTAINS(str("+target.trim()+") , \""+predicate + "\" ) OR CONTAINS(str("+target.trim()+") , \""+lemma + "\" ))).";
			}
			eq.executedQuery = rtnStr.trim()+"\n";
			eq.addVar(resource);
			eq.addVar(target);
			findUriVar(eq,n, resource,target);
			eq.addFilterConstraint("CONTAINS(str("+target.trim()+") , \""+predicate + "\" )");
			QueryExecution qe = sparql.qef.createQueryExecution("ASK WHERE {"+rtnStr+"}");
			if (qe != null) {
				qe.setTimeout(10000);
				result = qe.execAsk();
				if(result){
					return eq;
				}
			}
		}catch(Exception e){
//			if(e instanceof SocketTimeoutException || e.getCause() instanceof SocketTimeoutException || e.getCause().getCause() instanceof SocketTimeoutException){
//				return eq;
//			}else{
//				System.out.println(e.getCause().getCause());
//			}
           
		}
		return null;
	}
	
	private void findUriVar(SPARQLQuery query,DependencyTreeNode n,String... targets){
		if(question.isOutputPredictable){
			if(n.isOutput){
				if(targets.length>1){
					if(isTarget){
						query.uriVar = targets[0];
					}else{
						query.uriVar = targets[1];
					}
				}else{
					query.uriVar = targets[0];
				}
			}
		}else{
			if(targets.length>1){
				if(isTarget){
					query.uriVar = targets[0];
				}else{
					query.uriVar = targets[1];
				}
			}else{
				query.uriVar = targets[0];
			}
		}
	}
	
	public static void main(String[] args) {
		String s = "?var <http://dbpedia.org/ontology/deathPlace> <http://dbpedia.org/resource/Greece>.\n";
		String[] ss = s.split("\\.\n");
		for(String i:ss){
			System.out.println(i);
		}
//		System.out.println(ss.toString());
	}

}
