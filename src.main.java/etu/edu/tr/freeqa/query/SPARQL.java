package etu.edu.tr.freeqa.query;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Set;

import org.aksw.jena_sparql_api.cache.core.QueryExecutionFactoryCacheEx;
import org.aksw.jena_sparql_api.cache.extra.CacheCoreEx;
import org.aksw.jena_sparql_api.cache.extra.CacheCoreH2;
import org.aksw.jena_sparql_api.cache.extra.CacheEx;
import org.aksw.jena_sparql_api.cache.extra.CacheExImpl;
import org.aksw.jena_sparql_api.core.QueryExecutionFactory;
import org.aksw.jena_sparql_api.http.QueryExecutionFactoryHttp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;

/** citation by HAWK*/ 
public class SPARQL implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1018871230058824255L;
	Logger log = LoggerFactory.getLogger(SPARQL.class);
	// TODO treshold can be increased by introducing prefixes
	int sizeOfFilterThreshold = 25;
	public QueryExecutionFactory qef;

	public SPARQL() {
		try {
			long timeToLive = 360l * 24l * 60l * 60l * 1000l;
			CacheCoreEx cacheBackend = CacheCoreH2.create("sparql", timeToLive, true);
			CacheEx cacheFrontend = new CacheExImpl(cacheBackend);
			// AKSW SPARQL API call
			// qef = new
			// QueryExecutionFactoryHttp("http://192.168.15.69:8890/sparql",
			// "http://dbpedia.org/");
//			qef = new QueryExecutionFactoryHttp("http://localhost:3030/ds/sparql");

//			qef = new QueryExecutionFactoryHttp("http://139.18.2.164:3030/ds/sparql");
//			qef = new QueryExecutionFactoryHttp("http://www.sparql.org/sparql","http://dbpedia.org");

//			qef = new QueryExecutionFactoryHttp("http://localhost:3030/ds/sparql");
			 qef = new QueryExecutionFactoryHttp("http://www.dbpedia.org/sparql","http://dbpedia.org");

//			 qef = new QueryExecutionFactoryHttp("http://live.dbpedia.org/sparql","http://dbpedia.org");
//			 qef = new QueryExecutionFactoryHttp("http://lod.openlinksw.com/sparql/","http://dbpedia.org");
			// qef = new
			// QueryExecutionFactoryHttp("http://vtentacle.techfak.uni-bielefeld.de:443/sparql",
			// "http://dbpedia.org");
			// --> No reason to be nice
			// qef = new QueryExecutionFactoryDelay(qef, 2000);
			qef = new QueryExecutionFactoryCacheEx(qef, cacheFrontend);
			// qef = new QueryExecutionFactoryDelay(qef, 150);
			// qef = new QueryExecutionFactoryPaginated(qef, 10000);
		} catch (ClassNotFoundException | SQLException e) {
			log.error("Could not create SPARQL interface! ", e);
		}
	}

	/**
	 * using the AKSW library for wrapping Jena API
	 * 
	 * @param query
	 * @return
	 */
	public Set<RDFNode> sparql(String query) {
		Set<RDFNode> set = Sets.newHashSet();
		try {
			QueryExecution qe = qef.createQueryExecution(query);
			if (qe != null && query.toString() != null) {
				qe.setTimeout(2000);
				ResultSet results = qe.execSelect();
				while (results.hasNext()) {
					set.add(results.next().get("target"));
				}
			}
		} catch (Exception e) {
			log.error(query.toString(), e);
		}
		return set;
	}
	
	public Set<RDFNode> sparql(String query,String uri) {
//		System.out.println("sparql : " + uri + " : " + query);
		Set<RDFNode> set = Sets.newHashSet();
		try {
			QueryExecution qe = qef.createQueryExecution(query);
			if (qe != null && query.toString() != null) {
				ResultSet results = qe.execSelect();
				while (results.hasNext()) {
					set.add(results.next().get(uri.trim()));
				}
			}
		} catch (Exception e) {
			log.error(query.toString(), e);
		}
		return set;
	}
	
	public Set<RDFNode> sparqlJena(String strQuery,String uri){
		Set<RDFNode> set = Sets.newHashSet();
		try{
		String queryString=
				"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
				"PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>"+
				strQuery;

				// now creating query object
				Query query = QueryFactory.create(queryString);
				// initializing queryExecution factory with remote service.
				// **this actually was the main problem I couldn't figure out.**
				QueryExecution qexec = com.hp.hpl.jena.query.QueryExecutionFactory.sparqlService("http://dbpedia.org/sparql", query);

				//after it goes standard query execution and result processing which can
				// be found in almost any Jena/SPARQL tutorial.
				try {
				    ResultSet results = qexec.execSelect();
				    while (results.hasNext()) {
						System.out.println(results.next().get(uri.trim()));
					}
				}
				finally {
				   qexec.close();
				}
		} catch (Exception e) {
			log.error(strQuery.toString(), e);
		}
		return set;
	}
	
	public Set<RDFNode> sparqlBySource(String query) {
		query = query.replace("SELECT DISTINCT ?target WHERE", "SELECT DISTINCT ?source WHERE");
		Set<RDFNode> set = Sets.newHashSet();
		try {
			QueryExecution qe = qef.createQueryExecution(query);
			if (qe != null && query.toString() != null) {
				qe.setTimeout(10000);
				ResultSet results = qe.execSelect();
				while (results.hasNext()) {
					set.add(results.next().get("source"));
				}
			}
		} catch (Exception e) {
			log.error(query.toString(), e);
		}
		return set;
	}

	public static void main(String args[]) {
		SPARQL sqb = new SPARQL();
		String s = "SELECT DISTINCT ?p ?l WHERE { <http://dbpedia.org/resource/Japan> ?p ?o . ?p <http://www.w3.org/2000/01/rdf-schema#label> ?l. filter(lang(?l)='en') }";
		
		Set<RDFNode> set = sqb.sparql(s,"?p ");
		for (RDFNode item : set) {
			System.out.println(item);
		}

//		SPARQLQuery query = new SPARQLQuery();
//		query.addConstraint("?proj a <http://dbpedia.org/ontology/Cleric>.");
//		// query.addConstraint("?proj ?p ?const.");
//		// query.addFilter("proj",
//		// Lists.newArrayList("http://dbpedia.org/resource/Pope_John_Paul_I",
//		// "http://dbpedia.org/resource/Pope_John_Paul_II"));
//		// query.addFilter("const",
//		// Lists.newArrayList("http://dbpedia.org/resource/Canale_d'Agordo"));
//		for (String q : query.generateQueries()) {
//			Set<RDFNode> set = sqb.sparql(q);
//			for (RDFNode item : set) {
//				System.out.println(item);
//			}
//		}
	}
}
