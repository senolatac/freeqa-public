package etu.edu.tr.freeqa.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.ResultSet;

import etu.edu.tr.commons.Entity;
import etu.edu.tr.commons.Question;
import etu.edu.tr.freeqa.nlp.DependencyTreeNode;

/** citation by HAWK*/ 
public class SPARQLQuery implements Cloneable, Serializable, Comparable<SPARQLQuery> {

	private static final long serialVersionUID = 6652694466896208327L;
	// prune by lemma for verbs
	private static HashSet<String> stopwords = Sets.newHashSet("of", "is", "and", "in", "name", "was", "did", "person", "location", "organization");
	public List<String> constraints = Lists.newArrayList();
	public List<String> constraintTriples = Lists.newArrayList();
	public List<String> doubleTriples = Lists.newArrayList();
	public List<String> resourceTriples = Lists.newArrayList();
	public List<String> abstractTriples = Lists.newArrayList();
	public List<String> classConstraintTriples = Lists.newArrayList();
	public List<String> filterTriples = Lists.newArrayList();
	public List<String> wildChirdTriples = Lists.newArrayList();
	public List<String> varList = new ArrayList<String>();
	public List<DependencyTreeNode> nodeList = new ArrayList<DependencyTreeNode>();
	public Map<String, Set<String>> descriptionToken = Maps.newHashMap();
	public String executedQuery = "";
	public String uriVar = "";
	public String targetConstaint;
	public boolean isPossibleAnswer = false;
	private int limit = 1;
	public int depth = 0;
	public int isIncludeResource = 0;
	private Double score = 0.0;
	SPARQL sparQL;

	public SPARQLQuery(String initialConstraint) {
		constraintTriples.add(initialConstraint);
	}
	
	public SPARQLQuery(List<DependencyTreeNode> nodeList){
		this.nodeList = nodeList;
	}
	
	public SPARQLQuery(List<DependencyTreeNode> nodeList,boolean isResource){
		this.nodeList = nodeList;
		this.isIncludeResource = 1;
	}
	
	public SPARQLQuery(List<DependencyTreeNode> nodeList,String query){
		this.nodeList = nodeList;
		this.executedQuery = query;
		this.isIncludeResource = 1;
		this.score = this.score -0.01;
	}

	/**
	 * only for clone()
	 */
	public SPARQLQuery() {
		if(sparQL ==null){
			sparQL = new SPARQL();
		}
	}
	
	public void addVar(String var){
		if(this.varList==null){
			this.varList = new ArrayList<String>();
		}
		if(!varList.contains(var) ){ //&& !var.contains("http://dbpedia.org/resource")
			varList.add(var);
		}
	}
	
	public void addNode(DependencyTreeNode node){
		if(!nodeList.contains(node)){
			nodeList.add(node);
		}
		
	}

	public void addPropertyConstraint(String constraint) {
		if(!constraintTriples.contains(constraint)){
			constraintTriples.add(constraint);
			this.score = this.score + 1.12;
		}
		    
	}
	
	public void addDoubleConstraint(String constraint){
		doubleTriples.add(constraint);
		this.score = this.score + 0.01;
	}
	
	public void addConstaint(String constraint) {
		if(!constraints.contains(constraint) && !constraint.trim().equals(""))
		    constraints.add(constraint);
	}
	
	public void addResource(String resource){
		resourceTriples.add(resource);
		this.score = this.score + 1.05;
	}
	
	public void addAbstract(String resource){
		abstractTriples.add(resource);
		this.score = this.score + 0.9;
	}
	
	public void addClassConstraint(String classConstraint) {
		classConstraintTriples.add(classConstraint);
//		if(classConstraint.contains("http://dbpedia.org/ontology/")){
//			this.score = this.score + 1.14;
//		}else{
			this.score = this.score + 1.11;
//		}
	}

	public void addFilterOverAbstractsContraint(String variable, String label) {
//		fuzzyToken(variable, label);
//		exactToken(variable, label);

	}
	
	public void addFilterOverDescriptionContraint(String variable, String label) {
//		fuzzyToken(variable, label);
		exactDescriptionToken(variable, label);

	}

	
	private void exactDescriptionToken(String variable, String label) {
		if (descriptionToken.containsKey(variable)) {
			Set<String> set = descriptionToken.get(variable);
			set.add(label);
			descriptionToken.put(variable, set);
		} else {
			descriptionToken.put(variable, Sets.newHashSet(label));
		}
	}



	public boolean constraintsContains(String target) {
		for (String c : constraintTriples) {
			if (c.contains(target)) {
				return true;
			}
		}
		return false;
	}

	public void addFilterConstraint(String string) {
		filterTriples.add(string);
		this.score = this.score + 1.1;

	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		SPARQLQuery q = new SPARQLQuery();
		q.constraints = Lists.newArrayList();
		for (String constraint : this.constraints) {
			q.constraints.add(constraint);
		}
		q.resourceTriples = Lists.newArrayList();
		for (String constraint : this.resourceTriples) {
			q.resourceTriples.add(constraint);
		}
		q.constraintTriples = Lists.newArrayList();
		for (String constraint : this.constraintTriples) {
			q.constraintTriples.add(constraint);
		}
		q.classConstraintTriples = Lists.newArrayList();
		for (String constraint : this.classConstraintTriples) {
			q.classConstraintTriples.add(constraint);
		}	
		q.abstractTriples = Lists.newArrayList();
		for (String constraint : this.abstractTriples) {
			q.abstractTriples.add(constraint);
		}	
		q.filterTriples = Lists.newArrayList();
		for (String constraint : this.filterTriples) {
			q.filterTriples.add(constraint);
		}
		q.wildChirdTriples = Lists.newArrayList();
		for (String constraint : this.wildChirdTriples) {
			q.wildChirdTriples.add(constraint);
		}
		q.varList = Lists.newArrayList();
		for (String constraint : this.varList) {
			q.varList.add(constraint);
		}
		q.nodeList = Lists.newArrayList();
		for (DependencyTreeNode constraint : this.nodeList) {
			q.nodeList.add(constraint);
		}
		q.doubleTriples = Lists.newArrayList();
		for (String constraint : this.doubleTriples) {
			q.doubleTriples.add(constraint);
		}
		q.executedQuery = "";
		q.executedQuery = this.executedQuery;
		q.uriVar = "";
		q.uriVar = this.uriVar;
		q.depth = this.depth;
		q.score = 0.0;
		q.score = this.score;
		q.isPossibleAnswer = this.isPossibleAnswer;
		q.isIncludeResource=0;
		q.isIncludeResource = this.isIncludeResource;
		q.targetConstaint = null;
		q.targetConstaint = this.targetConstaint;
		return q;
	}
	

//	@Override
//	protected Object clone() throws CloneNotSupportedException {
//		SPARQLQuery q = new SPARQLQuery();
//		q.resourceTriples = Lists.newArrayList();
//		for (String constraint : this.resourceTriples) {
//			q.resourceTriples.add(constraint);
//		}
//		q.constraintTriples = Lists.newArrayList();
//		for (String constraint : this.constraintTriples) {
//			q.constraintTriples.add(constraint);
//		}
//		q.classConstraintTriples = Lists.newArrayList();
//		for (String constraint : this.classConstraintTriples) {
//			q.classConstraintTriples.add(constraint);
//		}
//		q.filter = Sets.newHashSet();
//		for (String key : this.filter) {
//			q.filter.add(key);
//		}
////		q.textMapFromVariableToSingleFuzzyToken = Maps.newHashMap();
////		for (String key : this.textMapFromVariableToSingleFuzzyToken.keySet()) {
////			Set<String> list = Sets.newHashSet(this.textMapFromVariableToSingleFuzzyToken.get(key));
////			q.textMapFromVariableToSingleFuzzyToken.put(key, list);
////		}
//		q.textMapFromVariableToCombinedNNExactMatchToken = Maps.newHashMap();
//		for (String key : this.textMapFromVariableToCombinedNNExactMatchToken.keySet()) {
//			Set<String> list = Sets.newHashSet(this.textMapFromVariableToCombinedNNExactMatchToken.get(key));
//			q.textMapFromVariableToCombinedNNExactMatchToken.put(key, list);
//		}
//		q.descriptionToken = Maps.newHashMap();
//		for (String key : this.descriptionToken.keySet()) {
//			Set<String> list = Sets.newHashSet(this.descriptionToken.get(key));
//			q.descriptionToken.put(key, list);
//		}
//		return q;
//	}

	@Override
	public String toString() {
		return generateQuery();
	}

	public Set<String> generateQueries() {
		Set<String> set = Sets.newHashSet();
		String exactQuery = generateQuery();
		set.add(exactQuery);
		return set;
	}
	
	public String generateQuery(){
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT DISTINCT "+this.uriVar+" WHERE {\n");
		sb.append(executedQuery);	
		sb.append("}\n");
		return sb.toString();
		
	}
	
	
//	public String generateQuery(){
//		StringBuilder sb = new StringBuilder();
//		sb.append("SELECT DISTINCT ?target WHERE {\n");
//		if(!executedQuery.contains("?target")){
//			sb.append(executedQuery.replace("?source", "?target"));
//		}else{
//			sb.append(executedQuery);	
//		}
//		sb.append("}\n");
//		sb.append("LIMIT " + limit);
//		return sb.toString();
//		
//	}
	
	public String generateAskQuery(){
		StringBuilder sb = new StringBuilder();
		sb.append("ASK WHERE {\n");
		if(!executedQuery.contains("?target")){
			sb.append(executedQuery.replace("?source", "?target"));
		}else{
			sb.append(executedQuery);	
		}
		sb.append("}\n");
		return sb.toString();
		
	}

	// taken from
	// http://stackoverflow.com/questions/237159/whats-the-best-way-to-check-to-see-if-a-string-represents-an-integer-in-java
	private boolean isInteger(String str) {
		if (str == null) {
			return false;
		}
		int length = str.length();
		if (length == 0) {
			return false;
		}
		int i = 0;
		if (str.charAt(0) == '-') {
			if (length == 1) {
				return false;
			}
			i = 1;
		}
		for (; i < length; i++) {
			char c = str.charAt(i);
			if (c <= '/' || c >= ':') {
				return false;
			}
		}
		return true;
	}

	public void setLimit(int cardinality) {
		this.limit = cardinality;

	}

	public void setScore(Double distance) {
		this.score = distance;
	}
	
	public Double getScore() {
		return score;
	}

	public void addAbstract(String variable,DependencyTreeNode resource,Question q){
		String label = resource.label.replace("http://dbpedia%2Eorg/resource/", "").replace("%2C", ",").replace("%2E", ".");
		String lemma = resource.lemma.replace("http://dbpedia%2eorg/resource/", "").replace("%2c", ",").replace("%2e", ".");
		if(resource.word!=null && resource.word.getClearText()!=null && !resource.word.getClearText().trim().equals("")){
			lemma = resource.word.getClearText().replace("http://dbpedia%2eorg/resource/", "").replace("%2c", ",").replace("%2e", ".");
		}
		if(resource.posTag.matches("VB(.)*") && (lemma.contains("_") || lemma.contains(" "))){
			String[] splits = lemma.replace("_", " ").replace("-", " ").split(" ");
			if(splits[splits.length-1].length()>3){
				lemma = lemma + "*";
			}
		}else if(resource.posTag.matches("VB(.)*") && lemma.length()>3){
			lemma = lemma + "*";
		}
		String searchText = "\"" + label + "\" OR \"" + lemma + "\"";
		if(lemma==null || lemma.trim().equals("")){
			searchText = "\"" + label + "\"";
		}
		if(!executedQuery.contains(variable+ " <http://dbpedia.org/ontology/abstract> ")){
			this.isPossibleAnswer=false;
		}
		if(executedQuery.contains(" <http://dbpedia.org/ontology/abstract> ")){
			searchText = "(" + searchText + ") AND ";
			executedQuery = executedQuery.replace("abc"+" bif:contains '", "abc"+" bif:contains '" + searchText);
		}else{
			searchText = "(" + searchText + ")" ;
			StringBuilder fulltext = new StringBuilder();
			fulltext.append(variable + " <http://dbpedia.org/ontology/abstract> " + variable+"abc" +".\n");
			fulltext.append(variable+"abc"+" bif:contains '");
			fulltext.append(searchText);
			fulltext.append("'" + ". \n");
			this.addConstaint(variable + " <http://dbpedia.org/ontology/abstract> " + variable+"abc.");
			executedQuery = executedQuery + fulltext.toString();
		}
		if(searchText.contains("*\"")){
			this.wildChirdTriples.add(searchText);
		}
		
	}
	
	public void addAbstract(String variable,String resource,Question q){
		String label = resource.replace("http://dbpedia%2Eorg/resource/", "").replace("%2C", ",").replace("%2E", ".");
		String lemma = resource.replace("http://dbpedia%2eorg/resource/", "").replace("%2C", ",").replace("%2E", ".");
//		if(lemma.contains("_")){
//			String[] splits = lemma.replace("_", " ").replace("-", " ").split(" ");
//			if(splits[splits.length-1].length()>3){
//				lemma = lemma + "*";
//			}
//		}else if(lemma.length()>3){
//			lemma = lemma + "*";
//		}
		if(!executedQuery.contains(variable+ " <http://dbpedia.org/ontology/abstract> ")){
			this.isPossibleAnswer=false;
		}
		if(executedQuery.contains(" <http://dbpedia.org/ontology/abstract> ")){
			executedQuery = executedQuery.replace("abc"+" bif:contains '", "abc"+" bif:contains '(" + "\"" + label + "\"" + ") AND ");
		}else{
			StringBuilder fulltext = new StringBuilder();
			fulltext.append(variable + " <http://dbpedia.org/ontology/abstract> " + variable+"abc" +".\n");
			fulltext.append(variable+"abc"+" bif:contains '");
			fulltext.append("(" + "\"" + label + "\"" + ")");
			fulltext.append("'" + ". \n");
			this.addConstaint(variable + " <http://dbpedia.org/ontology/abstract> " + variable+"abc.");
			executedQuery = executedQuery + fulltext.toString();
		}
	}
	
	public void addProperty(String source,String target,String resource){
		if(!executedQuery.contains(source + " <"+resource+"> " + target)){
			StringBuilder fulltext = new StringBuilder();
			if(constraintTriples.size()>0){
				fulltext.append(source + " <"+resource+"> " + target+constraintTriples.size() +".");
			}else{
				fulltext.append(source + " <"+resource+"> " + target +".");
			}
			this.addConstaint(fulltext.toString());
			executedQuery = executedQuery + fulltext.toString()+"\n";
		}
	}
	
	public void addType(String source,String resource){
		StringBuilder fulltext = new StringBuilder();
		fulltext.append(source + " a " + " <"+resource+">" + ".");
		this.addConstaint(fulltext.toString());
		executedQuery = executedQuery + fulltext.toString()+"\n";
    }
	public void addResourceTo(String source,String resource){
		StringBuilder fulltext = new StringBuilder();
		String relation = "?r ";
		if(executedQuery.contains("?r")){
			relation = "?r"+System.currentTimeMillis() +" ";
		}
		fulltext.append(source + " "+relation + "<"+resource+">" + ".");
		this.addConstaint(fulltext.toString());
		executedQuery = executedQuery + fulltext.toString()+"\n";
    }
	
	public void addResourceFrom(String source,String resource){
		StringBuilder fulltext = new StringBuilder();
		String relation = "?r ";
		if(executedQuery.contains("?r")){
			relation = "?r"+System.currentTimeMillis() +" ";
		}else{
			fulltext.append("<"+resource+"> " + relation +source + ".");
		}
		this.addConstaint(fulltext.toString());
		executedQuery = executedQuery + fulltext.toString()+"\n";
    }
	
	public void addContainFilter(String source , String filterStr){
		StringBuilder fulltext = new StringBuilder();
		String target = source+"cmd ";
		if(executedQuery.contains(source+"cmd")){
			target = " " + source+"cmd"+System.currentTimeMillis() +" ";
		}
		String relation = " ?r ";
		if(executedQuery.contains("?r")){
			relation = " ?r"+System.currentTimeMillis() +" ";
		}
		fulltext.append(source + relation + target + ".\n");
		if(executedQuery.contains("FILTER ( ")){
			executedQuery = executedQuery.replace("FILTER ( ", "FILTER ( "+"CONTAINS(str("+target.trim()+") , \""+filterStr + "\") AND ");
		}else{
			fulltext.append("FILTER ( "+"CONTAINS(str("+target.trim()+") , \""+filterStr + "\" )).\n");
		}
		executedQuery = executedQuery + fulltext.toString();
//		this.addConstaint(source + relation + target+".");
		this.addFilterConstraint("CONTAINS(str("+target+") , \""+filterStr + "\")");
	}
	
	public void addFilter(String filterStr){
		if(executedQuery.contains("FILTER ( ")){
			executedQuery = executedQuery.replace("FILTER ( ", "FILTER ( "+filterStr + " AND ");
		}else{
			StringBuilder fulltext = new StringBuilder();
			fulltext.append("FILTER ( " + filterStr + " ).\n");
			executedQuery = executedQuery + fulltext.toString();
		}
	}
	
	@Override
	public int compareTo(SPARQLQuery o2) {
		return ComparisonChain.start().compare(o2.score, this.score).result();
//		return ComparisonChain.start().compare(this.score, o2.score).compare(this.toString(), o2.toString()).result();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + this.executedQuery.hashCode();
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final SPARQLQuery other = (SPARQLQuery) obj;
		if (!this.executedQuery.equals(other.executedQuery)) {
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		PriorityQueue<SPARQLQuery> queue = new PriorityQueue<>(100);
		SPARQLQuery sq = new SPARQLQuery();
		sq.addPropertyConstraint("");
		sq.executedQuery ="asd";
		SPARQLQuery sq1 = new SPARQLQuery();
		sq1.addAbstract("");
		sq1.executedQuery ="asd1";
		sq.addPropertyConstraint("");
		SPARQLQuery sq2 = new SPARQLQuery();
		sq2.addAbstract("");
		sq2.addAbstract("");
		sq2.executedQuery ="asd2";
		sq2.addPropertyConstraint("");
		SPARQLQuery sq3 = new SPARQLQuery();
		sq3.addAbstract("");
		sq3.addAbstract("");
		sq3.executedQuery ="asd3";
		sq3.addPropertyConstraint("");
		queue.add(sq);
		queue.add(sq1);
		queue.add(sq2);
		queue.add(sq3);
		while(!queue.isEmpty()){
			SPARQLQuery s = queue.poll();
			System.out.println(s.score + ": " +s);
		}
	}
}
