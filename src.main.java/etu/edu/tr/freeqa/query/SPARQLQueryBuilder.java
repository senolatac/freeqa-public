package etu.edu.tr.freeqa.query;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;

import etu.edu.tr.commons.Answer;
import etu.edu.tr.commons.Question;
import etu.edu.tr.freeqa.ranking.RankingAnswer;

/** citation by HAWK*/ 
public class SPARQLQueryBuilder {
	private static Logger log = LoggerFactory.getLogger(SPARQLQueryBuilder.class);
	private SPARQL sparql;
	private RankingAnswer rankingAnswer;

	public SPARQLQueryBuilder(SPARQL sparql) {
		this.sparql = sparql;
		this.rankingAnswer = new RankingAnswer(sparql);
	}

	public Map<String, Answer> build(Question q) {
		Map<String, Answer> answer = Maps.newHashMap();
		try {
			// build sparql queries
			Set<SPARQLQuery> queryStrings = new HashSet<SPARQLQuery>(q.queryList);



			// identify the cardinality of the answers
			int cardinality = cardinality(q, queryStrings);
			log.debug("Cardinality:" + q.languageToQuestion.get("en").toString() + "-> " + cardinality);
			for (SPARQLQuery query : queryStrings) {
				for (String queryString : query.generateQueries()) {
					Answer a = new Answer();
						a.answerSet = sparql.sparql(queryString, query.uriVar);
					a.query = query;
					if (!a.answerSet.isEmpty()) {
						answer.put(queryString, a);
					}
				}
			}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		} finally {
			System.gc();
		}
		return rankingAnswer.rankAnswers(answer,q);
	}

	private int cardinality(Question q, Set<SPARQLQuery> queryStrings) {
		int cardinality = q.cardinality;
		// find a way to determine the cardinality of the answer

		for (SPARQLQuery s : queryStrings) {
			s.setLimit(cardinality);
		}
		return cardinality;
	}

}