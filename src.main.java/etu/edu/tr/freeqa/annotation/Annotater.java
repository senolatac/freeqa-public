package etu.edu.tr.freeqa.annotation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

import etu.edu.tr.commons.Entity;
import etu.edu.tr.commons.Question;
import etu.edu.tr.commons.Triple;
import etu.edu.tr.freeqa.lemon.DBOIndex;
import etu.edu.tr.freeqa.lemon.IndexDBO_classes;
import etu.edu.tr.freeqa.lemon.IndexDBO_properties;
import etu.edu.tr.freeqa.nlp.DependencyTree;
import etu.edu.tr.freeqa.nlp.DependencyTreeNode;
import etu.edu.tr.freeqa.nlp.similarity.WordNetService;
import etu.edu.tr.freeqa.nlp.stanford.StanfordNLP;
import etu.edu.tr.freeqa.nlp.stanford.StanfordNLP.QuestionType;
import etu.edu.tr.freeqa.query.SPARQL;

/** citation by HAWK*/ 
public class Annotater {
	Logger log = LoggerFactory.getLogger(Annotater.class);
	IndexDBO_classes classesIndex = new IndexDBO_classes();
	IndexDBO_properties propertiesIndex = new IndexDBO_properties();
//	DBOIndex dboIndex = new DBOIndex();
	// blacklisting is a bad solution but good for ambiguous nouns like "people"
	Set<String> blacklist = Sets.newHashSet("people");
	private SPARQL sparql;
	private StanfordNLP stanfordNLP;
	Question question;
	AnnotaterPruner pruner;

	public Annotater(SPARQL sparql) {
		this.sparql = sparql;
//		this.stanfordNLP = new StanfordNLP();
	}

	public void annotateTree(Question q,StanfordNLP stanfordNLP) {
		this.question = q;
		this.pruner = new AnnotaterPruner(sparql);
		if(stanfordNLP==null){
			stanfordNLP = new StanfordNLP();
		}
		this.stanfordNLP = stanfordNLP;
		DependencyTree tree = this.question.tree;
		annotateQuestionTypes(tree);
		annotateProjectionLeftTree(tree);
		annotateVerbs(tree);
		annotateNouns(tree);
		annotateResources();
		annotateProperties();
		pruner.prune(this.question);
		
	}
	
	private void annotateProperties(){
		Stack<DependencyTreeNode> stack = new Stack<>();
		stack.push(question.tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			String posTag = tmp.posTag;
			if (posTag.matches("NN(.)*|CombinedNN")) {
				List<String> search = null;
				if(tmp.word!=null){
					search = findProperty(tmp.word.getSource().trim());
//					search = findProperty(tmp.label);
					List<String> typeSearch = rankAsType(search);
					search = rank(search);
					for (String uri : search) {
						if(uri!=null && !uri.trim().equals("") && !containsCaseInsensitive(uri,tmp.getAnnotations())){
							tmp.addAnnotation(uri);
							addResource(tmp,uri);
						}
						
					}	
					for (String uri : typeSearch) {
						if(uri!=null && !uri.trim().equals("") && !containsCaseInsensitive(uri,tmp.getClassAnnotations())){
							tmp.addClassAnnotation(uri);
							addResource(tmp,uri);
						}
						
					}
				}
				boolean isContainResource = false;
				if(search!=null){
					for(String s:search){
						if(s.contains("http://dbpedia.org/resource/")){
							isContainResource = true;
							break;
						}
					}
				}
				search = findProperty(tmp.lemma);
				
				List<String> typeSearch = rankAsType(search);
				search = rank(search);
				for (String uri : search) {
					if(uri!=null && !uri.trim().equals("") && !containsCaseInsensitive(uri,tmp.getAnnotations())){
						if(!(isContainResource && uri.contains("http://dbpedia.org/resource/"))){
							tmp.addAnnotation(uri);
							addResource(tmp,uri);
						}
					}
					
				}
				for (String uri : typeSearch) {
					if(uri!=null && !uri.trim().equals("") && !containsCaseInsensitive(uri,tmp.getClassAnnotations())){
						tmp.addClassAnnotation(uri);
						addResource(tmp,uri);
					}
					
				}

				log.debug(Joiner.on(", ").join(tmp.getAnnotations()));
			}
			for (DependencyTreeNode child : tmp.getChildren()) {
				stack.push(child);
			}
		}
	}
	
	private void annotateQuestionTypes(DependencyTree tree){
		Stack<DependencyTreeNode> stack = new Stack<>();
		stack.push(tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			String label = tmp.label;
			for(QuestionType type : QuestionType.values()){
				if(label.equals(type.name())){
					tmp.addAnnotation(type.getReplaceValue());
				}
			}

			for (DependencyTreeNode child : tmp.getChildren()) {
				stack.push(child);
			}
		}
	}

	
	
	private void annotateResources(){
		List<Triple> tripleList = new ArrayList<Triple>();
		for(Entity e : question.languageToNamedEntites.get("en")){
			if(e.uris!=null && !e.uris.isEmpty()){
			List<Triple> propertyList = this.getProperties(e.uris.get(0).getURI());
			if(!propertyList.isEmpty()){
				tripleList.addAll(propertyList);
			}
			}
		}
		Stack<DependencyTreeNode> stack = new Stack<>();
		stack.push(question.tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			String posTag = tmp.posTag;
			if (posTag.matches("NN(.)*|CombinedNN")) {
				List<String> search = searchRelatedWords(tripleList, tmp.lemma, posTag);
				if (search.isEmpty() && tmp.lemma != null) {
					search = searchRelatedWords(tripleList, tmp.lemma, posTag);
				}
				search = rankResource(search, question.languageToNamedEntites.get("en"));
				for (String uri : search) {
					if(uri!=null && !uri.trim().equals("") && !containsCaseInsensitive(uri,tmp.getAnnotations())){
						tmp.addAnnotation(uri);
						addResource(tmp,uri);
					}
					
				}
				log.debug(Joiner.on(", ").join(tmp.getAnnotations()));
			}
			for (DependencyTreeNode child : tmp.getChildren()) {
				stack.push(child);
			}
		}
	}
	
	 /** citation by HAWK*/ 
	private void annotateNouns(DependencyTree tree) {
		Stack<DependencyTreeNode> stack = new Stack<>();
		stack.push(tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			String label = tmp.label;
			String posTag = tmp.posTag;
			if (!blacklist.contains(label)) {

				if (posTag.matches("NN(.)*|CombinedNN") && tmp.getAnnotations().isEmpty()) {
					ArrayList<String> search = classesIndex.search(label.replace("_", "").replace(" ", "").trim());
					if (!search.isEmpty()) {
						for (String uri : search) {
							if(uri!=null && !uri.trim().equals("") && !containsCaseInsensitive(uri,tmp.getClassAnnotations())){
								tmp.addClassAnnotation(uri);
								addResource(tmp,uri);
							}
						}
					} else if (!propertiesIndex.search(label).isEmpty()) {
						search = propertiesIndex.search(label);
						for (String uri : search) {
							if(uri!=null && !uri.trim().equals("") && !containsCaseInsensitive(uri,tmp.getAnnotations())){
								tmp.addAnnotation(uri);
								addResource(tmp,uri);
							}
							
						}
					} 
//					else {
//						search = dboIndex.search(label);
//						for (String uri : search) {
//							if(uri!=null && !uri.trim().equals(""))
//							tmp.addAnnotation(uri);
//						}
//					}
					// use lemma to increase chances to find sth.
					if (tmp.getAnnotations().isEmpty()) {
						if (tmp.lemma != null) {
							label = tmp.lemma;
						}
						search = classesIndex.search(label.replace("_", " "));
						for (String uri : search) {
							if(uri!=null && !uri.trim().equals("") && !containsCaseInsensitive(uri,tmp.getClassAnnotations())){
								tmp.addClassAnnotation(uri);
								addResource(tmp,uri);
							}
							
						}
						search = propertiesIndex.search(label);
						for (String uri : search) {
							if(uri!=null && !uri.trim().equals("") && !containsCaseInsensitive(uri,tmp.getAnnotations())){
								tmp.addAnnotation(uri);
								addResource(tmp,uri);
							}
							
						}
//						search = dboIndex.search(label);
//						for (String uri : search) {
//							if(uri!=null && !uri.trim().equals(""))
//							tmp.addAnnotation(uri);
//						}
					}
				} else {
					log.debug("Not annotated node: " + tmp);
				}
			}
			for (DependencyTreeNode child : tmp.getChildren()) {
				stack.push(child);
			}
		}
	}

	/** citation by HAWK*/ 
	private void annotateVerbs(DependencyTree tree) {
		Stack<DependencyTreeNode> stack = new Stack<>();
		stack.push(tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			String label = tmp.label;
			String posTag = tmp.posTag;
			if (posTag.matches("VB(.)*")) {
				List<String> search = propertiesIndex.search(label);
				if (search.isEmpty() && tmp.lemma != null) {
					search = propertiesIndex.search(tmp.lemma);
				} 
//				else if (search.isEmpty()) {
//					search = dboIndex.search(label);
//				}
				search = rank(search);
				for (String uri : search) {
					if(uri!=null && !uri.trim().equals("") && !containsCaseInsensitive(uri,tmp.getAnnotations())){
						tmp.addAnnotation(uri);
						addResource(tmp,uri);
					}
					
				}
				log.debug(Joiner.on(", ").join(tmp.getAnnotations()));
			}
			for (DependencyTreeNode child : tmp.getChildren()) {
				stack.push(child);
			}
		}
	}
	
	private List<String> findProperty(String label) {
		List<String> predicates = Lists.newArrayList();
		if(label==null){
			return predicates;
		}
		
		try {
				QueryExecution qe = sparql.qef.createQueryExecution("SELECT DISTINCT ?var WHERE { ?var rdfs:label ?l. ?l bif:contains '\""+label+"\"'@en. filter(regex (str(?l) , \"^"+label+"$\"@en , \"i\") && !regex(?var,\"http://dbpedia.org/resource/Category:\",\"i\") && regex(?var,\"http://dbpedia.org/\",\"i\") && langMatches(lang(?l),\"EN\"))} LIMIT 10");
				if (qe != null) {
					ResultSet results = qe.execSelect();
					while (results.hasNext()) {
						String predicate = results.next().get("?var").asResource().getURI();
						predicate = realResource(predicate);
						predicates = this.propertyToOntology(predicate, predicates);
						if(!containsCaseInsensitive(predicate, predicates)){
							predicates.add(predicate);
						}
						
					}
				}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return predicates;

	}

	private List<String> rankResource(List<String> search,List<Entity> nerList) {
		// TODO this ranking killed a certain predicate important for some
		// queries form training but stabilized ranking :)
		List<String> predicates = Lists.newArrayList();
		try {
			for (String predicate : search) {
				for(Entity e: nerList){
				QueryExecution qe = sparql.qef.createQueryExecution("SELECT count(*) as ?asd WHERE { <"+e.uris.get(0).getURI().replace("%2C", ",").replace("%2E", ".")+"> <"+predicate+"> ?var. filter(regex(?var , \"http://dbpedia.org/\"))}");
				if (qe != null) {
					ResultSet results = qe.execSelect();
					while (results.hasNext()) {
						int predicateCount = results.next().get("?asd").asLiteral().getInt();
						log.debug(predicate + "\t" + predicateCount);
						// TODO hack because of date properties
						if (predicateCount > 0 && !(predicate.contains("Year") || predicate.contains("Date"))) {
							predicates.add(predicate);
						}
					}
				}
				}
			}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return predicates;

	}
	
	private List<String> rankAsType(List<String> search) {
		// TODO this ranking killed a certain predicate important for some
		// queries form training but stabilized ranking :)
		List<String> predicates = Lists.newArrayList();
		try {
			int maxNum = 0;
			String maxPred = "";
			boolean dateContains = false;
			for (String predicate : search) {
				QueryExecution qe;
					qe = sparql.qef.createQueryExecution("SELECT count(*) as ?asd WHERE { ?const a <" + predicate + ">.}");
				if (qe != null) {
					ResultSet results = qe.execSelect();
					while (results.hasNext()) {
						int predicateCount = results.next().get("?asd").asLiteral().getInt();
						log.debug(predicate + "\t" + predicateCount);
						
							if(question.startsWithDate && !dateContains && (predicate.contains("Year") || predicate.contains("Date"))){
								dateContains = true;
								maxNum = predicateCount;
								maxPred = predicate;
							}
							if (predicateCount > maxNum && dateContains && (predicate.contains("Year") || predicate.contains("Date"))) {
								maxNum = predicateCount;
								maxPred = predicate;
							}else if (predicateCount > maxNum && !dateContains && !(predicate.contains("Year") || predicate.contains("Date"))) {
								maxNum = predicateCount;
								maxPred = predicate;
							}

					}
				}
			}
			if(!maxPred.equals("")){
				predicates.add(maxPred);
			}
			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return predicates;

	}
	
	private List<String> rank(List<String> search) {
		// TODO this ranking killed a certain predicate important for some
		// queries form training but stabilized ranking :)
		List<String> predicates = Lists.newArrayList();
		try {
			int maxNum = 0;
			String maxPred = "";
			
			int resMaxNum = 0;
			String resMaxPred = "";
		
			boolean dateContains = false;
			for (String predicate : search) {
				QueryExecution qe;
				if(predicate.contains("http://dbpedia.org/resource/")){
					qe = sparql.qef.createQueryExecution("SELECT count(*) as ?asd WHERE { ?const ?var <" + predicate + ">.}");
				}else{
					qe = sparql.qef.createQueryExecution("SELECT count(*) as ?asd WHERE { ?const <" + predicate + "> ?var.}");
				}
				if (qe != null) {
					ResultSet results = qe.execSelect();
					while (results.hasNext()) {
						int predicateCount = results.next().get("?asd").asLiteral().getInt();
						log.debug(predicate + "\t" + predicateCount);
						
						if(predicateCount > resMaxNum && predicate.contains("http://dbpedia.org/resource/")){
							resMaxNum = predicateCount;
							resMaxPred = predicate;
						}else{
							if(question.startsWithDate && !dateContains && (predicate.contains("Year") || predicate.contains("Date"))){
								dateContains = true;
								maxNum = predicateCount;
								maxPred = predicate;
							}
							if (predicateCount > maxNum && dateContains && (predicate.contains("Year") || predicate.contains("Date"))) {
								maxNum = predicateCount;
								maxPred = predicate;
							}else if (predicateCount > maxNum && !dateContains && !(predicate.contains("Year") || predicate.contains("Date"))) {
								maxNum = predicateCount;
								maxPred = predicate;
							}
						}

					}
				}
			}
			if(!maxPred.equals("")){
				predicates.add(maxPred);
			}
			if(!resMaxPred.equals("")){
				predicates.add(resMaxPred);
			}
			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return predicates;

	}

	/**
	 * this method annotates the left-most child of the root and uses the inline
	 * commented heuristics to annotate the tree
	 * 
	 * @param tree
	 */
	private void annotateProjectionLeftTree(DependencyTree tree) {
		Stack<DependencyTreeNode> stack = new Stack<>();
		if (tree.getRoot() != null && tree.getRoot().getChildren() != null && !tree.getRoot().getChildren().isEmpty()) {
			stack.push(tree.getRoot().getChildren().get(0));

			while (!stack.isEmpty()) {
				DependencyTreeNode tmp = stack.pop();
				String label = tmp.label;
				String posTag = tmp.posTag;
				// only one projection variable node
				if (!blacklist.contains(label)) {
					if (tmp.children.size() == 0) {
						if (posTag.matches("WRB|WP")) {
							// gives only hints towards the type of projection
							// variable
							if (label.equals("Where")) {
								tmp.addClassAnnotation("http://dbpedia.org/ontology/Place");
							} else if (label.equals("Who")) {
								tmp.addClassAnnotation("http://dbpedia.org/ontology/Agent");
							}
						} else if (posTag.matches("NN(.)*|CombinedNN")) {
							// DBO look up
							if (posTag.matches("NNS")) {
								// TODO improve lemmatization. e.g., birds->bird
								if (tmp.lemma != null)
									label = tmp.lemma;
							}
							if (classesIndex.search(label).size() > 0) {
								ArrayList<String> uris = classesIndex.search(label);
								for (String resourceURL : uris) {
									if(resourceURL!=null && !resourceURL.equals("") && !containsCaseInsensitive(resourceURL,tmp.getClassAnnotations())){
										tmp.addClassAnnotation(resourceURL);
										addResource(tmp,resourceURL);
									}
									
								}
							} else {
								log.error("Strange case that never should happen");
							}
						} else {
							log.debug("Not annotated node: " + tmp);
						}
					} else {
						/*
						 * imperative word queries like "List .." or "Give me.."
						 * do have parse trees where the root is a NN(.)*
						 */
						if (posTag.matches("NN(.)*")) {
							// TODO ask actress also in dbo owl
							if (posTag.matches("NNS")) {
								// TODO improve lemmatization. e.g.,
								// birds->bird, buildings -> building
								if (tmp.lemma != null)
									label = tmp.lemma;
							}
							if (classesIndex.search(label).size() > 0 || propertiesIndex.search(label).size() > 0) {
								ArrayList<String> uris = classesIndex.search(label);
								for (String resourceURL : uris) {
									if(resourceURL!=null && !resourceURL.equals("") && !containsCaseInsensitive(resourceURL,tmp.getClassAnnotations())){
										tmp.addClassAnnotation(resourceURL);
										addResource(tmp,resourceURL);
									}
									
								}
								uris = propertiesIndex.search(label);
								for (String resourceURL : uris) {
									if(resourceURL!=null && !resourceURL.equals("") && !containsCaseInsensitive(resourceURL,tmp.getAnnotations())){
										tmp.addAnnotation(resourceURL);
										addResource(tmp,resourceURL);
									}
									
								}

							} 
//							else if (dboIndex.search(label).size() > 0) {
//								// is not the prefered lookup
//								ArrayList<String> uris = dboIndex.search(label);
//								for (String resourceURL : uris) {
//									if(resourceURL!=null && !resourceURL.equals(""))
//									tmp.addAnnotation(resourceURL);
//								}
//							} 
							else {
								// full text lookup

								/*
								 * since a full text lookup takes place we
								 * assume hereafter there will be a FILTER
								 * clause needed which can only be handled it
								 * annotated as CombinedNoun w.r.t. its postag
								 */
								log.debug("Not annotated node: " + tmp);
							}
						} else if (posTag.matches("WRB|WP")) {
							// gives only hints towards the type of projection
							// variable
							if (label.equals("Where")) {
								tmp.addClassAnnotation("http://dbpedia.org/ontology/Place");
							} else if (label.equals("Who")) {
								tmp.addClassAnnotation("http://dbpedia.org/ontology/Agent");
							}
						} else {
							log.error("Strange case that never should happen: " + posTag);
						}
					}
				}
				break;
			}
		}
	}
	
	private List<Triple> getProperties(String uri){
		List<Triple> tripleList = new ArrayList<Triple>();
		try{
		QueryExecution qe = sparql.qef.createQueryExecution("SELECT DISTINCT ?p ?l WHERE { <"+uri.replace("%2C", ",").replace("%2E", ".")+"> ?p ?o . ?p rdfs:label ?l. filter(lang(?l)='en') }");
		if (qe != null) {
			ResultSet results = qe.execSelect();
			while (results.hasNext()) {
				QuerySolution qs = results.next();
				String predicate = qs.get("?p").asResource().getURI();
				String label = qs.get("?l").asLiteral().getString();
				tripleList.add(new Triple(predicate, "<http://www.w3.org/2000/01/rdf-schema#label>", stanfordNLP.lemmatizeSentence(label)));
			}
		}
		}catch(Exception e){
			System.err.println("SELECT DISTINCT ?p ?l WHERE { <"+uri.replace("%2C", ",").replace("%2E", ".")+"> ?p ?o . ?p rdfs:label ?l. filter(lang(?l)='en') }");
		}
		return tripleList;
	}
	
	private List<String> searchRelatedWords(List<Triple> tripleList,String label,String tag){
		List<String> relatedList = new ArrayList<String>();
		for(Triple t:tripleList){
			if(WordNetService.isRelatedWords(t.getObject(), label, tag)){
				relatedList.add(t.getSubject());
			}
		}
		return relatedList;
	}
	
	public boolean containsCaseInsensitive(String s, List<String> l){
	     for (String string : l){
	        if (string.trim().equalsIgnoreCase(s.trim())){
	            return true;
	         }
	     }
	    return false;
	  }
	
	public List<String> propertyToOntology(String s, List<String> l){
		if(s.contains("http://dbpedia.org/ontology/")){
			for(String p:l){
				if(p.contains("http://dbpedia.org/property/") && p.replace("http://dbpedia.org/property/", "http://dbpedia.org/ontology/").equals(s)){
					l.set(l.indexOf(p), s);
				}
			}
		}
		return l;
	}
	
	public String realResource(String uri){
		try{
		QueryExecution qe = sparql.qef.createQueryExecution("SELECT ?x WHERE { <"+uri+"> <http://dbpedia.org/ontology/wikiPageRedirects> ?x . }");
		if (qe != null) {
			ResultSet results = qe.execSelect();
			while (results.hasNext()) {
				uri = results.next().get("?x").asResource().getURI();
			}
		}
		}catch(Exception e){
			return uri;
		}
		return uri;
	}
	
	private void addResource(DependencyTreeNode node,String uri){
		if(uri.contains("http://dbpedia.org/resource/")){
			question.addRelatedResource(node,uri);
		}
	}
	
	public static void main(String[] args) {
//		Annotater a = new Annotater(new SPARQL());
//		a.getProperties("http://dbpedia.org/resource/Lester_Piggott");
		String s = "street_basketball_player";
		System.out.println(s.replace("_", " "));
	}
}
