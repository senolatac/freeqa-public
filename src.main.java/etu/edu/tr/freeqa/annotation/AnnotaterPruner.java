package etu.edu.tr.freeqa.annotation;

import java.util.List;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.ResultSet;

import etu.edu.tr.commons.AnnotationEntity;
import etu.edu.tr.commons.Question;
import etu.edu.tr.freeqa.nlp.DependencyTreeNode;
import etu.edu.tr.freeqa.query.SPARQL;

public class AnnotaterPruner {
	Logger log = LoggerFactory.getLogger(AnnotaterPruner.class);
	private SPARQL sparql;
	Question question;
	
	public AnnotaterPruner(SPARQL sparql){
		this.sparql = sparql;
	}
	
	public void prune(Question q){
		this.question = q;
		if(!q.languageToNamedEntites.get("en").get(0).uris.isEmpty()){
			this.pruneAnnotations();
			this.pruneClassAnnotations();
			this.pruneResources();
		}
	}
	
	private void pruneAnnotations(){
		Stack<DependencyTreeNode> stack = new Stack<>();
		stack.push(question.tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			tmp.setAnnotations(getRelatedProperties(tmp.getAnnotations()));
			for (DependencyTreeNode child : tmp.getChildren()) {
				stack.push(child);
			}
		}
	}
	
	private void pruneClassAnnotations(){
		Stack<DependencyTreeNode> stack = new Stack<>();
		stack.push(question.tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			tmp.setClassAnnotations(getRelatedClassProperties(tmp.getClassAnnotations()));
			for (DependencyTreeNode child : tmp.getChildren()) {
				stack.push(child);
			}
		}
	}
	
	private void pruneResources(){
		Stack<DependencyTreeNode> stack = new Stack<>();
		stack.push(question.tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			List<String> predicates = Lists.newArrayList();
			//for annotations
			for(String a : tmp.getAnnotations()){
				if(a.contains("http://dbpedia.org/resource/")){
					AnnotationEntity e = new AnnotationEntity(a, false,tmp);
					e= question.relatedResources.get(question.relatedResources.indexOf(e));
					if(e.isRelated){
						predicates.add(a);
					}
				}else{
					predicates.add(a);
				}
			}
			tmp.setAnnotations(predicates);
			//for entity
			if(tmp.label.contains("http://dbpedia%2Eorg/resource/")){
				AnnotationEntity e = new AnnotationEntity(tmp.label, false,tmp);
				e= question.relatedResources.get(question.relatedResources.indexOf(e));
				if(!e.isRelated){
					tmp.label = tmp.label.replace("http://dbpedia%2Eorg/resource/", "").replace("%2C", ",").replace("%2E", ".");
					question.tree.set(tmp);
				}
			}
			for (DependencyTreeNode child : tmp.getChildren()) {
				stack.push(child);
			}
		}
	}
	
	private List<String> getRelatedProperties(List<String> search){
		List<String> predicates = Lists.newArrayList();
		try {
			for (String predicate : search) {
				if(predicate.contains("http://dbpedia.org/resource/")){
					if(!predicates.contains(predicate)){
						predicates.add(predicate);
					}
				}else{
				for(AnnotationEntity e: question.relatedResources){
				QueryExecution qe = sparql.qef.createQueryExecution("SELECT count(*) as ?asd WHERE { <"+e.uri.replace("%2C", ",").replace("%2E", ".")+"> <"+predicate+"> ?var.}");
				if (qe != null) {
					ResultSet results = qe.execSelect();
					while (results.hasNext()) {
						int predicateCount = results.next().get("?asd").asLiteral().getInt();
						if(predicateCount>0){
							e.isRelated=true;
							question.relatedResources.set(question.relatedResources.indexOf(e), e);
						}
						if(predicateCount>0 && !predicates.contains(predicate)){
							predicates.add(predicate);
							e.isRelated=true;
							question.relatedResources.set(question.relatedResources.indexOf(e), e);
						}
					}
				}
				qe = sparql.qef.createQueryExecution("SELECT count(*) as ?asd WHERE {?var <"+predicate+"> <"+e.uri.replace("%2C", ",").replace("%2E", ".")+">.}");
				if (qe != null) {
					ResultSet results = qe.execSelect();
					while (results.hasNext()) {
						int predicateCount = results.next().get("?asd").asLiteral().getInt();
						if(predicateCount>0){
							e.isRelated=true;
							question.relatedResources.set(question.relatedResources.indexOf(e), e);
						}
						if(predicateCount>0 && !predicates.contains(predicate)){
							predicates.add(predicate);
						}
					}
				}
				qe = sparql.qef.createQueryExecution("SELECT count(*) as ?asd WHERE {?var <"+predicate+"> ?var1.?var <http://dbpedia.org/ontology/abstract> ?a. ?a bif:contains '\""+e.uri.replace("%2C", ",").replace("%2E", ".").replace("http://dbpedia.org/resource/", "")+"\"'.}");
				if (qe != null) {
					ResultSet results = qe.execSelect();
					while (results.hasNext()) {
						int predicateCount = results.next().get("?asd").asLiteral().getInt();
						if(predicateCount>0 && !predicates.contains(predicate)){
							predicates.add(predicate);
						}
					}
				}
				}
				}
			}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return predicates;
		
	}
	
	private List<String> getRelatedClassProperties(List<String> search){
		List<String> predicates = Lists.newArrayList();
		try {
			for (String predicate : search) {
				if(predicate.contains("http://dbpedia.org/resource/")){
					if(!predicates.contains(predicate)){
						predicates.add(predicate);
					}
				}else{
				for(AnnotationEntity e: question.relatedResources){
				QueryExecution qe = sparql.qef.createQueryExecution("SELECT count(*) as ?asd WHERE { <"+e.uri.replace("%2C", ",").replace("%2E", ".")+"> ?r ?var. ?var a <"+predicate+">}");
				if (qe != null) {
					ResultSet results = qe.execSelect();
					while (results.hasNext()) {
						int predicateCount = results.next().get("?asd").asLiteral().getInt();
						if(predicateCount>0){
							e.isRelated=true;
							question.relatedResources.set(question.relatedResources.indexOf(e), e);
						}
						if(predicateCount>0 && !predicates.contains(predicate)){
							predicates.add(predicate);
						}
					}
				}
				qe = sparql.qef.createQueryExecution("SELECT count(*) as ?asd WHERE {?var ?r <"+e.uri.replace("%2C", ",").replace("%2E", ".")+">. ?var a <"+predicate+">.}");
				if (qe != null) {
					ResultSet results = qe.execSelect();
					while (results.hasNext()) {
						int predicateCount = results.next().get("?asd").asLiteral().getInt();
						if(predicateCount>0){
							e.isRelated=true;
							question.relatedResources.set(question.relatedResources.indexOf(e), e);
						}
						if(predicateCount>0 && !predicates.contains(predicate)){
							predicates.add(predicate);
						}
					}
				}
				qe = sparql.qef.createQueryExecution("SELECT count(*) as ?asd WHERE {?var <http://dbpedia.org/ontology/abstract> ?a. ?a bif:contains '\""+e.uri.replace("%2C", ",").replace("%2E", ".").replace("http://dbpedia.org/resource/", "")+"\"'. ?var a <"+predicate+">.}");
				if (qe != null) {
					ResultSet results = qe.execSelect();
					while (results.hasNext()) {
						int predicateCount = results.next().get("?asd").asLiteral().getInt();
						if(predicateCount>0){
							e.isRelated=true;
							question.relatedResources.set(question.relatedResources.indexOf(e), e);
						}
						if(predicateCount>0 && !predicates.contains(predicate)){
							predicates.add(predicate);
						}
					}
				}
				}
				}
			}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return predicates;
		
	}

}
