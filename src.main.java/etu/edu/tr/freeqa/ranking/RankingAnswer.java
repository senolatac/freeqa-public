package etu.edu.tr.freeqa.ranking;

import java.util.AbstractMap;
import java.util.Map;

import com.google.common.collect.Maps;
import com.hp.hpl.jena.rdf.model.RDFNode;

import etu.edu.tr.commons.AnnotationEntity;
import etu.edu.tr.commons.Answer;
import etu.edu.tr.commons.Entity;
import etu.edu.tr.commons.Question;
import etu.edu.tr.freeqa.query.SPARQL;
import etu.edu.tr.freeqa.query.SPARQLQuery;

public class RankingAnswer {
	SPARQL sparQL;
	
	public RankingAnswer(SPARQL sparQL){
		this.sparQL = sparQL;
	}
	
	public Map<String, Answer> rankAnswers(Map<String, Answer> answer,Question q){
		Map<String, Answer> returnAnswer = Maps.newHashMap();
		Map.Entry<String, Answer> max = null;
		for(Entity e : q.languageToNamedEntites.get("en")){
			if(e.uris==null || e.uris.isEmpty()){
				max  = rankAndClearAnswers(answer, q, null);
			}else{
				max  = rankAndClearAnswers(answer, q, e);
			}
			if(max!=null)
			returnAnswer.put(max.getKey(), max.getValue());
		}
		return returnAnswer;
	}
	
	public Map.Entry<String, Answer> rankAndClearAnswers(Map<String, Answer> answer,Question q,Entity e){
		Map.Entry<String, Answer> max  = new AbstractMap.SimpleEntry<String, Answer>("", new Answer());
		if(answer==null || answer.isEmpty()){
			return null;
		}
		Map<String, Answer> returnAnswer = Maps.newHashMap();
		
		max.getValue().query = new SPARQLQuery();
		for (Map.Entry<String, Answer> entry : answer.entrySet())
		{
			if(e==null && entry.getValue().answerSet!=null && !entry.getValue().answerSet.isEmpty()){
				max = getMaxNum(max, rank(entry));
			}else if(entry.getKey().contains(e.uris.get(0).getURI().replace("http://dbpedia%2Eorg/resource/", "").replace("%2C", ",").replace("%2E", ".")) && entry.getValue().answerSet!=null && !entry.getValue().answerSet.isEmpty()){
				max = getMaxNum(max, rank(entry));
			}
		       
		}
		max = checkAnswer(max, q);
		returnAnswer.put(max.getKey(), max.getValue());
		return max;
	}
	
	private Map.Entry<String, Answer> checkAnswer(Map.Entry<String, Answer> entry,Question q){
		if(isTarget(entry, q)){
			return entry;
		}else{
			String key = entry.getKey();
			if(entry.getKey().contains("?source")){
				entry.getValue().answerSet = this.sparQL.sparqlBySource(entry.getKey());
				key = entry.getKey().replace("SELECT DISTINCT ?target WHERE", "SELECT DISTINCT ?source WHERE");
			}
			return new AbstractMap.SimpleEntry<String, Answer>(key, entry.getValue());
		}
	}
	
	private boolean isTarget(Map.Entry<String, Answer> entry,Question q){
		if(entry.getValue().query.constraintTriples.isEmpty()){
			return true;
		}
		if(!entry.getValue().answerSet.isEmpty()){
			for(RDFNode r : entry.getValue().answerSet){
				for(AnnotationEntity e :q.relatedResources){
					if(r!=null &&e.uri.replace("%2C",",").replace("%2E",".").equalsIgnoreCase(r.toString())){
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public Map.Entry<String, Answer> rank(Map.Entry<String, Answer> entry){
		SPARQLQuery q = entry.getValue().query;
		double score = 0;
		score = score + (q.constraintTriples.size() * 1.12); 
		score = score + (q.resourceTriples.size() * 1.05);
		score = score + (q.classConstraintTriples.size() * 1.11);
		score = score + (q.abstractTriples.size() * 0.9);
		score = score + (q.filterTriples.size() * 1.1);
		score = score + (q.doubleTriples.size() * 0.01);
		score = score + (q.executedQuery.contains("http://dbpedia.org/resource/")?0.01:0);
		q.setScore(score);
		entry.getValue().query = q;
//		System.out.println(score + " : " + q.executedQuery);
		return entry;
	}
	
    private Map.Entry<String, Answer> getMaxNum(Map.Entry<String, Answer> first,Map.Entry<String, Answer> second){
    	if(first.getValue().query.getScore()>=second.getValue().query.getScore()){
    		return first;
    	}else{
    		return second;
    	}
    }

}
