package etu.edu.tr.freeqa.main;

import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.rdf.model.RDFNode;

import etu.edu.tr.commons.Answer;
import etu.edu.tr.commons.DependencyWord;
import etu.edu.tr.commons.Question;
import etu.edu.tr.freeqa.annotation.Annotater;
import etu.edu.tr.freeqa.graph.GraphBasedSearch;
import etu.edu.tr.freeqa.nlp.DependencyTreePruner;
import etu.edu.tr.freeqa.nlp.SentenceAnalyze;
import etu.edu.tr.freeqa.nlp.SubSequence;
import etu.edu.tr.freeqa.nlp.disambiguate.DbpediaService;
import etu.edu.tr.freeqa.nlp.entityrecognition.Fox;
import etu.edu.tr.freeqa.nlp.entityrecognition.FreeNER;
import etu.edu.tr.freeqa.nlp.similarity.StringSimilarityHelper;
import etu.edu.tr.freeqa.nlp.similarity.WordNetHelper;
import etu.edu.tr.freeqa.nlp.stanford.StanfordParser;
import etu.edu.tr.freeqa.nlp.stanford.StanfordNLP;
import etu.edu.tr.freeqa.query.QueryLimit;
import etu.edu.tr.freeqa.query.SPARQL;
import etu.edu.tr.freeqa.query.SPARQLQueryBuilder;

/** citation by HAWK*/ 
public class MainContainer {
	static Logger log = LoggerFactory.getLogger(MainContainer.class);
	private Fox nerdModule;
	private FreeNER freeNer;
	private StanfordParser stanfordTree;
	private SubSequence sentenceToSequence;
	private DependencyTreePruner pruner;
	private Annotater annotater;
	private SPARQLQueryBuilder queryBuilder;
	private QueryLimit cardinality;
	private StanfordNLP stanfordNLP;
	private SentenceAnalyze sentenceAnalyze;
	private StringSimilarityHelper stringSimilarityHelper;
	private GraphBasedSearch graphBasedSearch;

	public MainContainer() {


		cardinality = new QueryLimit();
		// ASpotter wiki = new WikipediaMiner();
		// controller.nerdModule = new MultiSpotter(fox, tagMe, wiki, spot);
		nerdModule = new Fox();
		// controller.nerdModule = new Spotlight();
		// controller.nerdModule =new TagMe();
		// controller.nerdModule = new WikipediaMiner();
		freeNer = new FreeNER();

		stanfordTree = new StanfordParser();

		sentenceToSequence = new SubSequence();

		pruner = new DependencyTreePruner();

		SPARQL sparql = new SPARQL();
		annotater = new Annotater(sparql);
		queryBuilder = new SPARQLQueryBuilder(sparql);

		stanfordNLP = new StanfordNLP();
		sentenceAnalyze = new SentenceAnalyze(stanfordNLP);
		stringSimilarityHelper = new StringSimilarityHelper();
		graphBasedSearch = new GraphBasedSearch(sparql);
		
		System.setProperty("wordnet.database.dir", "dict/");
		WordNetHelper.initialize("dict/file_properties.xml");

	}


	public Map<String, Answer> run(Question q) {
		log.info(q.languageToQuestion.get("en"));
		
		q = sentenceAnalyze.analyze(q);
		
		q.dependencyList = stanfordTree.parse(q,stanfordNLP);
		q.dependencies = stanfordNLP.getDependencies(q);
	  	for(DependencyWord dw : q.dependencyList){
	  		System.out.println(dw.toString());
	  	}
		
		// 3. Build trees from questions and cache them
		q.clearTree = stanfordTree.processClear(q,stanfordNLP);
		log.info("" + q.tree);
		
		// 2. Disambiguate parts of the query
	  	nerdModule.setQuestion(q);
		q.languageToNamedEntites = nerdModule.getEntities(q.languageToQuestion.get("en"));
		
//		log.info("" + q.dependencyList);
		
		if(q.languageToNamedEntites.get("en").isEmpty() || q.languageToNamedEntites.get("en").size()==0){
			q.languageToNamedEntites = freeNer.disambiguate(q.dependencyList);
		}else{
			for(int i=0; i<q.languageToNamedEntites.get("en").size(); i++){
				String target = q.languageToNamedEntites.get("en").get(i).uris.get(0).getURI().replace("http://dbpedia%2Eorg/resource/", "").replace("%2C", ",").replace("%2E", ".").replace("_", " ");
				if(!stringSimilarityHelper.isTextSimilar(target, q.languageToNamedEntites.get("en").get(i).label.trim().replace("\\s+", " "))){
					q.languageToNamedEntites.get("en").set(i, freeNer.resourceDisambiguate(q, q.languageToNamedEntites.get("en").get(i).label));
				}else if(DbpediaService.getDisambiguateCount(q.languageToNamedEntites.get("en").get(i).label.trim().replace(" ", "_")).compareTo(0L)>0){
					q.languageToNamedEntites.get("en").set(i, freeNer.resourceDisambiguate(q, q.languageToNamedEntites.get("en").get(i).label));
				}
			}
		}
		
		// 3. Build trees from questions and cache them
		q.tree = stanfordTree.process(q,stanfordNLP);
		log.info("" + q.tree);

		// noun combiner, decrease #nodes in the DEPTree decreases
		sentenceToSequence.combineSequences(q);

		// 4. Apply pruning rules
		q.tree = pruner.prune(q);
		
		q.cardinality = cardinality.cardinality(q);

		// 5. Annotate tree
		annotater.annotateTree(q,stanfordNLP);
		log.info(q.tree.toString());
		
		q.maxDepth = cardinality.depthOfTree(q);
		log.info("" + q.maxDepth);
		
		if(!q.languageToNamedEntites.get("en").get(0).uris.isEmpty() && q.languageToNamedEntites.get("en").size()==1){
		  graphBasedSearch.search(q);
		}
		
		Map<String, Answer> answer = queryBuilder.build(q);
		for (Map.Entry<String, Answer> entry : answer.entrySet())
		{
		    for(RDFNode ans : entry.getValue().answerSet){
		    	System.err.println(ans);
		    }
		}

		return answer;
	}

}