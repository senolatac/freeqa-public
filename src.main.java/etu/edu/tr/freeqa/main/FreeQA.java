/**
 * 
 */
package etu.edu.tr.freeqa.main;

import java.util.Map;

import etu.edu.tr.commons.Answer;
import etu.edu.tr.commons.Question;


public class FreeQA {

	public static void main(String[] args) throws Exception {
		MainContainer controller = new MainContainer();

		Question q = new Question();
//		q.languageToQuestion.put("en", "Who was vice president under the president who authorized atomic weapons against Japan during World War II?");
//      q.languageToQuestion.put("en", "How many Golden Globe awards did the daughter of Henry Fonda win?");
//		q.languageToQuestion.put("en", "A landmark of which city is the home of Mona Lisa?");
//		q.languageToQuestion.put("en", "Under which pseudonym did Charles Dickens publish some of his books?");
//		q.languageToQuestion.put("en", "In which year did the Hungarian-American actor called \"The King of Horror\" make his first film?");
//		q.languageToQuestion.put("en", "What is the largest city in the county in which Faulkner spent most of his life?");
//		q.languageToQuestion.put("en", "Which Chinese-speaking country is a former Portguese colony?");
//		q.languageToQuestion.put("en", "Who is the front man of the band that wrote Coffee & TV?");
//		q.languageToQuestion.put("en", "Who plays Phileas Fogg in the adaptation of Around the World in 80 Days directed by Buzz Kulik?");
//		q.languageToQuestion.put("en", "In which city does the former main presenter of the Xpos� girls live?");
//		q.languageToQuestion.put("en", "What is the native city of Hollywood's highest-paid actress?");
//		q.languageToQuestion.put("en", "What eating disorder is characterized by an appetite for substances such as clay and sand?");
//		q.languageToQuestion.put("en", "How old is James Bond in the latest Bond book by William Boyd?");
//		q.languageToQuestion.put("en", "Did Napoleon's first wife die in France?");
//		q.languageToQuestion.put("en", "At which college did the only American actor that received the C�sar Award study?");
//		q.languageToQuestion.put("en", "Which city does the first person to climb all 14 eight-thousanders come from?");
//		q.languageToQuestion.put("en", "For which movie did the daughter of Francis Ford Coppola receive an Academy Award?");
//		q.languageToQuestion.put("en", "Gaborone is the capital of which country member of the African Union?");
//		q.languageToQuestion.put("en", "List all the battles commanded by the lover of Cleopatra.");
//		q.languageToQuestion.put("en", "Which building owned by the Bank of America was featured in the TV series MegaStructures?");
		q.languageToQuestion.put("en", "Which anti-apartheid activist was born in Mvezo?");
//		q.languageToQuestion.put("en", "In which town was the assassin of Martin Luther King born?");
//		q.languageToQuestion.put("en", "Who has vice president under the president who authorized atomic weapons against Japan during World War II?");
//		q.languageToQuestion.put("en", "Give me the currencies of all G8 countries.");
//		q.languageToQuestion.put("en", "How many Golden Globe awards did the husband of Mimi Rogers win?");
//		q.languageToQuestion.put("en", "Which members of the Wu-Tang Clan took their stage name from a film?");
//		q.languageToQuestion.put("en", "Which writers had influenced the philosopher that refused a Nobel Prize?");
//		q.languageToQuestion.put("en", "Under which king did the British prime minister that signed the Munich agreement serve?");
//		q.languageToQuestion.put("en", "Who composed the music for the film that depicts the early life of Jane Austen?");
//		q.languageToQuestion.put("en", "Who succeeded the pope that reigned only 33 days?");
//		q.languageToQuestion.put("en", "Which buildings owned by the crown overlook the North Sea?");
//		q.languageToQuestion.put("en", "List all the battles fought by the lover of Cleopatra.");
//		q.languageToQuestion.put("en", "Are the Rosetta Stone and the Gayer Anderson cat exhibits in the same museum?");
//		q.languageToQuestion.put("en", "Which street basketball player was hospital with Sarcoidosis?");
//		q.languageToQuestion.put("en", "When was the composer of the opera Madama Butterfly born?");
//		q.languageToQuestion.put("en", "Dakar is the capital of which country member of the African Union?");
//		q.languageToQuestion.put("en", "Which actress starring in the TV series Friends owns the production company Coquette Productions?");
//		q.languageToQuestion.put("en", "Which recipients of the Victoria Cross died in the Battle of Arnhem?");
//		q.languageToQuestion.put("en", "Where did the first man in space die?");
//		q.languageToQuestion.put("en", "On which island did the national poet of Greece die?");
//		q.languageToQuestion.put("en", "Which birds are protected under the National Parks and Wildlife Act?");
//		q.languageToQuestion.put("en", "Which buildings in art deco style did Shreve Lamb and Harmon design?");
//		q.languageToQuestion.put("en", "Which country did the first known photographer of snowflakes come from?");
//		q.languageToQuestion.put("en", "Which horses did The Long Fellow ride?");
//		q.languageToQuestion.put("en", "Of the people that died of radiation in Los Alamos whose death was an accident?");
//		q.languageToQuestion.put("en", "In which city was the assassin of Martin Luther King born?");
//		q.languageToQuestion.put("en", "How old was Steve Job's sister when she first met him?");
//		q.languageToQuestion.put("en", "Which anti-apartheid activist graduated from the University of South Africa?");

		
		Map<String, Answer> sparqll = controller.run(q);

		System.out.println(sparqll);
	}

}
