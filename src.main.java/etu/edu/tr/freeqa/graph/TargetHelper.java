package etu.edu.tr.freeqa.graph;

import java.util.Set;

import com.google.common.collect.Sets;

import etu.edu.tr.commons.Question;
import etu.edu.tr.freeqa.nlp.DependencyTreeNode;

public class TargetHelper {
	Set<String> blackList = Sets.newHashSet("person","people","be","did","do","when","and","while","have","take","HOW_MANY","has");
	Set<String> propertyBlackList = Sets.newHashSet("who","where","when");
	Set<String> nounBlackList = Sets.newHashSet("person","people");
	Set<String> verbBlackList = Sets.newHashSet("be","did","do","take","have");
	
	public boolean findTarget(DependencyTreeNode root){
		DependencyTreeNode firstChild = root.children.get(0);
		if(root.posTag.matches("VB(.)*") && propertyBlackList.contains(firstChild.lemma)){
			return true;
		}else if(root.posTag.matches("VB(.)*") && firstChild.posTag.matches("NN*")){
			return true;
		}
		if (!root.getAnnotations().isEmpty()) {
			return true;
		}else if(!root.getClassAnnotations().isEmpty()){
			return false;
		}else if(root.label.contains("http://dbpedia%2Eorg/resource/")){
			return false;
		}else if(nounBlackList.contains(root.lemma)){
			return false;
		}
		else if(root.posTag.matches("VB(.)*")){
			if (!firstChild.getAnnotations().isEmpty()) {
				return true;
			}else if(!firstChild.getClassAnnotations().isEmpty()){
				return false;
			}else if(firstChild.label.contains("http://dbpedia%2Eorg/resource/")){
				return false;
			}else if(nounBlackList.contains(firstChild.lemma)){
				return false;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	public void findTarget(Question q){
		DependencyTreeNode root = q.tree.getRoot();
		DependencyTreeNode firstChild = root.children.get(0);
		if(root.posTag.matches("VB(.)*") && propertyBlackList.contains(firstChild.lemma) && !blackList.contains(root.lemma)){
			root.isOutput = true;
			q.isOutputPredictable =true;
		}else if(root.posTag.matches("VB(.)*") && firstChild.posTag.matches("NN(.)*")){
			firstChild.isOutput = true;
			q.isOutputPredictable =true;
		}
	}
	
	public static void main(String[] args) {
		String s="NNP";
		if(s.matches("NN(.)*")){
			System.out.println(s);
		}
	}

}
