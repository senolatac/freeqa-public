package etu.edu.tr.freeqa.graph;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;

import etu.edu.tr.commons.AnnotationEntity;
import etu.edu.tr.commons.ExecutableQuery;
import etu.edu.tr.commons.Question;
import etu.edu.tr.freeqa.nlp.DependencyTreeNode;
import etu.edu.tr.freeqa.query.SPARQL;
import etu.edu.tr.freeqa.query.SPARQLQuery;
import etu.edu.tr.freeqa.query.SparQLHelper;

public class GraphBasedSearch {
	Logger log = LoggerFactory.getLogger(GraphBasedSearch.class);
	Set<String> blackList = Sets.newHashSet("person","people","be","did","do","when","and","while","have","take","HOW_MANY","tv series");
	Set<String> typeBlackList = Sets.newHashSet("who","where");
	private SPARQL sparql;
	Question question;
	SparQLHelper sparqlHelper;
	TargetHelper targetHelper;
	
	public GraphBasedSearch(SPARQL sparql){
	  this.sparql = sparql;
	  this.sparqlHelper = new SparQLHelper(sparql,question);
	  targetHelper =new TargetHelper();
	}
	
	public void search(Question q){
		this.question = q;
//		for(AnnotationEntity e: question.relatedResources){
//			if(!e.isRelated){
//			question.relatedResources.remove(question.relatedResources.indexOf(e));	
//			}
//		}
		targetHelper.findTarget(question);
		this.sparqlHelper.question = this.question;
		sparqlHelper.isTarget = targetHelper.findTarget(q.tree.getRoot());
		this.searchResources();
		this.searchResourceAbstract();
		this.searchTree();
//		this.searchRecursionTree();
//		for(SPARQLQuery qe:question.queryList){
//			System.err.println("-->("+qe.uriVar+") "+qe.executedQuery+"--<");
//		}
		
	}
	
	private void searchResourceAbstract(){
		if(question.queryList==null || question.queryList.isEmpty()){
			for(AnnotationEntity e: question.relatedResources){
			List<DependencyTreeNode> nodeList = new ArrayList<DependencyTreeNode>();
			searchAbstract(e.node,new SPARQLQuery(nodeList),e.uri,"?uri",e.uri);
			searchContain(e.node,new SPARQLQuery(nodeList),e.uri,"?uri",e.uri);
			}
		}
	}
	
	private void searchRecursionTree(){
		Stack<DependencyTreeNode> stack = new Stack<>();
		stack.push(question.tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			if(!blackList.contains(tmp.lemma) && !tmp.label.contains("http://dbpedia%2Eorg/resource/")){
					this.searchTree();
			}
			for (DependencyTreeNode child : tmp.getChildren()) {
				stack.push(child);
			}
		}
	}
	
	private void searchTree(){
		Stack<DependencyTreeNode> stack = new Stack<>();
		stack.push(question.tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			if(!blackList.contains(tmp.lemma)){
//				List<SPARQLQuery> qList = new ArrayList<SPARQLQuery>();
//				for(SPARQLQuery e:question.queryList){
////					System.err.println(e.getScore() + " : " + e);
//					qList.add(e);
//				}
			    PriorityQueue<SPARQLQuery> queue = new PriorityQueue<>(100);
			    queue.addAll(question.queryList);
			    while(!queue.isEmpty()){
			    	SPARQLQuery e = queue.remove();
			    	System.out.println("("+e.uriVar+") Score : "+ e.getScore() + " : " + e.isIncludeResource + " : " +e);
			    	if(question.maxDepth==(e.abstractTriples.size()+e.constraintTriples.size()+e.filterTriples.size()+e.classConstraintTriples.size()+e.isIncludeResource)){
			    		System.err.println(e.abstractTriples.size()+" : "+e.constraintTriples.size()+" : "+e.filterTriples.size()+" : "+e.classConstraintTriples.size()+" : "+e.isIncludeResource);
			    		question.queryList = new ArrayList<SPARQLQuery>();
			    		question.addQuery(e);
			    		System.out.println("Sonu� : " + e);
			    		return;
			    	}
			    	List<SPARQLQuery> qLst = search(e);
//			    	for(SPARQLQuery qs : qLst){
//			    		System.err.println("Score : "+ qs.getScore() + " : " + qs);
//			    	}
			    	queue.addAll(qLst);
//			    	List<String> varList = new ArrayList<String>();
//					for(String s : e.varList){
//						varList.add(s);
//					}
//					for(String s : varList){
//						List<SPARQLQuery> qLst = searchAnnotations(tmp,e,tmp.getAnnotations(),s);
//						if(qLst!=null && !qLst.isEmpty()){
//							queue.addAll(qLst);
//						}
//						qLst = searchClassAnnotations(tmp,e,tmp.getClassAnnotations(),s);
//						if(qLst!=null && !qLst.isEmpty()){
//							queue.addAll(qLst);
//						}
//						SPARQLQuery sq =searchAbstract(tmp,e,tmp.label,s,tmp.lemma);
//						if(sq!=null){
//							queue.add(sq);
//						}
//						sq = searchContain(tmp,e,tmp.label,s,tmp.lemma);
//						if(sq!=null){
//							queue.add(sq);
//						}
//					}
			    	
			    }
//				for(SPARQLQuery e: qList){
//					List<String> varList = new ArrayList<String>();
//					for(String s : e.varList){
//						varList.add(s);
//					}
//					for(String s : varList){
//						searchAnnotations(tmp,e,tmp.getAnnotations(),s);
//						searchClassAnnotations(tmp,e,tmp.getClassAnnotations(),s);
//						searchAbstract(tmp,e,tmp.label,s,tmp.lemma);
//						searchContain(tmp,e,tmp.label,s,tmp.lemma);
//					}
//				}
			}
			for (DependencyTreeNode child : tmp.getChildren()) {
				stack.push(child);
			}
		}
	}
	
	private List<SPARQLQuery> search(SPARQLQuery e){
		List<SPARQLQuery> rList = new ArrayList<SPARQLQuery>();
    	List<String> varList = new ArrayList<String>();
		for(String s : e.varList){
//			System.err.println("var : " + s);
			varList.add(s);
		}
		for(String s : varList){
		Stack<DependencyTreeNode> stack = new Stack<>();
		stack.push(question.tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			if(!blackList.contains(tmp.lemma)){
				List<SPARQLQuery> qLst = searchAnnotations(tmp,e,tmp.getAnnotations(),s);
				if(qLst!=null && !qLst.isEmpty()){
					rList.addAll(qLst);
				}
				qLst = searchClassAnnotations(tmp,e,tmp.getClassAnnotations(),s);
				if(qLst!=null && !qLst.isEmpty()){
					rList.addAll(qLst);
				}
				if(!typeBlackList.contains(tmp.lemma)){
					SPARQLQuery sq =searchAbstract(tmp,e,tmp.label,s,tmp.lemma);
					if(sq!=null){
						rList.add(sq);
					}
					sq = searchContain(tmp,e,tmp.label,s,tmp.lemma);
					if(sq!=null){
						rList.add(sq);
					}
				}
			}
			for (DependencyTreeNode child : tmp.getChildren()) {
				stack.push(child);
			}
		}
		}
		return rList;
	}
	
	private void searchResources(){
		Stack<DependencyTreeNode> stack = new Stack<>();
		stack.push(question.tree.getRoot());
		while (!stack.isEmpty()) {
			DependencyTreeNode tmp = stack.pop();
			if(!blackList.contains(tmp.lemma)){
				for(AnnotationEntity e: question.relatedResources){
				List<DependencyTreeNode> nodeList = new ArrayList<DependencyTreeNode>();
				nodeList.add(e.node);
				String q ="?uri <http://dbpedia.org/ontology/abstract> ?uriAbc.\n?uriAbc bif:contains '(\""+e.uri.replace("%2C", ",").replace("%2E", ".").replace("http://dbpedia.org/resource/", "")+"\")'.\n";
				String q1 ="?uri ?r ?uri1.\nFILTER((CONTAINS(str(?uri1),\""+e.uri.replace("%2C", ",").replace("%2E", ".").replace("http://dbpedia.org/resource/", "")+"\") OR CONTAINS(str(?uri1),\""+e.uri.replace("%2C", ",").replace("%2E", ".").replace("http://dbpedia.org/resource/", "").replace("_", " ")+"\"))).\n";
				searchAnnotations(tmp,new SPARQLQuery(nodeList,true),tmp.getAnnotations(),"<"+e.uri+">");
				searchAnnotations(tmp,new SPARQLQuery(nodeList,q),tmp.getAnnotations(),"?uri");
				searchAnnotations(tmp,new SPARQLQuery(nodeList,q1),tmp.getAnnotations(),"?uri");
				searchClassAnnotations(tmp,new SPARQLQuery(nodeList,true),tmp.getClassAnnotations(),"<"+e.uri+">");
				searchClassAnnotations(tmp,new SPARQLQuery(nodeList,q),tmp.getClassAnnotations(),"?uri");
				searchAbstract(tmp,new SPARQLQuery(nodeList,true),tmp.label,"<"+e.uri+">",tmp.lemma);
				searchContain(tmp,new SPARQLQuery(nodeList,true),tmp.label,"<"+e.uri+">",tmp.lemma);
				}
			}
			for (DependencyTreeNode child : tmp.getChildren()) {
				stack.push(child);
			}
		}
	}
	
	private List<SPARQLQuery> searchAnnotations(DependencyTreeNode n,SPARQLQuery executableQuery,List<String> search,String resource){
		List<SPARQLQuery> qList = new ArrayList<SPARQLQuery>();
		if(search.isEmpty()){
			return null;
		}
		if(!executableQuery.nodeList.contains(n)){
			for(String s : search){
				if(s.contains("http://dbpedia.org/resource/")){
					SPARQLQuery q =this.searchContain(n, executableQuery, s, resource,n.lemma);
					if(q!=null){
						qList.add(q);
					}
				}
				SPARQLQuery eq = this.sparqlHelper.getResourcePropertyRelation(n,executableQuery, resource, s);
//				System.out.println(executableQuery +":"+ resource+":"+ s);
				if(eq!=null){
					eq.addNode(n);
					this.question.addQuery(eq);
					qList.add(eq);
				}
			}
		}
		return qList;
	}
	
	private List<SPARQLQuery> searchClassAnnotations(DependencyTreeNode n,SPARQLQuery executableQuery,List<String> search,String resource){
		List<SPARQLQuery> qList = new ArrayList<SPARQLQuery>();
		if(search.isEmpty()){
			return null;
		}
		if(!executableQuery.nodeList.contains(n)){
			for(String s : search){
				SPARQLQuery eq = this.sparqlHelper.getResourceClassRelation(n,executableQuery, resource, "<"+s+">");
				if(eq!=null){
					eq.addNode(n);
					this.question.addQuery(eq);
					qList.add(eq);
				}
			}
		}
		return qList;
	}
	
	private SPARQLQuery searchAbstract(DependencyTreeNode n,SPARQLQuery executableQuery,String predicate,String resource,String lemma){
		List<SPARQLQuery> qList = new ArrayList<SPARQLQuery>();
		if(predicate==null){
			return null;
		}
		if(!executableQuery.nodeList.contains(n)){
			SPARQLQuery eq = this.sparqlHelper.getResourceAbstractRelation(n,executableQuery, resource, predicate,lemma);
			if(eq!=null){
				eq.addNode(n);
				this.question.addQuery(eq);
				return eq;
			}
		}
		return null;
	}
	
	private SPARQLQuery searchContain(DependencyTreeNode n,SPARQLQuery executableQuery,String search,String resource,String lemma){
		if(search==null){
			return null;
		}
		if(!executableQuery.nodeList.contains(n)){
			SPARQLQuery eq = this.sparqlHelper.getResourceContainRelation(n,executableQuery, resource, search,lemma);
			if(eq!=null){
				eq.addNode(n);
				this.question.addQuery(eq);
//				System.out.println("Contain : " +eq.getScore() + " : " + eq);
				return eq;
			}
		}
		return null;
	}

}
